local composer = require( "composer" )
local scene = composer.newScene()
musicafondochanel=nil
destinoBack = "menus.menuJuego"
local gameState = require("utilerias.gameState")
local base = require("utilerias.base" )

local candado2,candado3 = false,false

local function btnTap(event)
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    audio.stop(musicafondochanel)  
    audio.dispose(musicafondochanel)
    musicafondochanel=nil
    
    pathSelection = event.target.name
    
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end  

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        
        if  gameState.menuCuento==false then
            --utils.reproducirSonido("sounds/menu/menuCuento",0)
            gameState.menuCuento=true
        end
       
        local background = display.newImage( group, path.."fondo.png" )
        background:translate( centerX, centerY )
    
        local game1 = display.newImage( group,path..pathGame.."Beginning.png" )
        game1:translate( centerX/3.5, centerY*.95 )
        game1:addEventListener("tap", btnTap)
        game1.destination = destinoDeJuegoSeleccionado
        game1.name = "juego1/"
        
        local game2 = display.newImage( group,path..pathGame.."Middle.png" )
        game2:translate( centerX, centerY*.95 )
        game2.destination = destinoDeJuegoSeleccionado
        game2.name = "juego2/"
       
        local game3 = display.newImage( group,path..pathGame.."End.png" )
        game3:translate( centerX*1.7, centerY*.95 )
        game3.destination = destinoDeJuegoSeleccionado
        game3.name = "juego3/"

        -- Preguntar por niveles abiertos
        
        if  gameState.tipodejuego==1 then -- la compra
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j1memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j1memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j1diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j1diferencias') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==2 then -- el viaje
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j2memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j2memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j2diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j2diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j2arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j2arrastra') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==3 then -- penny benny
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j3memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j3memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "recuerda/" then
                if base.consultarVariable('nivel2j3recuerda') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j3recuerda') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j3diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j3diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j3arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j3arrastra') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==4 then -- los sonidos
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j4memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j4memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "recuerda/" then
                if base.consultarVariable('nivel2j4recuerda') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j4recuerda') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j4diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j4diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j4arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j4arrastra') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==5 then -- popi
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j5memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j5memorama') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==6 then -- danny tanny
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j6memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j6memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "recuerda/" then
                if base.consultarVariable('nivel2j6recuerda') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j6recuerda') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j6diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j6diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j6arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j6arrastra') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==7 then -- cally gally
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j7memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j7memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "recuerda/" then
                if base.consultarVariable('nivel2j7recuerda') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j7recuerda') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j7diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j7diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j7arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j7arrastra') == "false" then
                    candado3 = true
                end
            end

        elseif  gameState.tipodejuego==8 then -- zara zane
            
            if pathGame == "memorama/" then
                if base.consultarVariable('nivel2j8memorama') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j8memorama') == "false" then
                    candado3 = true
                end
            elseif pathGame == "recuerda/" then
                if base.consultarVariable('nivel2j8recuerda') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j8recuerda') == "false" then
                    candado3 = true
                end
            elseif pathGame == "diferencias/" then
                if base.consultarVariable('nivel2j8diferencias') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j8diferencias') == "false" then
                    candado3 = true
                end
            elseif pathGame == "arrastra/" then
                if base.consultarVariable('nivel2j8arrastra') == "false" then
                    candado2 = true
                end

                if base.consultarVariable('nivel3j8arrastra') == "false" then
                    candado3 = true
                end
            end

        end
        
        
        --Poner candados 
        candado2= false;
        candado3=false;
        
        if candado2 == true then
            local block2 = display.newImage( group,"images/candado.png" )
            block2:translate( centerX*1.01, centerY*.95 )
        else
            game2:addEventListener("tap", btnTap)
        end
        
        if candado3 == true then
            local block3 = display.newImage( group,"images/candado.png" )
            block3:translate( centerX*1.71, centerY*.95 )
        else
            game3:addEventListener("tap", btnTap)
        end
        
        ----------------
       
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menus.menuJuego"
        btnRegresar.name = ""
        
        local musicafondo =audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
      
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
