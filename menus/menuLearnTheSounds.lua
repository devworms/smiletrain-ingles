local composer = require( "composer" )
local widget = require( "widget" )
local scene = composer.newScene()
local gameState = require("utilerias.gameState")
destinoBack = "menus.menuJuego"
local video

local btnFirst
local btnSecond

local function videoListener( event )
    print( "Video event phase: " .. event.phase )
    if (event.phase == "ended") then

        btnFirst:setEnabled( true ) 
        btnSecond:setEnabled( true ) 

        video:removeSelf()
        video = nil 

        destinoBack = "menus.menuJuego"
    elseif (event.phase == "ready") then

        btnFirst:setEnabled( false ) 
        btnSecond:setEnabled( false ) 

        destinoBack = "" -- TODO see
    end
end

local function videoTap( event )
    if video == nil then
        audio.stop()

        video = native.newVideo( centerX, display.contentCenterY-35, 1024, 520 )
        video:load( path..pathGame..event.target.id..".mp4")
        video:addEventListener( "video", videoListener )
        video:play()
    end

    return true
end

local function btnTap(event)
    if video ~= nil then

        btnFirst:setEnabled( true ) 
        btnSecond:setEnabled( true ) 

        video:removeSelf()
        video = nil

        destinoBack = "menus.menuJuego"
    else 
        audio.stop()

        utils.tipoImagen()
        utils.reproducirSonido("boton")
        
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        
        print(event.target.destination)
    end
    
    return true
end   

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
        
    if gameState.tipodejuego == 3 then --penny
        utils.reproducirSonido("sounds/menu/ltspenny")
    elseif gameState.tipodejuego == 6 then --danny
        utils.reproducirSonido("sounds/menu/ltsdanny")
    elseif gameState.tipodejuego == 7 then --cally
        utils.reproducirSonido("sounds/menu/ltscally")
    elseif(gameState.tipodejuego == 8) then -- zara                                  
        utils.reproducirSonido("sounds/menu/ltszara") 
    end
       
    local background = display.newImage( path.."fondo.png" )
    background:translate( centerX, centerY )

    btnFirst = widget.newButton( {
        id = "1", 
        onPress = videoTap,
        defaultFile = path..pathGame.."1.png" } )
    btnFirst.x = centerX-160
    btnFirst.y = centerY
    
    btnSecond = widget.newButton( {
        id = "2", 
        onPress = videoTap,
        defaultFile = path..pathGame.."2.png" } )
    btnSecond.x = centerX+200
    btnSecond.y = centerY

    local btnRegresar = display.newImage( "images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menus.menuJuego"
    
    local musicafondo = audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
    local musicafondochanel = audio.play( musicafondo, { loops= -1 }  ) 

    group:insert(background)
    group:insert(btnFirst)
    group:insert(btnSecond)
    group:insert(btnRegresar)

end

-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end

-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene