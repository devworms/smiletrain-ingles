local composer = require( "composer" )
local scene = composer.newScene()
musicafondochanel=nil
local gameState = require("utilerias.gameState")
destinoBack = "menus.menu"

local function btnTap(event)
    utils.tipoImagen()

    local audioChannel = audio.stop() 
    
    if (musicafondochanel~=nil) then
        audio.stop(musicafondochanel)  
        audio.dispose(musicafondochanel)
        musicafondochanel=nil
    end
    
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end  

-- Called when the scene's view does not exist:
function scene:create( event )
    local group = self.view
        
    --if  gameState.menuCuento==false then
        --[[if gameState.tipodejuego == 1 then -- trip
            utils.reproducirSonido("sounds/menu/introC1")
        elseif gameState.tipodejuego == 2 then --zara
            utils.reproducirSonido("sounds/menu/introC2")
        elseif gameState.tipodejuego == 3 then --zara
            utils.reproducirSonido("sounds/menu/introC3")
        elseif gameState.tipodejuego == 4 then --zara
            utils.reproducirSonido("sounds/menu/introC4")
        elseif gameState.tipodejuego == 5 then --zara
            utils.reproducirSonido("sounds/menu/introC5")
        elseif gameState.tipodejuego == 6 then --zara
            utils.reproducirSonido("sounds/menu/introC6")
        elseif gameState.tipodejuego == 7 then --zara
            utils.reproducirSonido("sounds/menu/introC7")
        elseif(gameState.tipodejuego == 8) then -- Secuencias]]                                  
            utils.reproducirSonido("sounds/menu/intro") 
        --end
    
       -- gameState.menuCuento=false
    --end
       
        local background = display.newImage( group, path.."fondo.png" )
        background:translate( centerX, centerY )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5.2 )
        topsign.xScale = 1.2
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="What Do You Want To Do?"
        
        
        
        
        local btnJugar = display.newImage( group, path.."play.png" )
        btnJugar:translate( centerX+200, centerY )
        btnJugar:addEventListener("tap", btnTap)
        btnJugar.destination = "menus.menuJuego"
        btnJugar.name = ""
        
        local history = display.newImage( group, path.."story.png" )
        history:translate( centerX-200, centerY)
        history:addEventListener("tap", btnTap)
        history.destination = "menus.menuHistory"
      

        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menus.menu"
        btnRegresar.name = ""
        
        local musicafondo =audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 

end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
    -- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
    local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
    local group = self.view
    
    composer.removeScene( composer.getSceneName( "current" ) )
    
    --Runtime:removeEventListener("enterFrame", update)
    -- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
    local group = self.view

    -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
