local composer = require( "composer" )
local scene = composer.newScene()
local gameState = require("utilerias.gameState")
destinoBack = "home"
local spriteTam
local spriteImag
local sequenceDataSprite
local SpriteNuevo
local groupGlobalScene
local btnclose

local function btnTap(event)
    
    audio.stop() 
    
    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

local function clickSprite()
    audio.stop()
    
    print("tipo de juego = "..gameState.tipodejuego)
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    composer.gotoScene ( "menus.menuCuento", { effect = "fade"} )
    composer.removeScene( "bloqueo" )
end

local function recorrerSprite(nameSprite)
    
    spriteTam = nil
    spriteImag = nil
    sequenceDataSprite = nil
    SpriteNuevo = nil
    
    if nameSprite == "compra" then
        spriteTam = { width=307, height=263,  numFrames=36 }
    elseif nameSprite == "viaje" then
        spriteTam = { width=307, height=264,  numFrames=36 }
    elseif nameSprite == "penny" then
        spriteTam = { width=332, height=265,  numFrames=36 }
    elseif nameSprite == "sonidos" then
        spriteTam = { width=324, height=264,  numFrames=36 }
    elseif nameSprite == "popi" then
        spriteTam = { width=313, height=265,  numFrames=36 }
    elseif nameSprite == "tanny" then
        spriteTam = { width=307, height=263,  numFrames=36 }
    elseif nameSprite == "cally" then
        spriteTam = { width=307, height=263,  numFrames=36 }
    elseif nameSprite == "zara" then
        spriteTam = { width=327, height=262,  numFrames=36 }
    end
    
    spriteImag = graphics.newImageSheet( "images/menuSprites/"..nameSprite..".png", spriteTam )
    
    sequenceDataSprite =
                {
                    { name="life", start=1, count=34, time=2750 ,loopCount=1 }
                }
    SpriteNuevo = display.newSprite( spriteImag, sequenceDataSprite )
    SpriteNuevo.x = centerX
    SpriteNuevo.y = centerY*.9
        --SpriteNuevo.xScale=.6
        --SpriteNuevo.yScale=.6

    SpriteNuevo.w = SpriteNuevo.contentWidth
    SpriteNuevo.h = SpriteNuevo.contentHeight
        
    SpriteNuevo:addEventListener("tap", clickSprite)

    groupGlobalScene:insert(SpriteNuevo)
                
    SpriteNuevo:play()
end

local function btnTapImg(event)
    
    SpriteNuevo:removeSelf()
    
    if event.target.name == "compra" then
        gameState.tipodejuego=1
        recorrerSprite( event.target.name )
    elseif event.target.name == "viaje" then
        gameState.tipodejuego=2
        recorrerSprite( event.target.name )
    elseif event.target.name == "penny" then
        gameState.tipodejuego=3
        recorrerSprite( event.target.name )
    elseif event.target.name == "sonidos" then
        gameState.tipodejuego=4
        recorrerSprite( event.target.name )
    elseif event.target.name == "popi" then
        gameState.tipodejuego=5
        recorrerSprite( event.target.name )
    elseif event.target.name == "tanny" then
        gameState.tipodejuego=6
        recorrerSprite( event.target.name )
    elseif event.target.name == "cally" then
        gameState.tipodejuego=7
        recorrerSprite( event.target.name )
    elseif event.target.name == "zara" then
        gameState.tipodejuego=8
        recorrerSprite( event.target.name )
    end
end

local function recorrerAtras()
    SpriteNuevo:removeSelf()
    
    if gameState.tipodejuego == 7 then
        gameState.tipodejuego=6
        recorrerSprite( "tanny" )
    elseif gameState.tipodejuego == 8 then
        gameState.tipodejuego=7
        recorrerSprite( "cally" )
    elseif gameState.tipodejuego == 2 then
        gameState.tipodejuego=8
        recorrerSprite( "zara" )
    elseif gameState.tipodejuego == 1 then
        gameState.tipodejuego=2
        recorrerSprite( "viaje" )
    elseif gameState.tipodejuego == 5 then
        gameState.tipodejuego=1
        recorrerSprite( "compra" )
    elseif gameState.tipodejuego == 4 then
        gameState.tipodejuego=5
        recorrerSprite( "popi" )
    elseif gameState.tipodejuego == 3 then
        gameState.tipodejuego=4
        recorrerSprite( "sonidos" )
    elseif gameState.tipodejuego == 6 then
        gameState.tipodejuego=3
        recorrerSprite( "penny" )
    end
end

local function recorrerAdelante()
    SpriteNuevo:removeSelf()
    
    if gameState.tipodejuego == 3 then
        gameState.tipodejuego=6
        recorrerSprite( "tanny" )
    elseif gameState.tipodejuego == 6 then
        gameState.tipodejuego=7
        recorrerSprite( "cally" )
    elseif gameState.tipodejuego == 7 then
        gameState.tipodejuego=8
        recorrerSprite( "zara" )
    elseif gameState.tipodejuego == 8 then
        gameState.tipodejuego=2
        recorrerSprite( "viaje" )
    elseif gameState.tipodejuego == 2 then
        gameState.tipodejuego=1
        recorrerSprite( "compra" )
    elseif gameState.tipodejuego == 1 then
        gameState.tipodejuego=5
        recorrerSprite( "popi" )
    elseif gameState.tipodejuego == 5 then
        gameState.tipodejuego=4
        recorrerSprite( "sonidos" )
    elseif gameState.tipodejuego == 4 then
        gameState.tipodejuego=3
        recorrerSprite( "penny" )
    end
end

-- Called when the scene's view does not exist:
function scene:create( event )
    
    local function repiteloMen(event)
    local audioChannel = audio.stop() 
    end
	local group = self.view
        
        groupGlobalScene = group
        
        if  gameState.menunNino==false then
            utils.reproducirSonido("sounds/menu/menuS")
            gameState.menunNino=false
        end
    
        local background = display.newImage( group,"images/menu/fondo.png" )
        background:translate( centerX, centerY )	
        
        gameState.tipodejuego=3
        recorrerSprite( "penny" )
        
        local movIzq = display.newImage( group,"images/menu/flecha_izq.png" )
        movIzq:translate( centerX*.45, centerY*.9 )
        movIzq:addEventListener("tap", recorrerAtras)
       
        local movDer = display.newImage( group,"images/menu/flecha_der.png" )
        movDer:translate( centerX*1.55, centerY*.9 )
        movDer:addEventListener("tap", recorrerAdelante)
        
        local btnPenny = display.newImage( group,"images/menu/boton1.png" )
        btnPenny:translate( centerX*.25, centerY*1.15 )
        btnPenny:addEventListener("tap", btnTapImg)
        btnPenny.name = "penny"
        
        local btnViaje = display.newImage( group,"images/menu/boton2.png" )
        btnViaje:translate( centerX*.45, centerY*1.3 )
        btnViaje:addEventListener("tap", btnTapImg)
        btnViaje.name = "tanny"
        
        local btnPopi = display.newImage( group,"images/menu/boton3.png" )
        btnPopi:translate( centerX*.65, centerY*1.45 )
        btnPopi:addEventListener("tap", btnTapImg)
        btnPopi.name = "cally"
        
        local btnSonido = display.newImage( group,"images/menu/boton4.png" )
        btnSonido:translate( centerX*.85, centerY*1.6 )
        btnSonido:addEventListener("tap", btnTapImg)
        btnSonido.name = "zara"
        
        local btnSonido1 = display.newImage( group,"images/menu/boton5.png" )
        btnSonido1:translate( centerX*1.15, centerY*1.6 )
        btnSonido1:addEventListener("tap", btnTapImg)
        btnSonido1.name = "viaje"
        
        local btnSonido2 = display.newImage( group,"images/menu/boton6.png" )
        btnSonido2:translate( centerX*1.35, centerY*1.45 )
        btnSonido2:addEventListener("tap", btnTapImg)
        btnSonido2.name = "compra"
        
        local btnSonido3 = display.newImage( group,"images/menu/boton7.png" )
        btnSonido3:translate( centerX*1.55, centerY*1.3 )
        btnSonido3:addEventListener("tap", btnTapImg)
        btnSonido3.name = "popi"
        
        local btnSonido4 = display.newImage( group,"images/menu/boton8.png" )
        btnSonido4:translate( centerX*1.75, centerY*1.15 )
        btnSonido4:addEventListener("tap", btnTapImg)
        btnSonido4.name = "sonidos"
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "home"

        local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
        btnInfo:translate( centerX*1.75, centerY*1.9 )
        btnInfo :addEventListener("tap", btnTap)
        btnInfo.destination = "extras.info_menu"
        
        local btnRepetir = display.newImage( group,"images/menu/silencio.png" )
       
        btnRepetir:translate( centerX*1.6, centerY*1.9 )
        btnRepetir:addEventListener("tap", repiteloMen)
        
        local musicafondo = audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        local musicafondochanel = audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
        
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view        

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
        
        composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end

-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------