local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
local destino1, destino2, destino3, destino4, destino6
local name1, name2, name3, name4, name6
local tag1, tag2, tag3, tag4, tag6
local btnclose

destinoBack = "menus.menuCuento"

local function btnTap(event)

    audio.stop() 
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")
    
    --print("hola: "..event.target.name)
    --print("hola2: "..event.target.tag)
    print(event.target.destination)
    
    pathGame = event.target.name
    destinoDeJuegoSeleccionado = event.target.tag
    
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    
    return true
end

local function tipoJuego()
    if  gameState.tipodejuego == 1 then -- la compra
        destino1 = "menus.menuDesbloquear"
        destino2 = "menus.menuDesbloquear"
        destino3 = "juegos.lacompra.lacompra"
        destino4 = "juegos.lacompra.elsuper.elsuper"
        --destino5 = "juegos.practiceWordsGame"

        name1 = "diferencias/"
        name2 = "memorama/"
        name3 = ""
        name4 = ""
        --name5 = ""
        
        tag1 = "juegos.diferencias"
        tag2 = "juegos.memorama"
        tag3 = ""
        tag4 = ""
        --tag5 ="juegos.practiceWordsGame"
        
    elseif  gameState.tipodejuego == 2 then -- el viaje
        destino1 = "menus.menuDesbloquear"
        destino2 = "menus.menuDesbloquear"
        destino3 = "menus.menuDesbloquear"
        destino4 = "juegos.elviaje.viaje"
        --destino5 = "juegos.practiceWordsGame"
        
        name1 = "diferencias/"
        name2 = "memorama/"
        name3 = "arrastra/"
        name4 = "viaje/"
        --name5 = ""
        
        tag1 = "juegos.diferencias"
        tag2 = "juegos.memorama"
        tag3 = "juegos.arrastra"
        tag4 = "juegos.viaje"
        --tag5 ="juegos.practiceWordsGame"
    elseif  gameState.tipodejuego == 5 then -- popi
        destino1 = "menus.menuDesbloquear"
        destino2 = "juegos.popi.encasa.menuEncasa"
        destino3 = "juegos.arrastra"
        destino4 = "juegos.popi.ruleta"
        --destino5="juegos.practiceWordsGame"

        name1 = "memorama/"
        name2 = ""
        name3 = "arrastra/"
        name4 = ""
        --name5 =""
        
        tag1 = "juegos.memorama"
        tag2 = ""
        tag3 = "juegos.arrastra"
        tag4 = ""
        --tag5 ="juegos.practiceWordsGame"
    elseif  gameState.tipodejuego == 4 then -- los sonidos
        destino1 = "menus.menuDesbloquear"
        destino2 = "menus.menuDesbloquear"
        destino3 = "menus.menuDesbloquear"
        destino4 = "menus.menuDesbloquear"
        --destino5 = "juegos.practiceWordsGame"

        name1 = "explosive/"
        name2 = "memorama/"
        name3 = "arrastra/"
        name4 = "ruleta/"
        --name5 = ""
        
        tag1 = "juegos.explosive"
        tag2 = "juegos.memorama"
        tag3 = "juegos.arrastra"
        tag4 = "juegos.ruleta"
        --tag5 ="juegos.practiceWordsGame"  
    else
        destino1 = "menus.menuLearnTheSounds"
        destino2 = "juegos.practiceWordsGame"
        destino3 = "menus.menuDesbloquear"
        destino4 = "menus.menuDesbloquear"
        destino5 = "menus.menuDesbloquear"
        destino6 = "menus.menuDesbloquear"
        
        name1 = "learnthesounds/"
        name2=  " "
        name3 = "ruleta/"
        name4 = "diferencias/"
        name5 = "memorama/"
        name6 = "arrastra/" 

        
        tag1 = ""
        tag2 = "juegos.practiceWordsGame"
        tag3 = "juegos.ruleta"--practice silabas
        tag4 = "juegos.diferencias"
        tag5 = "juegos.memorama"
        tag6 = "juegos.arrastra"

        
    end    
end 

-- Called when the scene's view does not exist:
function scene:create( event )
    local group = self.view
        
        tipoJuego()
        composer.removeScene( "bloqueo" )
        if  gameState.tipodejuego==1 then
             utils.reproducirSonido("sounds/menu/introShop")
             --compra
             --gameState.menucompra=false
        elseif  gameState.tipodejuego==2 then
                utils.reproducirSonido("sounds/menu/introTrip")
                --viaje
                 --gameState.menuviaje=false
        elseif  gameState.tipodejuego==5 then
                utils.reproducirSonido("sounds/menu/introPo")
                --popi
                -- gameState.menupopi=false
        elseif  gameState.tipodejuego==4 then
                utils.reproducirSonido("sounds/menu/introSounds")
                --sonidos
                -- gameState.menupopi=false
        else
                utils.reproducirSonido("sounds/menu/introJ")
             --   gameState.menusonido=false
        end
        
        local pos1,pos2, pos3, pos4, pos5, pos6 = centerX/2, centerX, centerX*1.5, centerX/2, centerX, centerX*1.5
       
        local background = display.newImage( group,path.."fondo-1.png" )
        background:translate( centerX, centerY )
        local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
        
        -- or "images/lacompra/" or "images/popi/" or "images/lossonidos/" 
        if path ~= "images/lossonidos/" and 
            path ~= "images/lacompra/" and
            path ~= "images/popi/" and
            path ~= "images/elviaje/" then
                        
            local btnJ1 = display.newImage( group,path.."boton1.png" )
            btnJ1:translate( pos1, centerY*.78)
            btnJ1.xScale=.8
            btnJ1.yScale=.8
            btnJ1:addEventListener("tap", btnTap)
            btnJ1.destination = destino1
            btnJ1.name = name1
            btnJ1.tag = tag1
            
            local btnJ2 = display.newImage( group,path.."boton2.png" )
            btnJ2:translate( pos2, centerY*.78 )
            btnJ2.xScale=.8
            btnJ2.yScale=.8
            btnJ2:addEventListener("tap", btnTap)
            btnJ2.destination = destino2
            btnJ2.name = name2
            btnJ2.tag = tag2
            
            local btnJ3 = display.newImage(group,path.."boton3.png" )
            btnJ3:translate( pos3, centerY*.78)
            btnJ3.xScale=.8
            btnJ3.yScale=.8
            btnJ3:addEventListener("tap", btnTap)
            btnJ3.destination = destino3
            btnJ3.name = name3
            btnJ3.tag = tag3
            
            local btnJ4 = display.newImage( group,path.."boton4.png" )
            btnJ4:translate( pos4, centerY*1.43 )
            btnJ4.xScale=.8
            btnJ4.yScale=.8
            btnJ4:addEventListener("tap", btnTap)
            btnJ4.destination = destino4
            btnJ4.name = name4
            btnJ4.tag = tag4

            local btnJ5 = display.newImage( group,path.."boton5.png" )
            btnJ5:translate( pos5, centerY *1.43)
            btnJ5.xScale=.8
            btnJ5.yScale=.8
            btnJ5:addEventListener("tap", btnTap)
            btnJ5.destination = destino5
            btnJ5.name = name5
            btnJ5.tag = tag5
            
            local btnJ6 = display.newImage( group,path.."boton6.png" )
            btnJ6:translate( pos6, centerY*1.43)
            btnJ6.xScale=.8
            btnJ6.yScale=.8
            btnJ6:addEventListener("tap", btnTap)
            btnJ6.destination = destino6
            btnJ6.name = name6
            btnJ6.tag = tag6 

            local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
            btnInfo:translate( centerX*1.75, centerY*1.9 )
            btnInfo :addEventListener("tap", btnTap)
            btnInfo.destination = "extras.info_juego"
        else
                                   
            local btnJ1 = display.newImage( group,path.."boton1.png" )
            btnJ1:translate( centerX*.25, centerY)
            --btnJ1.xScale=.8
            --btnJ1.yScale=.8
            btnJ1:addEventListener("tap", btnTap)
            btnJ1.destination = destino1
            btnJ1.name = name1
            btnJ1.tag = tag1

            local btnJ2 = display.newImage( group,path.."boton2.png" )
            btnJ2:translate( centerX*.75, centerY )
            --btnJ2.xScale=.8
            --btnJ2.yScale=.8
            btnJ2:addEventListener("tap", btnTap)
            btnJ2.destination = destino2
            btnJ2.name = name2
            btnJ2.tag = tag2
            
            local btnJ3 = display.newImage(group,path.."boton3.png" )
            btnJ3:translate( centerX*1.25, centerY)
            --btnJ3.xScale=.8
            --btnJ3.yScale=.8
            btnJ3:addEventListener("tap", btnTap)
            btnJ3.destination = destino3
            btnJ3.name = name3
            btnJ3.tag = tag3
            
            local btnJ4 = display.newImage( group,path.."boton4.png" )
            btnJ4:translate( centerX*1.75, centerY )
            if path == "images/lossonidos/" then
            --btnJ4.xScale=.8
            --btnJ4.yScale=.8
            end
            btnJ4:addEventListener("tap", btnTap)
            btnJ4.destination = destino4
            btnJ4.name = name4
            btnJ4.tag = tag4

            local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
            btnInfo:translate( centerX*1.75, centerY*1.9 )
            btnInfo :addEventListener("tap", btnTap)
            btnInfo.destination = "extras.info_juego_ad"


        end

        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menus.menuCuento"
        btnRegresar.name = ""
        btnRegresar.tag = ""
            
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Choose a Game"  

        local musicafondo = audio.loadStream( "sounds/sounds/menu/fondo.mp3" )
        local musicafondochanel = audio.play( musicafondo, { channel=audioChannel, loops= -1 }  )      
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
    -- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
    local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
    local group = self.view
                
    composer.removeScene( composer.getSceneName( "current" ) )
    
    --Runtime:removeEventListener("enterFrame", update)
    -- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
    local group = self.view

    -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene---------------------------------------------------------
