local composer = require( "composer" )
local scene = composer.newScene()
local nube
local arriba = true

local function btnTap(event)
    audio.stop()

    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
   
   print(event.target.destination)
   return true
end

local function Paralax( )
	nube.x = nube.x - 3
        
        if (nube.x < -45 )  then
 		nube.x = (centerX*2)+45
 	end
        
        if (arriba)  then
            nube.y = nube.y - 3
            
            if (nube.y <= centerY*.05) then
                arriba = false
            end
        elseif not(arriba)  then
            nube.y = nube.y + 3
            
            if (nube.y >= centerY*.5) then
                arriba = true
            end
 	end

end

function scene:create( event )
    local group = self.view
    
    utils.reproducirSonido("sounds/home/home")
    --composer.removeScene( "bloqueo" )
    
    local background = display.newImage( group,"images/fondo.png" )
    background:translate( centerX, centerY )
    
    nube = display.newImage( group,"images/nube.png" )
    nube:translate( centerX*1.8 , centerY/2.5 )
    
    local btnNinos = display.newImage( group,"images/btn-ninos.png" )
    btnNinos:translate( centerX, centerY )
    btnNinos:addEventListener("tap", btnTap)
    btnNinos.destination = "menus.menu"
    
    local topsign = display.newImage( group,"images/top-sign.png" )
    topsign:translate( centerX, centerY/5 )
    
    local btnPadres = display.newImage( group,"images/btn-papas.png" )
    btnPadres:translate( centerX*1.75, centerY*1.85 )
    btnPadres:addEventListener("tap", btnTap)
    btnPadres.destination = "extras.papas"

    local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
    btnInfo :translate( centerX/6 ,centerY*1.85 )
    btnInfo :addEventListener("tap", btnTap)
    btnInfo.destination = "extras.info"

    local inst = display.newText( optionsTextMenu )
    group:insert(inst)
    
    inst.text="Welcome"
    
    local musicafondo = audio.loadStream( "sounds/sounds/home/fondo.mp3" )
    local musicafondochanel = audio.play( musicafondo, { channel=audioChannel, loops= -1 }  ) 
     
     Runtime:addEventListener("enterFrame", Paralax )
    
end
 
-- Called immediately after scene has moved onscreen:
function scene:show( event )
	local group = self.view
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
        
end
 
-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
        
        Runtime:removeEventListener("enterFrame", Paralax)
        composer.removeScene( composer.getSceneName( "current" ) )
 	collectgarbage()
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 
end
 
-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
 
end
 
---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
 
-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

