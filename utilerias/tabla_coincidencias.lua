-- Tabla de Palabras, solo para la creacion de la base de datos

    local tabla_coincidencias={}

function tabla_coincidencias.query()
    local query = 
    
        [[ 

             INSERT INTO coincidencias VALUES (1,'ventanas',1);
             INSERT INTO coincidencias VALUES (2,'ventana',1);
             INSERT INTO coincidencias VALUES (3,'ventanal',1);
             INSERT INTO coincidencias VALUES (4,'venta ana',1);


             INSERT INTO coincidencias VALUES (5,'puertas',2);
             INSERT INTO coincidencias VALUES (6,'huertas',2);
             INSERT INTO coincidencias VALUES (7,'muertas',2);
             INSERT INTO coincidencias VALUES (8,'puerta',2);
             INSERT INTO coincidencias VALUES (9,'cómo estas',2);


             INSERT INTO coincidencias VALUES (10,'hey on',3);
             INSERT INTO coincidencias VALUES (11,'mejillón',3);
             INSERT INTO coincidencias VALUES (12,'sillón',3);
             INSERT INTO coincidencias VALUES (13,'zhejiang',3);
             INSERT INTO coincidencias VALUES (14,'he ion',3);


             INSERT INTO coincidencias VALUES (15,'app',4);
             INSERT INTO coincidencias VALUES (16,'para pepe',4);
             INSERT INTO coincidencias VALUES (17,'para psp',4);
             INSERT INTO coincidencias VALUES (18,'para pc',4);
             INSERT INTO coincidencias VALUES (19,'a pepe',4);
             INSERT INTO coincidencias VALUES (20,'tt',4);
             INSERT INTO coincidencias VALUES (21,'tuenti',4);
             INSERT INTO coincidencias VALUES (22,'gente',4);
             INSERT INTO coincidencias VALUES (23,'hackkett',4);
             INSERT INTO coincidencias VALUES (24,'chapete',4);
             INSERT INTO coincidencias VALUES (25,'chaquet',4);
             INSERT INTO coincidencias VALUES (26,'chapter',4);


             INSERT INTO coincidencias VALUES (27,'cochinito',5);
             INSERT INTO coincidencias VALUES (28,'chinito',5);
             INSERT INTO coincidencias VALUES (29,'chinitito',5);
             INSERT INTO coincidencias VALUES (30,'chinitos',5);
             INSERT INTO coincidencias VALUES (31,'hot chinito',5);


             INSERT INTO coincidencias VALUES (32,'flores',6);
             INSERT INTO coincidencias VALUES (33,'florence',6);
             INSERT INTO coincidencias VALUES (34,'floren',6);
             INSERT INTO coincidencias VALUES (35,'florero',6);
             INSERT INTO coincidencias VALUES (36,'flórez',6);
             INSERT INTO coincidencias VALUES (37,'florez',6);


             INSERT INTO coincidencias VALUES (38,'arbusto',7);
             INSERT INTO coincidencias VALUES (39,'arbustos',7);
             INSERT INTO coincidencias VALUES (40,'al gusto',7);
             INSERT INTO coincidencias VALUES (41,'al busto',7);


             INSERT INTO coincidencias VALUES (42,'vino',8);
             INSERT INTO coincidencias VALUES (43,'vinos',8);
             INSERT INTO coincidencias VALUES (44,'tino',8);
             INSERT INTO coincidencias VALUES (45,'chino',8);
             INSERT INTO coincidencias VALUES (46,'pino',8);


             INSERT INTO coincidencias VALUES (47,'wow',9);
             INSERT INTO coincidencias VALUES (48,'tráfico',9);
             INSERT INTO coincidencias VALUES (49,'plástico',9);
             INSERT INTO coincidencias VALUES (50,'ratico',9);
             INSERT INTO coincidencias VALUES (51,'cántico',9);
             INSERT INTO coincidencias VALUES (52,'gráfico',9);
             INSERT INTO coincidencias VALUES (53,'iko iko',9);
             INSERT INTO coincidencias VALUES (54,'5 tráfico',9);
             INSERT INTO coincidencias VALUES (55,'practico',9);
             INSERT INTO coincidencias VALUES (56,'prácticum',9);
             INSERT INTO coincidencias VALUES (57,'prágmatico',9);             
             
             INSERT INTO coincidencias VALUES (58,'kiko',9);
             INSERT INTO coincidencias VALUES (59,'francisco',9);
             INSERT INTO coincidencias VALUES (60,'disco',9);
             INSERT INTO coincidencias VALUES (61,'circo',9);
             INSERT INTO coincidencias VALUES (62,'chico',9);


             INSERT INTO coincidencias VALUES (63,'niña iniesta',10);
             INSERT INTO coincidencias VALUES (64,'violinista violinistas',10);
             INSERT INTO coincidencias VALUES (65,'violines style',10);
             INSERT INTO coincidencias VALUES (66,'violín just style',10);
             INSERT INTO coincidencias VALUES (67,'violín y orquesta',10);
             

             INSERT INTO coincidencias VALUES (68,'autista',11);
             INSERT INTO coincidencias VALUES (69,'flauta',11);
             INSERT INTO coincidencias VALUES (70,'flaws',11);
             INSERT INTO coincidencias VALUES (71,'flaws tío',11);
             INSERT INTO coincidencias VALUES (72,'flaw',11);
             INSERT INTO coincidencias VALUES (73,'flautist',11);


             INSERT INTO coincidencias VALUES (74,'roja',12);
             INSERT INTO coincidencias VALUES (75,'rosas',12);
             INSERT INTO coincidencias VALUES (76,'rosa',12);
             INSERT INTO coincidencias VALUES (77,'rozas',12);


             INSERT INTO coincidencias VALUES (78,'blanco',13);
             INSERT INTO coincidencias VALUES (79,'manco',13);
             INSERT INTO coincidencias VALUES (80,'mango',13);
             INSERT INTO coincidencias VALUES (81,'bano',13);
             INSERT INTO coincidencias VALUES (82,'baño',13);


             INSERT INTO coincidencias VALUES (83,'paja',14);
             INSERT INTO coincidencias VALUES (84,'pajas',14);
             INSERT INTO coincidencias VALUES (85,'ajá',14);
             INSERT INTO coincidencias VALUES (86,'pájaro',14);


             INSERT INTO coincidencias VALUES (87,'juan paja',15);
             INSERT INTO coincidencias VALUES (88,'juan pantoja',15);
             INSERT INTO coincidencias VALUES (89,'paja',15);
             INSERT INTO coincidencias VALUES (90,'fondo paja',15);
             INSERT INTO coincidencias VALUES (91,'juego paja',15);
             INSERT INTO coincidencias VALUES (92,'pon paja',15);
             INSERT INTO coincidencias VALUES (93,'tom paja',15);
             INSERT INTO coincidencias VALUES (94,'pompa ja',15);
             INSERT INTO coincidencias VALUES (95,'pompa',15);
             INSERT INTO coincidencias VALUES (96,'pom paja',15);
             

             INSERT INTO coincidencias VALUES (97,'palos',16);
             INSERT INTO coincidencias VALUES (98,'palo',16);
             INSERT INTO coincidencias VALUES (99,'para los',16);
             INSERT INTO coincidencias VALUES (100,'a los',16);


             INSERT INTO coincidencias VALUES (101,'lagrimeo',17);
             INSERT INTO coincidencias VALUES (102,'ladrillo',17);
             INSERT INTO coincidencias VALUES (103,'la gringa', 17);
             INSERT INTO coincidencias VALUES (104,'la gripe', 17);
             INSERT INTO coincidencias VALUES (105,'la grande',17);


                    	 
             INSERT INTO coincidencias VALUES (106,'mesa',18);
             INSERT INTO coincidencias VALUES (107,'mesas',18);
             INSERT INTO coincidencias VALUES (108,'mesa',18);
             INSERT INTO coincidencias VALUES (109,'meza',18);
             INSERT INTO coincidencias VALUES (110,'me sa',18);
             INSERT INTO coincidencias VALUES (111,'meisa',18);
             INSERT INTO coincidencias VALUES (112,'feisa',18);
             INSERT INTO coincidencias VALUES (113,'feixa',18);



             INSERT INTO coincidencias VALUES (114,'howard',19);
             INSERT INTO coincidencias VALUES (115,'guarde', 19);
             INSERT INTO coincidencias VALUES (116,'josé', 19);
             INSERT INTO coincidencias VALUES (117,'cuadros',19);
             INSERT INTO coincidencias VALUES (118,'cuadro',19);
             
             INSERT INTO coincidencias VALUES (119,'camila',20);
             INSERT INTO coincidencias VALUES (120,'caminar',20);
             INSERT INTO coincidencias VALUES (121,'camina',20);
             INSERT INTO coincidencias VALUES (122,'camilina',20);
             INSERT INTO coincidencias VALUES (123,'jamilena',20); 
             
             INSERT INTO coincidencias VALUES (124,'ya está',21);
             INSERT INTO coincidencias VALUES (125,'ya estás',21);
             INSERT INTO coincidencias VALUES (126,'ya esta',21);
             INSERT INTO coincidencias VALUES (127,'estás',21);
             INSERT INTO coincidencias VALUES (128,'salta',21);
             INSERT INTO coincidencias VALUES (129,'sala',21);
             INSERT INTO coincidencias VALUES (130,'sexta',21);
             INSERT INTO coincidencias VALUES (131,'saica',21);
             INSERT INTO coincidencias VALUES (132,'salta',21);
             INSERT INTO coincidencias VALUES (133,'taika',21);
             INSERT INTO coincidencias VALUES (134,'saltar',21);
             
             INSERT INTO coincidencias VALUES (135,'qué tal',22);
             INSERT INTO coincidencias VALUES (136,'qué tal as',22);
             INSERT INTO coincidencias VALUES (137,'que tal',22);
             INSERT INTO coincidencias VALUES (138,'qué tal ha',22);
             INSERT INTO coincidencias VALUES (139,'qué estás',22);
             INSERT INTO coincidencias VALUES (140,'kettal',22);
             INSERT INTO coincidencias VALUES (141,'que tal',22);
             INSERT INTO coincidencias VALUES (142,'que esta',22);
             INSERT INTO coincidencias VALUES (143,'qué estás',22);
             
             INSERT INTO coincidencias VALUES (144,'escoge',23);
             INSERT INTO coincidencias VALUES (145,'escoger',23);
             INSERT INTO coincidencias VALUES (146,'coge',23);
             INSERT INTO coincidencias VALUES (147,'se coge',23);
             INSERT INTO coincidencias VALUES (148,'se coje',23);
             INSERT INTO coincidencias VALUES (149,'recoge',23);
             INSERT INTO coincidencias VALUES (150,'recoger',23);
             INSERT INTO coincidencias VALUES (151,'recoges',23);
             INSERT INTO coincidencias VALUES (152,'recajo',23);
             INSERT INTO coincidencias VALUES (153,'recas',23);
             INSERT INTO coincidencias VALUES (154,'recoja',23);
             INSERT INTO coincidencias VALUES (155,'recarga',23);
             INSERT INTO coincidencias VALUES (156,'re caja',23);

             INSERT INTO coincidencias VALUES (157,'google',24);
             INSERT INTO coincidencias VALUES (158,'cover',24);
             INSERT INTO coincidencias VALUES (159,'kobe',24);
             INSERT INTO coincidencias VALUES (160,'gogle',24);
             INSERT INTO coincidencias VALUES (161,'coves',24);
             INSERT INTO coincidencias VALUES (162,'corre',24);
             INSERT INTO coincidencias VALUES (163,'corroe',24);
             INSERT INTO coincidencias VALUES (164,'torre',24);
             INSERT INTO coincidencias VALUES (165,'hotmail',24);
             INSERT INTO coincidencias VALUES (166,'jódar',24);
             INSERT INTO coincidencias VALUES (167,'joda',24);
             INSERT INTO coincidencias VALUES (168,'josé',24);
            
             INSERT INTO coincidencias VALUES (169,'caca',25);
             INSERT INTO coincidencias VALUES (170,'kaká',25);
             INSERT INTO coincidencias VALUES (171,'kk',25);
             INSERT INTO coincidencias VALUES (172,'carta',25);
             INSERT INTO coincidencias VALUES (173,'toca',25);
             INSERT INTO coincidencias VALUES (174,'foca',25);
             INSERT INTO coincidencias VALUES (175,'sopcast',25);
             INSERT INTO coincidencias VALUES (176,'tocan',25);
             
             
             INSERT INTO coincidencias VALUES (177,'chile',26);
             INSERT INTO coincidencias VALUES (178,'tele',26);
             INSERT INTO coincidencias VALUES (179,'hitler',26);
             INSERT INTO coincidencias VALUES (180,'lne',26);
             INSERT INTO coincidencias VALUES (181,'prende',26);
             INSERT INTO coincidencias VALUES (182,'pene',26);
             INSERT INTO coincidencias VALUES (183,'penes',26);
             INSERT INTO coincidencias VALUES (184,'pende',26);
             INSERT INTO coincidencias VALUES (185,'prendes',26);
             INSERT INTO coincidencias VALUES (186,'prenden',26);
             INSERT INTO coincidencias VALUES (187,'prender',26);
             INSERT INTO coincidencias VALUES (188,'por ende',26);
             
             INSERT INTO coincidencias VALUES (189,'camino a la cocina', 27);
             INSERT INTO coincidencias VALUES (190,'caminar a la cocina', 27);
             INSERT INTO coincidencias VALUES (191,'camina en la cocina', 27);
             INSERT INTO coincidencias VALUES (192,'caminan en la cocina', 27);
             INSERT INTO coincidencias VALUES (193,'caminar en la cocina', 27);
             INSERT INTO coincidencias VALUES (194,'caminar en la cocin', 27);
             INSERT INTO coincidencias VALUES (195,'camino a la ina', 27);
             INSERT INTO coincidencias VALUES (196,'camino a la', 27);
             INSERT INTO coincidencias VALUES (197,'camino a la oficina', 27);
             
             INSERT INTO coincidencias VALUES (198,'tomó la olla',28);
             INSERT INTO coincidencias VALUES (199,'toma la olla',28);
             INSERT INTO coincidencias VALUES (200,'toma la horts',28);
             INSERT INTO coincidencias VALUES (201,'toma la hora',28);
             INSERT INTO coincidencias VALUES (202,'tómo la 8',28);
             INSERT INTO coincidencias VALUES (203,'tomo la 8',28);
             INSERT INTO coincidencias VALUES (204,'tómo la otxa',28);
             
             INSERT INTO coincidencias VALUES (205,'camina a la chimenea',29);
             INSERT INTO coincidencias VALUES (206,'camina a las chimeneas',29);
             INSERT INTO coincidencias VALUES (207,'camino a la chimenea',29);
             INSERT INTO coincidencias VALUES (208,'amina a la chimenea',29);
             INSERT INTO coincidencias VALUES (209,'amino a la chimenea',29);
             INSERT INTO coincidencias VALUES (210,'a mí no a la chimenea',29);
             INSERT INTO coincidencias VALUES (211,'aminas a la chimenea',29);
             INSERT INTO coincidencias VALUES (212,'a mí ni a la chimenea.',29);
             
             INSERT INTO coincidencias VALUES (213,'caliente',30);
             INSERT INTO coincidencias VALUES (214,'calientes',30);
             INSERT INTO coincidencias VALUES (215,'calienta',30);
             INSERT INTO coincidencias VALUES (216,'alienta',30);
             INSERT INTO coincidencias VALUES (217,'calientas',30);
             INSERT INTO coincidencias VALUES (218,'alienta',30);
             INSERT INTO coincidencias VALUES (219,'aliento',30);
             INSERT INTO coincidencias VALUES (220,'saliente',30);
             INSERT INTO coincidencias VALUES (221,'aliente',30);
             INSERT INTO coincidencias VALUES (222,'aliens',30);
                         
             INSERT INTO coincidencias VALUES (223,'g',31);
             INSERT INTO coincidencias VALUES (224,'je',31);
             INSERT INTO coincidencias VALUES (225,'c',31);
             INSERT INTO coincidencias VALUES (226,'casa',31);
             INSERT INTO coincidencias VALUES (227,'café',31);
             INSERT INTO coincidencias VALUES (228,'caf',31);
             INSERT INTO coincidencias VALUES (229,'cafe',31);
                          
             INSERT INTO coincidencias VALUES (230,'chaca',32);
             INSERT INTO coincidencias VALUES (231,'casa',32);
             INSERT INTO coincidencias VALUES (232,'chao',32);
             INSERT INTO coincidencias VALUES (233,'oa',32);
             INSERT INTO coincidencias VALUES (234,'kaká',32);
             INSERT INTO coincidencias VALUES (235,'caca',32);
             INSERT INTO coincidencias VALUES (236,'coca',32);

             INSERT INTO coincidencias VALUES (237,'hella',33);
             INSERT INTO coincidencias VALUES (238,'gela',33);
             INSERT INTO coincidencias VALUES (239,'telas',33);
             INSERT INTO coincidencias VALUES (240,'tela',33);
             INSERT INTO coincidencias VALUES (241,'pera',33);
             INSERT INTO coincidencias VALUES (242,'pela',33);
             INSERT INTO coincidencias VALUES (243,'terra',33);
             INSERT INTO coincidencias VALUES (244,'pero',33);
             INSERT INTO coincidencias VALUES (245,'telar',33);

             INSERT INTO coincidencias VALUES (246,'alabaza',34);
             INSERT INTO coincidencias VALUES (247,'calabaza',34);
             INSERT INTO coincidencias VALUES (248,'cala bassa',34);
             INSERT INTO coincidencias VALUES (249,'carabasa',34);
             INSERT INTO coincidencias VALUES (250,'caracava',34);


             INSERT INTO coincidencias VALUES (251,'barquilla',35);
             INSERT INTO coincidencias VALUES (252,'cartilla',35);
             INSERT INTO coincidencias VALUES (253,'arquía',35);
             INSERT INTO coincidencias VALUES (254,'partía',35);
             INSERT INTO coincidencias VALUES (255,'quíla',35);
             INSERT INTO coincidencias VALUES (256,'cortila',35);
             INSERT INTO coincidencias VALUES (257,'cortina',35);
             INSERT INTO coincidencias VALUES (258,'totila',35);
             INSERT INTO coincidencias VALUES (259,'totilla',35);
             
             INSERT INTO coincidencias VALUES (260,'harley. gla',36);
             INSERT INTO coincidencias VALUES (261,'henley',36);
             INSERT INTO coincidencias VALUES (262,'hanley',36);
             INSERT INTO coincidencias VALUES (263,'helle',36);
             INSERT INTO coincidencias VALUES (264,'chalet. carne',36);
             INSERT INTO coincidencias VALUES (265,'carnet',36);
             INSERT INTO coincidencias VALUES (266,'chandler',36);
             INSERT INTO coincidencias VALUES (267,'carl',36);

            
            INSERT INTO coincidencias VALUES (268,'caso gárate',37);
            INSERT INTO coincidencias VALUES (269,'talaso white',37);
            INSERT INTO coincidencias VALUES (270,'caso guateque caso white',37);
            INSERT INTO coincidencias VALUES (271,'arahuetes',37);
            INSERT INTO coincidencias VALUES (272,'aragua',37);
            INSERT INTO coincidencias VALUES (273,'has o haz',37);
            INSERT INTO coincidencias VALUES (274,'aro 17',37);
            INSERT INTO coincidencias VALUES (275,'cacahuates ',37);
            INSERT INTO coincidencias VALUES (276,'cacahuete.',37);
            INSERT INTO coincidencias VALUES (277,'caso guateque',37);
            INSERT INTO coincidencias VALUES (278,'caso white',37);

            INSERT INTO coincidencias VALUES (279,'papas',38);
            INSERT INTO coincidencias VALUES (280,'zapatos',38);
            INSERT INTO coincidencias VALUES (281,'capaz',38);
            INSERT INTO coincidencias VALUES (282,'papá',38);
            INSERT INTO coincidencias VALUES (283,'papa',38);
            INSERT INTO coincidencias VALUES (284,'capa',38);
            INSERT INTO coincidencias VALUES (285,'cap',38);
            INSERT INTO coincidencias VALUES (286,'cup',38);
            INSERT INTO coincidencias VALUES (287,'casp',38);

            INSERT INTO coincidencias VALUES (288,'chocolat',39);
            INSERT INTO coincidencias VALUES (289,'chocolates',39);
            INSERT INTO coincidencias VALUES (290,'chocolatero',39);
            INSERT INTO coincidencias VALUES (291,'yo karate',39);
            INSERT INTO coincidencias VALUES (292,'karate',39);

            INSERT INTO coincidencias VALUES (293,'ages',40);
            INSERT INTO coincidencias VALUES (294,'eje',40);
            INSERT INTO coincidencias VALUES (295,'eche',40);
            INSERT INTO coincidencias VALUES (296,'leche',40);
            INSERT INTO coincidencias VALUES (297,'leches',40);
            INSERT INTO coincidencias VALUES (298,'lecce',40);
            INSERT INTO coincidencias VALUES (299,'le eches',40);
            INSERT INTO coincidencias VALUES (300,'le eche.',40);
            
            
            INSERT INTO coincidencias VALUES (301,'calvito',41);
            INSERT INTO coincidencias VALUES (302,'caldito',41);
            INSERT INTO coincidencias VALUES (303,'caballito',41);
            INSERT INTO coincidencias VALUES (304,'cariñito',41);
            INSERT INTO coincidencias VALUES (305,'caballito',41);
            INSERT INTO coincidencias VALUES (306,'carrito',41);
            INSERT INTO coincidencias VALUES (307,'carito',41);
            INSERT INTO coincidencias VALUES (308,'calito',41);
            INSERT INTO coincidencias VALUES (309,'carritos',41);
            INSERT INTO coincidencias VALUES (310,'calitos',41);
            INSERT INTO coincidencias VALUES (311,'caritos',41);

            INSERT INTO coincidencias VALUES (312,'futas',42);
            INSERT INTO coincidencias VALUES (313,'fruta',42);
            INSERT INTO coincidencias VALUES (314,'frutales',42);
            INSERT INTO coincidencias VALUES (315,'frutal',42);
            INSERT INTO coincidencias VALUES (316,'fut',42);
            INSERT INTO coincidencias VALUES (317,'food',42);
            INSERT INTO coincidencias VALUES (318,'foot',42);
            INSERT INTO coincidencias VALUES (319,'futa',42);
            
            
            INSERT INTO coincidencias VALUES (320,'swatch',43);
            INSERT INTO coincidencias VALUES (321,'soacha',43);
            INSERT INTO coincidencias VALUES (322,'sweps',43);
            INSERT INTO coincidencias VALUES (323,'twiter',43);
            INSERT INTO coincidencias VALUES (324,'shutter',43);
            INSERT INTO coincidencias VALUES (325,'sueter',43);
            INSERT INTO coincidencias VALUES (326,'setter',43);
            
            INSERT INTO coincidencias VALUES (327,'para ella',44);
            INSERT INTO coincidencias VALUES (328,'playa ya',44);
            INSERT INTO coincidencias VALUES (329,'para ellas',44);
            INSERT INTO coincidencias VALUES (330,'era ella',44);
            INSERT INTO coincidencias VALUES (331,'playeras',44);
            INSERT INTO coincidencias VALUES (332,'ayer',44);
            INSERT INTO coincidencias VALUES (333,'jayena',44);


            INSERT INTO coincidencias VALUES (334,'saco',45);
            INSERT INTO coincidencias VALUES (335,'saca',45);
            INSERT INTO coincidencias VALUES (336,'chaco',45);
            INSERT INTO coincidencias VALUES (337,'sacos',45);
            INSERT INTO coincidencias VALUES (338,'sacó',45);
            INSERT INTO coincidencias VALUES (339,'sac',45);
            INSERT INTO coincidencias VALUES (340,'zac',45);
            INSERT INTO coincidencias VALUES (341,'suck',45);
            INSERT INTO coincidencias VALUES (342,'zack',45);
            INSERT INTO coincidencias VALUES (343,'zach',45);
            
            INSERT INTO coincidencias VALUES (344,'pasa que tal',46);
            INSERT INTO coincidencias VALUES (345,'qué tal',46);
            INSERT INTO coincidencias VALUES (346,'pasa qué tal',46);
            INSERT INTO coincidencias VALUES (347,'payasa qué tal',46);

            INSERT INTO coincidencias VALUES (348,'sepelio',47);
            INSERT INTO coincidencias VALUES (349,'cepillo',47);
            INSERT INTO coincidencias VALUES (350,'sepe y yo',47);
            INSERT INTO coincidencias VALUES (351,'cp y yo',47);
            INSERT INTO coincidencias VALUES (352,'sepei y yo',47);
            INSERT INTO coincidencias VALUES (353,'sátiro',47);
            INSERT INTO coincidencias VALUES (354,'tío',47);
            INSERT INTO coincidencias VALUES (355,'video',47);
            INSERT INTO coincidencias VALUES (356,'sabio',47);
            INSERT INTO coincidencias VALUES (357,'saltillo',47);
            INSERT INTO coincidencias VALUES (358,'qué tío',47);
            INSERT INTO coincidencias VALUES (359,'qué tía',47);

            INSERT INTO coincidencias VALUES (360,'lusa azul',48);
            INSERT INTO coincidencias VALUES (361,'blusa azul',48);
            INSERT INTO coincidencias VALUES (362,'usa azul',48);
            INSERT INTO coincidencias VALUES (363,'blusa a azul',48);
            INSERT INTO coincidencias VALUES (364,'tuzsa azul',48);
            INSERT INTO coincidencias VALUES (365,'blusa al sur',48);
            INSERT INTO coincidencias VALUES (366,'blusas al sur',48);
            INSERT INTO coincidencias VALUES (367,'blues al sur',48);


            INSERT INTO coincidencias VALUES (368,'calcetines',49);
            INSERT INTO coincidencias VALUES (369,'calcetin',49);
            INSERT INTO coincidencias VALUES (370,'casa',49);
            INSERT INTO coincidencias VALUES (371,'cassette',49);
            INSERT INTO coincidencias VALUES (372,'caso',49);

            INSERT INTO coincidencias VALUES (373,'zapatos',50);
            INSERT INTO coincidencias VALUES (374,'sap',50);
            INSERT INTO coincidencias VALUES (375,'sap para dos',50);
            INSERT INTO coincidencias VALUES (376,'zap tos',50);
            INSERT INTO coincidencias VALUES (377,'patos',50);

            INSERT INTO coincidencias VALUES (378,'transporte',51);
            INSERT INTO coincidencias VALUES (379,'last post',51);
            INSERT INTO coincidencias VALUES (380,'las potter',51);
            INSERT INTO coincidencias VALUES (381,'raz potter',51);
            INSERT INTO coincidencias VALUES (382,'rush poster',51);
            INSERT INTO coincidencias VALUES (383,'transporter',51);
            INSERT INTO coincidencias VALUES (384,'transport',51);
            INSERT INTO coincidencias VALUES (385,'transportes',51);
            
            INSERT INTO coincidencias VALUES (386,'chen',52);
            INSERT INTO coincidencias VALUES (387,'en',52);
            INSERT INTO coincidencias VALUES (388,'zen',52);
            INSERT INTO coincidencias VALUES (389,'ten',52);
            INSERT INTO coincidencias VALUES (390,'tren',52);
            INSERT INTO coincidencias VALUES (391,'change',52);
            INSERT INTO coincidencias VALUES (392,'saint',52);
            
            INSERT INTO coincidencias VALUES (393,'barc',53);
            INSERT INTO coincidencias VALUES (394,'barco',53);
            INSERT INTO coincidencias VALUES (395,'barcos',53);
            INSERT INTO coincidencias VALUES (396,'pasco',53);
            INSERT INTO coincidencias VALUES (397,'vasco',53);
            INSERT INTO coincidencias VALUES (398,'asco',53);
            INSERT INTO coincidencias VALUES (399,'park',53);
            INSERT INTO coincidencias VALUES (400,'arch',53);
            INSERT INTO coincidencias VALUES (401,'bar',53);
            INSERT INTO coincidencias VALUES (402,'bart',53);
            INSERT INTO coincidencias VALUES (403,'marca',53);
            INSERT INTO coincidencias VALUES (404,'var',53);

            INSERT INTO coincidencias VALUES (405,'autobuses',54);
            INSERT INTO coincidencias VALUES (406,'autobús',54);
            INSERT INTO coincidencias VALUES (407,'audio bus',54);
            INSERT INTO coincidencias VALUES (408,'uvus',54);
            INSERT INTO coincidencias VALUES (409,'obus',54);
            
            INSERT INTO coincidencias VALUES (410,'ion',55);
            INSERT INTO coincidencias VALUES (411,'ileon',55);
            INSERT INTO coincidencias VALUES (412,'iron',55);
            INSERT INTO coincidencias VALUES (413,'aion',55);
            INSERT INTO coincidencias VALUES (414,'león',55);
            INSERT INTO coincidencias VALUES (415,'avión',55);
            INSERT INTO coincidencias VALUES (416,'acción',55);

            INSERT INTO coincidencias VALUES (417,'destinos',56);
            INSERT INTO coincidencias VALUES (418,'destino',56);
            INSERT INTO coincidencias VALUES (419,'testino',56);
            INSERT INTO coincidencias VALUES (420,'tetina',56);
            INSERT INTO coincidencias VALUES (421,'esti no',56);

            INSERT INTO coincidencias VALUES (422,'um',57);
            INSERT INTO coincidencias VALUES (423,'aún',57);
            INSERT INTO coincidencias VALUES (424,'un',57);
            INSERT INTO coincidencias VALUES (425,'a un',57);
            INSERT INTO coincidencias VALUES (426,'cam',57);
            INSERT INTO coincidencias VALUES (427,'cancún',57);
            INSERT INTO coincidencias VALUES (428,'canción',57);
            INSERT INTO coincidencias VALUES (429,'canco',57);

            INSERT INTO coincidencias VALUES (430,'pueblo',58);
            INSERT INTO coincidencias VALUES (431,'puebla',58);
            INSERT INTO coincidencias VALUES (432,'tebla',58);
            INSERT INTO coincidencias VALUES (433,'pela',58);
            INSERT INTO coincidencias VALUES (434,'puela',58);

            INSERT INTO coincidencias VALUES (435,'monterrey',59);
            INSERT INTO coincidencias VALUES (436,'montaje del rey',59);
            INSERT INTO coincidencias VALUES (437,'moon r',59);
            INSERT INTO coincidencias VALUES (438,'obd',59);
            INSERT INTO coincidencias VALUES (439,'he',59);


            INSERT INTO coincidencias VALUES (440,'chapas',60);
            INSERT INTO coincidencias VALUES (441,'chat',60);
            INSERT INTO coincidencias VALUES (442,'xataka',60);
            INSERT INTO coincidencias VALUES (443,'chapa',60);
            INSERT INTO coincidencias VALUES (444,'chaval',60);
            INSERT INTO coincidencias VALUES (445,'app',60);
            INSERT INTO coincidencias VALUES (446,'up',60);
            INSERT INTO coincidencias VALUES (447,'y',60);
            INSERT INTO coincidencias VALUES (448,'y app',60);

            INSERT INTO coincidencias VALUES (449,'y ah oo',61);
            INSERT INTO coincidencias VALUES (450,'ia o',61);
            INSERT INTO coincidencias VALUES (451,'y ahoo',61);
            INSERT INTO coincidencias VALUES (452,'ia',61);
            INSERT INTO coincidencias VALUES (453,'y',61);
            INSERT INTO coincidencias VALUES (454,'sin olor',61);
            INSERT INTO coincidencias VALUES (455,'sinaloa',61);

            INSERT INTO coincidencias VALUES (456,'habbo',62);
            INSERT INTO coincidencias VALUES (457,'pago',62);
            INSERT INTO coincidencias VALUES (458,'jabón',62);
            INSERT INTO coincidencias VALUES (459,'pagos',62);
            INSERT INTO coincidencias VALUES (460,'pagó',62);
            INSERT INTO coincidencias VALUES (461,'java',62);
            INSERT INTO coincidencias VALUES (462,'javo',62);
            INSERT INTO coincidencias VALUES (463,'pavo',62);
            
            
            INSERT INTO coincidencias VALUES (464,'check',63);
            INSERT INTO coincidencias VALUES (465,'chat',63);
            INSERT INTO coincidencias VALUES (466,'chep',63);
            INSERT INTO coincidencias VALUES (467,'chucky',63);
            INSERT INTO coincidencias VALUES (468,'chuck',63);
            INSERT INTO coincidencias VALUES (469,'ets',63);
            INSERT INTO coincidencias VALUES (470,'x',63);
            INSERT INTO coincidencias VALUES (471,'ex',63);
            INSERT INTO coincidencias VALUES (472,'cheche',63);
            INSERT INTO coincidencias VALUES (473,'cheste',63);
            INSERT INTO coincidencias VALUES (474,'chester',63);
            INSERT INTO coincidencias VALUES (475,'che che che.',63);

            INSERT INTO coincidencias VALUES (476,'tarjeta',64);
            INSERT INTO coincidencias VALUES (477,'tarjetas',64);
            INSERT INTO coincidencias VALUES (478,'aeat',64);
            INSERT INTO coincidencias VALUES (479,'aemet',64);
            INSERT INTO coincidencias VALUES (480,'aea',64);
            INSERT INTO coincidencias VALUES (481,'aeam',64);
            INSERT INTO coincidencias VALUES (482,'asset',64);
            INSERT INTO coincidencias VALUES (483,'algete',64);
            INSERT INTO coincidencias VALUES (484,'azeta',64);
            INSERT INTO coincidencias VALUES (485,'asetra',64);
            INSERT INTO coincidencias VALUES (486,'acepta',64);

            INSERT INTO coincidencias VALUES (487,'es festivo',65);
            INSERT INTO coincidencias VALUES (488,'efectivo',65);
            INSERT INTO coincidencias VALUES (489,'s tío',65);
            INSERT INTO coincidencias VALUES (490,'ese tío',65);
            INSERT INTO coincidencias VALUES (491,'es el hijo',65);

            INSERT INTO coincidencias VALUES (492,'papá',66);
            INSERT INTO coincidencias VALUES (493,'caca',66);
            INSERT INTO coincidencias VALUES (494,'cap',66);
            INSERT INTO coincidencias VALUES (495,'pasta',66);
            INSERT INTO coincidencias VALUES (496,'pack',66);
            
            INSERT INTO coincidencias VALUES (497,'sol',71);
            INSERT INTO coincidencias VALUES (498,'scholl',71);
            INSERT INTO coincidencias VALUES (499,'son',71);
            INSERT INTO coincidencias VALUES (500,'hall',71);
            INSERT INTO coincidencias VALUES (501,'song',71);
            INSERT INTO coincidencias VALUES (502,'o',71);
            INSERT INTO coincidencias VALUES (503,'ol',71);
            INSERT INTO coincidencias VALUES (504,'hola',71);
            INSERT INTO coincidencias VALUES (505,'old',71);

            INSERT INTO coincidencias VALUES (506,'faldas',72);
            INSERT INTO coincidencias VALUES (507,'adidas',72);
            INSERT INTO coincidencias VALUES (508,'alda',72);
            INSERT INTO coincidencias VALUES (509,'adela',72);
            INSERT INTO coincidencias VALUES (510,'alzira',72);
            INSERT INTO coincidencias VALUES (511,'anda',72);
            INSERT INTO coincidencias VALUES (512,'ata',72);
            INSERT INTO coincidencias VALUES (513,'at',72);
            INSERT INTO coincidencias VALUES (514,'aeat',72);
            INSERT INTO coincidencias VALUES (515,'app',72);
            INSERT INTO coincidencias VALUES (516,'add',72);
            INSERT INTO coincidencias VALUES (517,'alta',72);
            INSERT INTO coincidencias VALUES (518,'alda',72);
            INSERT INTO coincidencias VALUES (519,'alba',72);
            
            INSERT INTO coincidencias VALUES (520,'soc',73);
            INSERT INTO coincidencias VALUES (521,'sac',73);
            INSERT INTO coincidencias VALUES (522,'suck',73);
            INSERT INTO coincidencias VALUES (523,'zach',73);

            INSERT INTO coincidencias VALUES (524,'hot',74);
            INSERT INTO coincidencias VALUES (525,'hope',74);
            INSERT INTO coincidencias VALUES (526,'hop',74);
            INSERT INTO coincidencias VALUES (527,'ok',74);
            INSERT INTO coincidencias VALUES (528,'yoc',74);
            INSERT INTO coincidencias VALUES (529,'fox',74);
            INSERT INTO coincidencias VALUES (530,'soc',74);
            INSERT INTO coincidencias VALUES (531,'foca',74);
            INSERT INTO coincidencias VALUES (532,'focs',74);
            INSERT INTO coincidencias VALUES (533,'fac',74);
            INSERT INTO coincidencias VALUES (534,'fosca',74);
            INSERT INTO coincidencias VALUES (535,'ford ka',74);

            INSERT INTO coincidencias VALUES (536,'frutas',75);
            INSERT INTO coincidencias VALUES (537,'fruit',75);
            INSERT INTO coincidencias VALUES (538,'fruta',75);
            INSERT INTO coincidencias VALUES (539,'fut',75);
            INSERT INTO coincidencias VALUES (540,'food',75);
            INSERT INTO coincidencias VALUES (541,'foot',75);
            INSERT INTO coincidencias VALUES (542,'suit',75);
            INSERT INTO coincidencias VALUES (543,'foot',75);
            
            INSERT INTO coincidencias VALUES (544,'sirenas',76);
            INSERT INTO coincidencias VALUES (545,'sirena',76);
            INSERT INTO coincidencias VALUES (546,'serena',76);
            INSERT INTO coincidencias VALUES (547,'elena',76);
            INSERT INTO coincidencias VALUES (548,'gerena',76);
            INSERT INTO coincidencias VALUES (549,'heorina',76);
            INSERT INTO coincidencias VALUES (550,'helena',76);
            INSERT INTO coincidencias VALUES (551,'ere na',76);
            INSERT INTO coincidencias VALUES (552,'sirens',76);

            INSERT INTO coincidencias VALUES (553,'filet',77);
            INSERT INTO coincidencias VALUES (554,'filete',77);
            INSERT INTO coincidencias VALUES (555,'filetes',77);
            INSERT INTO coincidencias VALUES (556,'gilet',77);
            INSERT INTO coincidencias VALUES (557,'gillette',77);
            INSERT INTO coincidencias VALUES (558,'ie',77);
            INSERT INTO coincidencias VALUES (559,'ieee',77);


            INSERT INTO coincidencias VALUES (560,'salchichas',78);
            INSERT INTO coincidencias VALUES (561,'salchichón',78);
            INSERT INTO coincidencias VALUES (562,'salchicha',78);
            INSERT INTO coincidencias VALUES (563,'hay chicha',78);
            INSERT INTO coincidencias VALUES (564,'al chicho',78);
            INSERT INTO coincidencias VALUES (565,'al chico',78);
            INSERT INTO coincidencias VALUES (566,'al chicharo',78);


            INSERT INTO coincidencias VALUES (567,'sopa',79);
            INSERT INTO coincidencias VALUES (568,'opa',79);
            INSERT INTO coincidencias VALUES (569,'op',79);
            INSERT INTO coincidencias VALUES (570,'pop',79);
            INSERT INTO coincidencias VALUES (571,'popa',79);
            INSERT INTO coincidencias VALUES (572,'hop',79);
            INSERT INTO coincidencias VALUES (573,'opw',79);
            INSERT INTO coincidencias VALUES (574,'chop',79);
            INSERT INTO coincidencias VALUES (575,'ropa',79);
            INSERT INTO coincidencias VALUES (576,'sop',79);
            INSERT INTO coincidencias VALUES (577,'shop',79);
            
            INSERT INTO coincidencias VALUES (578,'familia',80);
            INSERT INTO coincidencias VALUES (579,'amilia',80);
            INSERT INTO coincidencias VALUES (580,'amelia',80);
            INSERT INTO coincidencias VALUES (581,'a million',80);
            INSERT INTO coincidencias VALUES (582,'jamilena',80);
            INSERT INTO coincidencias VALUES (583,'chamilia',80);
            
            INSERT INTO coincidencias VALUES (585,'china',84);
            INSERT INTO coincidencias VALUES (586,'tina',84);
            INSERT INTO coincidencias VALUES (587,'cine',84);
            INSERT INTO coincidencias VALUES (588,'trina',84);
            INSERT INTO coincidencias VALUES (589,'fina',84);
            INSERT INTO coincidencias VALUES (590,'inns',84);
            INSERT INTO coincidencias VALUES (591,'inem',84);
            INSERT INTO coincidencias VALUES (592,'ina',84);
            INSERT INTO coincidencias VALUES (593,'ine',84);
            INSERT INTO coincidencias VALUES (594,'inap',84);
            
            INSERT INTO coincidencias VALUES (595,'talla',85);
            INSERT INTO coincidencias VALUES (596,'kayak',85);
            INSERT INTO coincidencias VALUES (597,'para allá',85);
            INSERT INTO coincidencias VALUES (598,'toalla',85);
            INSERT INTO coincidencias VALUES (599,'calla',85);
            INSERT INTO coincidencias VALUES (600,'falla',85);
            INSERT INTO coincidencias VALUES (601,'gaia',85);
            INSERT INTO coincidencias VALUES (602,'palla',85);
            INSERT INTO coincidencias VALUES (603,'jaiak',85);
            INSERT INTO coincidencias VALUES (604,'hyatt',85);
            INSERT INTO coincidencias VALUES (605,'allá',85);
            INSERT INTO coincidencias VALUES (606,'hi ha',85);


            INSERT INTO coincidencias VALUES (607,'jabón',86);
            INSERT INTO coincidencias VALUES (608,'avón',86);
            INSERT INTO coincidencias VALUES (609,'japón',86);
            INSERT INTO coincidencias VALUES (610,'upon',86);
            INSERT INTO coincidencias VALUES (611,'up on',86);
            INSERT INTO coincidencias VALUES (612,'app on',86);

            INSERT INTO coincidencias VALUES (613,'cortina',87);
            INSERT INTO coincidencias VALUES (614,'cortinas',87);
            INSERT INTO coincidencias VALUES (615,'hot inna',87);
            INSERT INTO coincidencias VALUES (616,'inna',87);
            INSERT INTO coincidencias VALUES (617,'inem',87);
            INSERT INTO coincidencias VALUES (618,'irina',87);
            INSERT INTO coincidencias VALUES (619,'in a',87);
            INSERT INTO coincidencias VALUES (620,'opina',87);
            INSERT INTO coincidencias VALUES (621,'okina',87);
            INSERT INTO coincidencias VALUES (622,'orina',87);
            INSERT INTO coincidencias VALUES (623,'tina',87);
            INSERT INTO coincidencias VALUES (624,'chinas',87);

            INSERT INTO coincidencias VALUES (625,'champu',88);
            INSERT INTO coincidencias VALUES (626,'tampu',88);
            INSERT INTO coincidencias VALUES (627,'ampu',88);
            INSERT INTO coincidencias VALUES (628,'ampans',88);
            INSERT INTO coincidencias VALUES (629,'ampo',88);
            INSERT INTO coincidencias VALUES (630,'campu',88);
            INSERT INTO coincidencias VALUES (631,'campus',88);
            
            INSERT INTO coincidencias VALUES (632,'piece',89);
            INSERT INTO coincidencias VALUES (633,'piso',89);
            INSERT INTO coincidencias VALUES (634,'pics',89);
            INSERT INTO coincidencias VALUES (635,'pis',89);
            INSERT INTO coincidencias VALUES (636,'pisos',89);
            INSERT INTO coincidencias VALUES (637,'ipod',89);
            INSERT INTO coincidencias VALUES (638,'ikea',89);
            INSERT INTO coincidencias VALUES (639,'icloud',89);
            INSERT INTO coincidencias VALUES (640,'icot',89);
            INSERT INTO coincidencias VALUES (641,'ipod',89);
            
            INSERT INTO coincidencias VALUES (642,'espejos',90);
            INSERT INTO coincidencias VALUES (643,'espejo',90);
            INSERT INTO coincidencias VALUES (644,'gipe',90);
            INSERT INTO coincidencias VALUES (645,'ep',90);
            INSERT INTO coincidencias VALUES (646,'pepe',90);
                        
            INSERT INTO coincidencias VALUES (647,'festa',91);
            INSERT INTO coincidencias VALUES (648,'ya está',91);
            INSERT INTO coincidencias VALUES (649,'cuesta',91);
            INSERT INTO coincidencias VALUES (650,'perta',91);
            INSERT INTO coincidencias VALUES (651,'puerta',91);
            INSERT INTO coincidencias VALUES (652,'puesta',91);

            INSERT INTO coincidencias VALUES (653,'happy',92);
            INSERT INTO coincidencias VALUES (654,'papi',92);
            INSERT INTO coincidencias VALUES (655,'popi',92);
            INSERT INTO coincidencias VALUES (656,'poni',92);
            INSERT INTO coincidencias VALUES (657,'pop',92);
            INSERT INTO coincidencias VALUES (658,'opi',92);
            INSERT INTO coincidencias VALUES (659,'api',92);
            INSERT INTO coincidencias VALUES (660,'off',92);
            INSERT INTO coincidencias VALUES (661,'oye',92);
            INSERT INTO coincidencias VALUES (662,'y',92);
            INSERT INTO coincidencias VALUES (663,'ir',92);

            INSERT INTO coincidencias VALUES (664,'bolso',93);
            INSERT INTO coincidencias VALUES (665,'sexo',93);
            INSERT INTO coincidencias VALUES (666,'salsa',93);
            INSERT INTO coincidencias VALUES (667,'bolsa',93);
            INSERT INTO coincidencias VALUES (668,'8',93);
            INSERT INTO coincidencias VALUES (669,'whatsapp',93);
            INSERT INTO coincidencias VALUES (670,'hueso',93);
            INSERT INTO coincidencias VALUES (671,'webcam',93);
            INSERT INTO coincidencias VALUES (672,'watson',93);
            INSERT INTO coincidencias VALUES (673,'west',93);
            INSERT INTO coincidencias VALUES (674,'quest',93);
            INSERT INTO coincidencias VALUES (675,'pues sí',93);
            INSERT INTO coincidencias VALUES (676,'pues',93);
            
            INSERT INTO coincidencias VALUES (677,'tapete',94);
            INSERT INTO coincidencias VALUES (678,'app',94);
            INSERT INTO coincidencias VALUES (679,'chapete',94);
            INSERT INTO coincidencias VALUES (680,'te apeteces',94);
            INSERT INTO coincidencias VALUES (681,'tapetes',94);
            INSERT INTO coincidencias VALUES (682,'tapestry',94);
            INSERT INTO coincidencias VALUES (683,'te apetece',94);
            INSERT INTO coincidencias VALUES (684,'a pepe',94);
            INSERT INTO coincidencias VALUES (685,'app app',94);

            INSERT INTO coincidencias VALUES (686,'mesa',95);
            INSERT INTO coincidencias VALUES (687,'me sa',95);
            INSERT INTO coincidencias VALUES (688,'mesas',95);
            INSERT INTO coincidencias VALUES (689,'mena',95);
            INSERT INTO coincidencias VALUES (690,'mensa',95);
            INSERT INTO coincidencias VALUES (691,'mensaje',95);
            
            INSERT INTO coincidencias VALUES (692,'pelotas',96);
            INSERT INTO coincidencias VALUES (693,'pelota',96);
            INSERT INTO coincidencias VALUES (694,'clot',96);
            INSERT INTO coincidencias VALUES (695,'claude',96);
            INSERT INTO coincidencias VALUES (696,'slot',96);
            INSERT INTO coincidencias VALUES (697,'lot',96);
            INSERT INTO coincidencias VALUES (698,'close',96);
            INSERT INTO coincidencias VALUES (699,'pelot',96);
            INSERT INTO coincidencias VALUES (700,'pelo',96);
            INSERT INTO coincidencias VALUES (701,'otan',96);
            INSERT INTO coincidencias VALUES (702,'hota',96);
            INSERT INTO coincidencias VALUES (703,'hotel',96);


            INSERT INTO coincidencias VALUES (704,'disco',97);
            INSERT INTO coincidencias VALUES (705,'beeg',97);
            INSERT INTO coincidencias VALUES (706,'disc',97);
            INSERT INTO coincidencias VALUES (707,'disk',97);
            INSERT INTO coincidencias VALUES (708,'dicop',97);
            INSERT INTO coincidencias VALUES (709,'big cock',97);
            INSERT INTO coincidencias VALUES (710,'big bobos',97);
            
            INSERT INTO coincidencias VALUES (711,'fuerza',98);
            INSERT INTO coincidencias VALUES (712,'puertas',98);
            INSERT INTO coincidencias VALUES (713,'puerta',98);
            INSERT INTO coincidencias VALUES (714,'fuerzas',98);
            INSERT INTO coincidencias VALUES (715,'juerga',98);
            INSERT INTO coincidencias VALUES (716,'cuerdas',98);
            INSERT INTO coincidencias VALUES (717,'corda',98);
            INSERT INTO coincidencias VALUES (718,'acuerda',98);
            INSERT INTO coincidencias VALUES (719,'huelga',98);
                        
            INSERT INTO coincidencias VALUES (720,'burbuja',99);
            INSERT INTO coincidencias VALUES (721,'upap',99);
            INSERT INTO coincidencias VALUES (722,'huc whatsapp',99);
            INSERT INTO coincidencias VALUES (723,'uefa',99);
            INSERT INTO coincidencias VALUES (724,'usal',99);
            INSERT INTO coincidencias VALUES (725,'usa',99);
            INSERT INTO coincidencias VALUES (726,'uja',99);
            INSERT INTO coincidencias VALUES (727,'husa',99);
            INSERT INTO coincidencias VALUES (728,'usar',99);
            INSERT INTO coincidencias VALUES (729,'boo',99);
            INSERT INTO coincidencias VALUES (730,'bu',99);
            INSERT INTO coincidencias VALUES (731,'wu',99);

            INSERT INTO coincidencias VALUES (732,'aieta',100);
            INSERT INTO coincidencias VALUES (733,'bayeta',100);
            INSERT INTO coincidencias VALUES (734,'galleta',100);
            INSERT INTO coincidencias VALUES (735,'galletas',100);
            INSERT INTO coincidencias VALUES (736,'alleta',100);
            INSERT INTO coincidencias VALUES (737,'gallet',100);
            INSERT INTO coincidencias VALUES (738,'gaieta',100);
            INSERT INTO coincidencias VALUES (739,'calle está',100);
            INSERT INTO coincidencias VALUES (740,'calle ta.',100);
            
            INSERT INTO coincidencias VALUES (741,'coqueta',101);
            INSERT INTO coincidencias VALUES (742,'koketas',101);
            INSERT INTO coincidencias VALUES (743,'coquetas',101);
            INSERT INTO coincidencias VALUES (744,'coquette',101);
            INSERT INTO coincidencias VALUES (745,'coquetear',101);
            INSERT INTO coincidencias VALUES (746,'coquette',101);
            INSERT INTO coincidencias VALUES (747,'croquetas',101);
            INSERT INTO coincidencias VALUES (748,'socket',101);
            INSERT INTO coincidencias VALUES (749,'soqueta',101);
            INSERT INTO coincidencias VALUES (750,'chaqueta.',101);

            INSERT INTO coincidencias VALUES (751,'salchicha',102);
            INSERT INTO coincidencias VALUES (752,'salchichas',102);
            INSERT INTO coincidencias VALUES (753,'salchichón',102);
            
            
            INSERT INTO coincidencias VALUES (754,'carmen',103);
            INSERT INTO coincidencias VALUES (755,'carne',103);
            INSERT INTO coincidencias VALUES (756,'carnet',103);
            INSERT INTO coincidencias VALUES (757,'carme',103);
            INSERT INTO coincidencias VALUES (758,'carbone',103);

            INSERT INTO coincidencias VALUES (759,'sport',104);
            INSERT INTO coincidencias VALUES (760,'esponja',104);
            INSERT INTO coincidencias VALUES (761,'spot',104);
            INSERT INTO coincidencias VALUES (762,'esponjas',104);
            INSERT INTO coincidencias VALUES (763,'sponja',104);
            INSERT INTO coincidencias VALUES (764,'spa',104);
            
            INSERT INTO coincidencias VALUES (765,'coelho',105);
            INSERT INTO coincidencias VALUES (766,'cobeño',105);
            INSERT INTO coincidencias VALUES (767,'cobeña',105);
            INSERT INTO coincidencias VALUES (768,'con ello',105);
            INSERT INTO coincidencias VALUES (769,'ello',105);
            INSERT INTO coincidencias VALUES (770,'ellos',105);
            INSERT INTO coincidencias VALUES (771,'ella',105);
            INSERT INTO coincidencias VALUES (772,'a ella',105);
            INSERT INTO coincidencias VALUES (773,'con ellos',105);
            INSERT INTO coincidencias VALUES (774,'jue yo',105);
            INSERT INTO coincidencias VALUES (775,'cubelles',105);
            INSERT INTO coincidencias VALUES (776,'codillo',105);
            INSERT INTO coincidencias VALUES (777,'colegio',105);
            INSERT INTO coincidencias VALUES (778,'cuellos',105);
            INSERT INTO coincidencias VALUES (779,'uellos',105);
            INSERT INTO coincidencias VALUES (780,'uello.',105);
            

            INSERT INTO coincidencias VALUES (781,'abc',106);
            INSERT INTO coincidencias VALUES (782,'cabeza',106);
            INSERT INTO coincidencias VALUES (783,'chávez',106);
            INSERT INTO coincidencias VALUES (784,'sabes',106);
            INSERT INTO coincidencias VALUES (785,'claves',106);
            INSERT INTO coincidencias VALUES (786,'cabezón',106);
            INSERT INTO coincidencias VALUES (787,'cabezal',106);
            INSERT INTO coincidencias VALUES (788,'cada vez',106);
            INSERT INTO coincidencias VALUES (789,'cabe',106);
            INSERT INTO coincidencias VALUES (790,'cabezal',106);
            INSERT INTO coincidencias VALUES (791,'aves',106);
            INSERT INTO coincidencias VALUES (792,'aveces',106);
            
            
            INSERT INTO coincidencias VALUES (793,'hola',107);
            INSERT INTO coincidencias VALUES (794,'cola',107);
            INSERT INTO coincidencias VALUES (795,'collar',107);
            INSERT INTO coincidencias VALUES (796,'colega',107);
            INSERT INTO coincidencias VALUES (797,'collage',107);
            INSERT INTO coincidencias VALUES (798,'hola hola',107);


            INSERT INTO coincidencias VALUES (799,'panchita',108);
            INSERT INTO coincidencias VALUES (800,'pancita',108);
            INSERT INTO coincidencias VALUES (801,'panchito',108);
            INSERT INTO coincidencias VALUES (802,'transit',108);
            INSERT INTO coincidencias VALUES (803,'panceta',108);
            INSERT INTO coincidencias VALUES (804,'pan cita',108);
            INSERT INTO coincidencias VALUES (805,'can cita',108);
            INSERT INTO coincidencias VALUES (806,'ikea',108);
            INSERT INTO coincidencias VALUES (807,'y',108);
            INSERT INTO coincidencias VALUES (808,'aemet',108);
            INSERT INTO coincidencias VALUES (809,'icc',108);

            INSERT INTO coincidencias VALUES (810,'pap',109);
            INSERT INTO coincidencias VALUES (811,'papa',109);
            INSERT INTO coincidencias VALUES (812,'cap',109);
            INSERT INTO coincidencias VALUES (813,'pata',109);
            INSERT INTO coincidencias VALUES (814,'pasta',109);
            INSERT INTO coincidencias VALUES (815,'patas',109);
            INSERT INTO coincidencias VALUES (816,'patata',109);
            INSERT INTO coincidencias VALUES (817,'cata',109);
            INSERT INTO coincidencias VALUES (818,'tata',109);
            
            INSERT INTO coincidencias VALUES (819,'ancha',110);
            INSERT INTO coincidencias VALUES (820,'mancha',110);
            INSERT INTO coincidencias VALUES (821,'manchas',110);
            INSERT INTO coincidencias VALUES (822,'manchar',110);
            INSERT INTO coincidencias VALUES (823,'pancha',110);
            INSERT INTO coincidencias VALUES (824,'panchas',110);

            INSERT INTO coincidencias VALUES (825,'bbva',111);
            INSERT INTO coincidencias VALUES (826,'cueva',111);
            INSERT INTO coincidencias VALUES (827,'nueva',111);
            INSERT INTO coincidencias VALUES (828,'juega',111);
            INSERT INTO coincidencias VALUES (829,'eva',111);
            INSERT INTO coincidencias VALUES (830,'prueba',111);
            
            INSERT INTO coincidencias VALUES (831,'top',112);
            INSERT INTO coincidencias VALUES (832,'tops',112);
            INSERT INTO coincidencias VALUES (833,'toc',112);
            INSERT INTO coincidencias VALUES (834,'loca',112);
            INSERT INTO coincidencias VALUES (835,'loc',112);
            INSERT INTO coincidencias VALUES (836,'local',112);
            INSERT INTO coincidencias VALUES (837,'locas',112);
            INSERT INTO coincidencias VALUES (838,'los k',112);
            INSERT INTO coincidencias VALUES (839,'rock',112);
            INSERT INTO coincidencias VALUES (840,'ropa',112);
            INSERT INTO coincidencias VALUES (841,'procaz',112);
            INSERT INTO coincidencias VALUES (842,'topas',112);

            INSERT INTO coincidencias VALUES (843,'chera',113);
            INSERT INTO coincidencias VALUES (844,'terra',113);
            INSERT INTO coincidencias VALUES (845,'tera',113);
            INSERT INTO coincidencias VALUES (846,'cher terrasa',113);
            INSERT INTO coincidencias VALUES (847,'tierra',113);
            INSERT INTO coincidencias VALUES (848,'yedra',113);
            INSERT INTO coincidencias VALUES (849,'piedra',113);
            INSERT INTO coincidencias VALUES (850,'hiedra',113);
            INSERT INTO coincidencias VALUES (851,'pietra',113);

            INSERT INTO coincidencias VALUES (852,'volvo',114);
            INSERT INTO coincidencias VALUES (853,'polvo',114);
            INSERT INTO coincidencias VALUES (854,'polvos',114);
            INSERT INTO coincidencias VALUES (855,'pólipo',114);
            INSERT INTO coincidencias VALUES (856,'olivo',114);
            INSERT INTO coincidencias VALUES (857,'ovo',114);
            INSERT INTO coincidencias VALUES (858,'obo',114);
            INSERT INTO coincidencias VALUES (859,'oboe',114);
            INSERT INTO coincidencias VALUES (860,'obos',114);
            INSERT INTO coincidencias VALUES (861,'bo bo',114);

            INSERT INTO coincidencias VALUES (862,'0',115);
            INSERT INTO coincidencias VALUES (863,'cerro',115);
            INSERT INTO coincidencias VALUES (864,'cero',115);
            INSERT INTO coincidencias VALUES (865,'cerró',115);
            INSERT INTO coincidencias VALUES (866,'jero',115);
            INSERT INTO coincidencias VALUES (867,'texto',115);
            INSERT INTO coincidencias VALUES (868,'testo',115);
            INSERT INTO coincidencias VALUES (869,'te quiero',115);
            INSERT INTO coincidencias VALUES (870,'esto',115);

            INSERT INTO coincidencias VALUES (871,'it a audio',116);
            INSERT INTO coincidencias VALUES (872,'its a audio',116);
            INSERT INTO coincidencias VALUES (873,'dinosauiro',116);
            INSERT INTO coincidencias VALUES (874,'titanosaurio',116);
            INSERT INTO coincidencias VALUES (875,'silanos audio',116);
            INSERT INTO coincidencias VALUES (876,'síganos audio',116);
            INSERT INTO coincidencias VALUES (877,'siganos audio',116);
            INSERT INTO coincidencias VALUES (878,'titanos audio',116);

            
            INSERT INTO coincidencias VALUES (879,'montaña',117);
            INSERT INTO coincidencias VALUES (880,'montaño',117);
            INSERT INTO coincidencias VALUES (881,'montania',117);
            INSERT INTO coincidencias VALUES (882,'montagna',117);
            INSERT INTO coincidencias VALUES (883,'años',117);
            INSERT INTO coincidencias VALUES (884,'año',117);
            INSERT INTO coincidencias VALUES (885,'añas',117);
            INSERT INTO coincidencias VALUES (886,'azaña',117);
            INSERT INTO coincidencias VALUES (887,'aña',117);

            INSERT INTO coincidencias VALUES (888,'al gusto',118);
            INSERT INTO coincidencias VALUES (889,'al busto',118);
            INSERT INTO coincidencias VALUES (890,'al bulto',118);
            INSERT INTO coincidencias VALUES (891,'arbusto',118);
            INSERT INTO coincidencias VALUES (892,'arbustos',118);

            INSERT INTO coincidencias VALUES (893,'pasta',119);
            INSERT INTO coincidencias VALUES (894,'past',119);
            INSERT INTO coincidencias VALUES (895,'as',119);
            INSERT INTO coincidencias VALUES (896,'cast',119);
            INSERT INTO coincidencias VALUES (897,'tast',119);
            INSERT INTO coincidencias VALUES (898,'pasto',119);
            INSERT INTO coincidencias VALUES (899,'estás',119);

            INSERT INTO coincidencias VALUES (900,'come caca',120);
            INSERT INTO coincidencias VALUES (901,'come carne',120);
            INSERT INTO coincidencias VALUES (902,'tome cano',120);
            INSERT INTO coincidencias VALUES (903,'como spam',120);
            INSERT INTO coincidencias VALUES (904,'comer carne',120);
            INSERT INTO coincidencias VALUES (905,'comic sans',120);
            INSERT INTO coincidencias VALUES (906,'comic año',120);
            INSERT INTO coincidencias VALUES (907,'comic años',120);
            INSERT INTO coincidencias VALUES (908,'comic antes. andy',120);
            INSERT INTO coincidencias VALUES (909,'andy',120);
            INSERT INTO coincidencias VALUES (910,'amén amén',120);
            INSERT INTO coincidencias VALUES (911,'en milán',120);
            INSERT INTO coincidencias VALUES (912,'dónde anda',120);

            INSERT INTO coincidencias VALUES (913,'com sandia',121);
            INSERT INTO coincidencias VALUES (914,'come sandia',121);
            INSERT INTO coincidencias VALUES (915,'camí sandia',121);
            INSERT INTO coincidencias VALUES (916,'carmen sandia',121);
            INSERT INTO coincidencias VALUES (917,'camisa en día',121);
            INSERT INTO coincidencias VALUES (918,'omnia',121);
            INSERT INTO coincidencias VALUES (919,'omenaldia',121);
            INSERT INTO coincidencias VALUES (920,'omnia',121);
            INSERT INTO coincidencias VALUES (921,'omnis',121);
            INSERT INTO coincidencias VALUES (922,'omni a',121);
            INSERT INTO coincidencias VALUES (923,'home al día',121);

            INSERT INTO coincidencias VALUES (924,'come manzanas',122);
            INSERT INTO coincidencias VALUES (925,'hotmail manzana',122);
            INSERT INTO coincidencias VALUES (926,'home manzana',122);
            INSERT INTO coincidencias VALUES (927,'om manzana',122);
            INSERT INTO coincidencias VALUES (928,'home mantuano',122);
            INSERT INTO coincidencias VALUES (929,'manzana',122);
            INSERT INTO coincidencias VALUES (930,'ome manzana',122);

            INSERT INTO coincidencias VALUES (931,'home tuna',123);
            INSERT INTO coincidencias VALUES (932,'home tonight',123);
            INSERT INTO coincidencias VALUES (933,'hotm tuna',123);
            INSERT INTO coincidencias VALUES (934,'come tona',123);
            INSERT INTO coincidencias VALUES (935,'hamilton',123);
            INSERT INTO coincidencias VALUES (936,'carmen tona',123);
            INSERT INTO coincidencias VALUES (937,'jaime tona',123);
            INSERT INTO coincidencias VALUES (938,'comete una',123);
            INSERT INTO coincidencias VALUES (939,'carnet una',123);
            INSERT INTO coincidencias VALUES (940,'carme tuna',123);
            INSERT INTO coincidencias VALUES (941,'carne tuna',123);

            INSERT INTO coincidencias VALUES (942,'chat',124);
            INSERT INTO coincidencias VALUES (943,'rata',124);
            INSERT INTO coincidencias VALUES (944,'tata',124);
            INSERT INTO coincidencias VALUES (945,'chata',124);
            INSERT INTO coincidencias VALUES (946,'ya está',124);
            INSERT INTO coincidencias VALUES (947,'rap',124);
            INSERT INTO coincidencias VALUES (948,'seat',124);
            INSERT INTO coincidencias VALUES (949,'plato',124);
            INSERT INTO coincidencias VALUES (950,'sap',124);
            INSERT INTO coincidencias VALUES (951,'pat',124);
            INSERT INTO coincidencias VALUES (952,'path',124);
            INSERT INTO coincidencias VALUES (953,'pad',124);
            INSERT INTO coincidencias VALUES (954,'pato',124);
            INSERT INTO coincidencias VALUES (955,'cat',124);


            INSERT INTO coincidencias VALUES (956,'mochila',125);
            INSERT INTO coincidencias VALUES (957,'mochilas',125);
            INSERT INTO coincidencias VALUES (958,'mozilla',125);
            INSERT INTO coincidencias VALUES (959,'hotmail',125);
            INSERT INTO coincidencias VALUES (960,'motxilla',125);
            INSERT INTO coincidencias VALUES (961,'isla',125);
            INSERT INTO coincidencias VALUES (962,'llia',125);
            INSERT INTO coincidencias VALUES (963,'il ya',125);
            
            
            INSERT INTO coincidencias VALUES (964,'pietra',126);
            INSERT INTO coincidencias VALUES (965,'piedra',126);
            INSERT INTO coincidencias VALUES (966,'piedras',126);
            INSERT INTO coincidencias VALUES (967,'yedra',126);
            INSERT INTO coincidencias VALUES (968,'pedro',126);
            INSERT INTO coincidencias VALUES (969,'hiedra',126);
            INSERT INTO coincidencias VALUES (970,'sidra',126);
            
            INSERT INTO coincidencias VALUES (971,'amina',127);
            INSERT INTO coincidencias VALUES (972,'camino',127);
            INSERT INTO coincidencias VALUES (973,'caminos',127);
            INSERT INTO coincidencias VALUES (974,'camina',127);
            INSERT INTO coincidencias VALUES (975,'a mí no',127);
            INSERT INTO coincidencias VALUES (976,'ikea',127);
            INSERT INTO coincidencias VALUES (977,'himno',127);
            INSERT INTO coincidencias VALUES (978,'icon',127);
            INSERT INTO coincidencias VALUES (979,'icono',127);
            INSERT INTO coincidencias VALUES (980,'y no',127);

            INSERT INTO coincidencias VALUES (981,'cajamar',128);
            INSERT INTO coincidencias VALUES (982,'cama',128);
            INSERT INTO coincidencias VALUES (983,'camas',128);
            INSERT INTO coincidencias VALUES (984,'hama',128);
            INSERT INTO coincidencias VALUES (985,'ketama',128);
            INSERT INTO coincidencias VALUES (986,'hama',128);
            INSERT INTO coincidencias VALUES (987,'jama',128);
            INSERT INTO coincidencias VALUES (988,'jarama',128);
            INSERT INTO coincidencias VALUES (989,'ama',128);

            INSERT INTO coincidencias VALUES (990,'colcha',129);
            INSERT INTO coincidencias VALUES (991,'colt',129);
            INSERT INTO coincidencias VALUES (992,'cold',129);
            INSERT INTO coincidencias VALUES (993,'colchas',129);
            INSERT INTO coincidencias VALUES (994,'colchón',129);
            INSERT INTO coincidencias VALUES (995,'hot',129);
            INSERT INTO coincidencias VALUES (996,'ott',129);
            INSERT INTO coincidencias VALUES (997,'8',129);
            INSERT INTO coincidencias VALUES (998,'hotel',129);

            INSERT INTO coincidencias VALUES (999,'canasta',130);
            INSERT INTO coincidencias VALUES (1000,'canal',130);
            INSERT INTO coincidencias VALUES (1001,'kanata',130);
            INSERT INTO coincidencias VALUES (1002,'canal fiesta',130);
            INSERT INTO coincidencias VALUES (1003,'canal está',130);
            INSERT INTO coincidencias VALUES (1004,'lanata',130);
            INSERT INTO coincidencias VALUES (1005,'para nata',130);
            INSERT INTO coincidencias VALUES (1006,'anata',130);
            
            INSERT INTO coincidencias VALUES (1007,'iranosaurio',116);
            INSERT INTO coincidencias VALUES (1008,'ianoaurio',116);
            INSERT INTO coincidencias VALUES (1009,'aurio',116);
            
            INSERT INTO coincidencias VALUES (1010,'bronoaurio',131);
            INSERT INTO coincidencias VALUES (1011,'grontoaurio',131);
            INSERT INTO coincidencias VALUES (1012,'ontoaurio',131);
            INSERT INTO coincidencias VALUES (1013,'ontosaurio',131);
            INSERT INTO coincidencias VALUES (1014,'grontosaurio',131);
            
            INSERT INTO coincidencias VALUES (1015,'utas',42);
            
            INSERT INTO coincidencias VALUES (1016,'grutas',42);
            INSERT INTO coincidencias VALUES (1017,'gruta',42);
            INSERT INTO coincidencias VALUES (1018,'dutas',42);
            
            INSERT INTO coincidencias VALUES (1019,'arne',36);
            INSERT INTO coincidencias VALUES (1020,'garne',36);
            INSERT INTO coincidencias VALUES (1021,'darne',36);
            INSERT INTO coincidencias VALUES (1022,'ne',36);
            
            INSERT INTO coincidencias VALUES (1023,'ontana',132);
            INSERT INTO coincidencias VALUES (1024,'ontama',132);
            INSERT INTO coincidencias VALUES (1025,'gontana',132);
            INSERT INTO coincidencias VALUES (1026,'ontaña',132);
            
            INSERT INTO coincidencias VALUES (1027,'ocas',133);
            INSERT INTO coincidencias VALUES (1028,'oca',133);
            INSERT INTO coincidencias VALUES (1029,'roca',133);
            INSERT INTO coincidencias VALUES (1030,'goca',133);
            INSERT INTO coincidencias VALUES (1031,'gocas',133);
            
            INSERT INTO coincidencias VALUES (1032,'anzana',134);
            INSERT INTO coincidencias VALUES (1033,'angana',134);
            INSERT INTO coincidencias VALUES (1034,'andana',134);
            INSERT INTO coincidencias VALUES (1035,'mandana',134);
            INSERT INTO coincidencias VALUES (1036,'mangana',134);
            
            INSERT INTO coincidencias VALUES (1037,'aranja',135);
            INSERT INTO coincidencias VALUES (1038,'aanja',135);
            INSERT INTO coincidencias VALUES (1039,'garanga',135);
            
            INSERT INTO coincidencias VALUES (1040,'uego',136);
            INSERT INTO coincidencias VALUES (1041,'juego',136);
            INSERT INTO coincidencias VALUES (1042,'guego',136);
            INSERT INTO coincidencias VALUES (1043,'ego',136);
            
            INSERT INTO coincidencias VALUES (1044,'ogo',137);
            INSERT INTO coincidencias VALUES (1045,'oco',137);
            INSERT INTO coincidencias VALUES (1046,'gogo',137);
            INSERT INTO coincidencias VALUES (1047,'jojo',137);
            
            INSERT INTO coincidencias VALUES (1048,'hella',138);
            INSERT INTO coincidencias VALUES (1049,'gela',138);
            INSERT INTO coincidencias VALUES (1050,'telas',138);
            INSERT INTO coincidencias VALUES (1051,'tela',138);
            INSERT INTO coincidencias VALUES (1052,'pera',138);
            INSERT INTO coincidencias VALUES (1053,'pela',138);
            INSERT INTO coincidencias VALUES (1054,'terra',138);
            INSERT INTO coincidencias VALUES (1055,'pero',138);
            
            INSERT INTO coincidencias VALUES (1056,'museos',139);
            INSERT INTO coincidencias VALUES (1057,'museo',139);
            INSERT INTO coincidencias VALUES (1058,'mustio',139);
            INSERT INTO coincidencias VALUES (1059,'buseo',139);
            INSERT INTO coincidencias VALUES (1060,'cuseo',139);
            INSERT INTO coincidencias VALUES (1061,'sucio',139);
            INSERT INTO coincidencias VALUES (1062,'osea',139);
            INSERT INTO coincidencias VALUES (1063,'no sea',139);
            INSERT INTO coincidencias VALUES (1064,'eo',139);
            INSERT INTO coincidencias VALUES (1065,'eol',139);
            INSERT INTO coincidencias VALUES (1066,'useo',139);
            INSERT INTO coincidencias VALUES (1067,'mujeo',139);
            INSERT INTO coincidencias VALUES (1068,'mufeo',139);
            
            INSERT INTO coincidencias VALUES (1069,'circo',140);
            INSERT INTO coincidencias VALUES (1070,'circ',140);
            INSERT INTO coincidencias VALUES (1071,'citop',140);
            INSERT INTO coincidencias VALUES (1072,'cicop',140);
            INSERT INTO coincidencias VALUES (1073,'oct',140);
            INSERT INTO coincidencias VALUES (1074,'ok',140);
            INSERT INTO coincidencias VALUES (1075,'pipo',140);
            INSERT INTO coincidencias VALUES (1076,'si o no',140);
            INSERT INTO coincidencias VALUES (1077,'si',140);
            INSERT INTO coincidencias VALUES (1078,'ico',140);
            INSERT INTO coincidencias VALUES (1079,'isco',140);
            INSERT INTO coincidencias VALUES (1080,'kiko',140);
            INSERT INTO coincidencias VALUES (1081,'icod',140);
            INSERT INTO coincidencias VALUES (1082,'jico',140);
            INSERT INTO coincidencias VALUES (1083,'jirco',140);
            INSERT INTO coincidencias VALUES (1084,'dirjo',140);
            INSERT INTO coincidencias VALUES (1085,'jirjo',140);
            
            INSERT INTO coincidencias VALUES (1086,'sime',141);
            INSERT INTO coincidencias VALUES (1087,'cines',141);
            INSERT INTO coincidencias VALUES (1088,'sim',141);
            INSERT INTO coincidencias VALUES (1089,'see me',141);
            INSERT INTO coincidencias VALUES (1090,'inem',141);
            INSERT INTO coincidencias VALUES (1091,'ines',141);
            INSERT INTO coincidencias VALUES (1092,'in',141);
            INSERT INTO coincidencias VALUES (1093,'cine',141);
            INSERT INTO coincidencias VALUES (1094,'jine',141);
            INSERT INTO coincidencias VALUES (1095,'ine',141);
            
            INSERT INTO coincidencias VALUES (1096,'ian',142);
            INSERT INTO coincidencias VALUES (1097,'y',142);
            INSERT INTO coincidencias VALUES (1098,'hospital',142);
            INSERT INTO coincidencias VALUES (1099,'opital',142);
            INSERT INTO coincidencias VALUES (1100,'oital',142);
            INSERT INTO coincidencias VALUES (1101,'ojdital',142);
            
            INSERT INTO coincidencias VALUES (1102,'EOI',143);
            INSERT INTO coincidencias VALUES (1103,'hero',143);
            INSERT INTO coincidencias VALUES (1104,'perro',143);
            INSERT INTO coincidencias VALUES (1105,'eva',143);
            INSERT INTO coincidencias VALUES (1106,'eso',143);
            INSERT INTO coincidencias VALUES (1107,'ceo',143);
            INSERT INTO coincidencias VALUES (1108,'berro',143);
            INSERT INTO coincidencias VALUES (1109,'erro',143);
            INSERT INTO coincidencias VALUES (1110,'eo',143);
            INSERT INTO coincidencias VALUES (1111,'jerro',143);
            INSERT INTO coincidencias VALUES (1112,'jeo',143);
            
            INSERT INTO coincidencias VALUES (1113,'vichy',144);
            INSERT INTO coincidencias VALUES (1114,'visit',144);
            INSERT INTO coincidencias VALUES (1115,'vice',144);
            INSERT INTO coincidencias VALUES (1116,'visite',144);
            INSERT INTO coincidencias VALUES (1117,'bici',144);
            INSERT INTO coincidencias VALUES (1118,'bii',144);
            INSERT INTO coincidencias VALUES (1119,'viri',144);
            INSERT INTO coincidencias VALUES (1120,'pili',144);
            INSERT INTO coincidencias VALUES (1121,'pici',144);
            INSERT INTO coincidencias VALUES (1122,'biji',144);
            INSERT INTO coincidencias VALUES (1123,'iji',144);
            
            INSERT INTO coincidencias VALUES (1124,'taxis',145);
            INSERT INTO coincidencias VALUES (1125,'perfil',145);
            INSERT INTO coincidencias VALUES (1126,'papi',145);
            INSERT INTO coincidencias VALUES (1127,'taxi',145);
            INSERT INTO coincidencias VALUES (1128,'maxi',145);
            INSERT INTO coincidencias VALUES (1129,'ahi',145);
            INSERT INTO coincidencias VALUES (1130,'ai',145);
            INSERT INTO coincidencias VALUES (1131,'tai',145);
            INSERT INTO coincidencias VALUES (1132,'daji',145);
            INSERT INTO coincidencias VALUES (1133,'aji',145);
            INSERT INTO coincidencias VALUES (1134,'taji',145);
            
            INSERT INTO coincidencias VALUES (1135,'moto y le está',146);
            INSERT INTO coincidencias VALUES (1136,'moto y letra',146);
            INSERT INTO coincidencias VALUES (1137,'motor y letra',146);
            INSERT INTO coincidencias VALUES (1138,'moto y letal',146);
            INSERT INTO coincidencias VALUES (1139,'moto y le ta',146);
            INSERT INTO coincidencias VALUES (1140,'motocicletas',146);
            INSERT INTO coincidencias VALUES (1141,'motocicleta',146);
            INSERT INTO coincidencias VALUES (1142,'INE',146);            
            INSERT INTO coincidencias VALUES (1143,'INEM',146);
            INSERT INTO coincidencias VALUES (1144,'ENM',146);
            INSERT INTO coincidencias VALUES (1145,'image',146);
            INSERT INTO coincidencias VALUES (1146,'motojicleta',146);
            
            INSERT INTO coincidencias VALUES (1147,'aion',147);
            INSERT INTO coincidencias VALUES (1148,'ion',147);
            INSERT INTO coincidencias VALUES (1149,'aeon',147);
            INSERT INTO coincidencias VALUES (1150,'y león',147);
            INSERT INTO coincidencias VALUES (1151,'avion',147);
            INSERT INTO coincidencias VALUES (1152,'aviones',147);
            INSERT INTO coincidencias VALUES (1153,'avión y',147);
            INSERT INTO coincidencias VALUES (1154,'avions',147);
            INSERT INTO coincidencias VALUES (1155,'acción',147);
            INSERT INTO coincidencias VALUES (1156,'action',147);
            INSERT INTO coincidencias VALUES (1157,'jhon',147);
            INSERT INTO coincidencias VALUES (1158,'yon',147);
            
            INSERT INTO coincidencias VALUES (1159,'pon',104);
            INSERT INTO coincidencias VALUES (1160,'esponja',104);
            INSERT INTO coincidencias VALUES (1161,'pons',104);
            INSERT INTO coincidencias VALUES (1162,'pont',104);
            INSERT INTO coincidencias VALUES (1163,'com',104);
            INSERT INTO coincidencias VALUES (1164,'ESPO',104);
            INSERT INTO coincidencias VALUES (1165,'esponjas',104);
            INSERT INTO coincidencias VALUES (1166,'Sponja',104);
            INSERT INTO coincidencias VALUES (1167,'Sport',104);
            INSERT INTO coincidencias VALUES (1168,'esposa',104);
            INSERT INTO coincidencias VALUES (1169,'esposo',104);
            INSERT INTO coincidencias VALUES (1170,'Epo',104);
            INSERT INTO coincidencias VALUES (1171,'epos',104);
            INSERT INTO coincidencias VALUES (1172,'pepo',104);
            INSERT INTO coincidencias VALUES (1173,'epoxi',104);
            INSERT INTO coincidencias VALUES (1174,'EPOC',104);
            INSERT INTO coincidencias VALUES (1175,'el pozo',104);
            INSERT INTO coincidencias VALUES (1176,'he Esponja',104);
            
            INSERT INTO coincidencias VALUES (1177,'alo',148);
            INSERT INTO coincidencias VALUES (1178,'dalo',148);
            INSERT INTO coincidencias VALUES (1179,'balo',148);
            INSERT INTO coincidencias VALUES (1180,'galo',148);
            INSERT INTO coincidencias VALUES (1181,'gao',148);
            INSERT INTO coincidencias VALUES (1182,'ao',148);
            INSERT INTO coincidencias VALUES (1183,'dao',148);
            INSERT INTO coincidencias VALUES (1184,'bao',148);
            
            INSERT INTO coincidencias VALUES (1185,'apatos',50);		 	
            INSERT INTO coincidencias VALUES (1186,'apados',50);			
            INSERT INTO coincidencias VALUES (1187,'dapados',50);			
            INSERT INTO coincidencias VALUES (1188,'dapado',50);			
            INSERT INTO coincidencias VALUES (1189,'apado',50);			
            INSERT INTO coincidencias VALUES (1190,'apaos',50);			
        
            INSERT INTO coincidencias VALUES (1191,'piano audio',116);				
            INSERT INTO coincidencias VALUES (1192,'piano a orio',116);				
            INSERT INTO coincidencias VALUES (1193,'piano ahora',116);				
            INSERT INTO coincidencias VALUES (1194,'piano AOL',116);				
            INSERT INTO coincidencias VALUES (1195,'ya no ahora',116);				
            INSERT INTO coincidencias VALUES (1196,'iano ua',116);				
            INSERT INTO coincidencias VALUES (1197,'ian',116);				
            INSERT INTO coincidencias VALUES (1198,'ian AOLL',116);				
            INSERT INTO coincidencias VALUES (1199,'Ian Audi',116);				
            INSERT INTO coincidencias VALUES (1200,'Ian Ahora',116);				
            INSERT INTO coincidencias VALUES (1201,'y ano',116);				
            INSERT INTO coincidencias VALUES (1202,'y en audio',116);				
            INSERT INTO coincidencias VALUES (1203,'y no abrió',116);				
            INSERT INTO coincidencias VALUES (1204,'Ian abrió',116);				
            INSERT INTO coincidencias VALUES (1205,'Ian Audio',116);				
            INSERT INTO coincidencias VALUES (1206,'Ian Auryn',116);				
            INSERT INTO coincidencias VALUES (1207,'ya no saurio',116);				
            INSERT INTO coincidencias VALUES (1208,'Ian o saurio',116);				
            INSERT INTO coincidencias VALUES (1209,'yanos audio',116);				
            INSERT INTO coincidencias VALUES (1210,'y ah no saurio',116);				
            INSERT INTO coincidencias VALUES (1211,'ya nos Auryn',116);				

            INSERT INTO coincidencias VALUES (1212,'adta endal',46);				
            INSERT INTO coincidencias VALUES (1213,'asta',46);				
            INSERT INTO coincidencias VALUES (1214,'adta',46);				
            INSERT INTO coincidencias VALUES (1215,'al',46);				
            INSERT INTO coincidencias VALUES (1216,'ental',46);				
            INSERT INTO coincidencias VALUES (1217,'ednal',46);				
            INSERT INTO coincidencias VALUES (1218,'asta endal',46);				
            INSERT INTO coincidencias VALUES (1219,'ata enal',46);				
            INSERT INTO coincidencias VALUES (1220,'renal',46);				
            INSERT INTO coincidencias VALUES (1221,'serial',46);				


            INSERT INTO coincidencias VALUES (1222,'etes',149);			
            INSERT INTO coincidencias VALUES (1223,'dientes',149);			
            INSERT INTO coincidencias VALUES (1224,'dieta',149);			
            INSERT INTO coincidencias VALUES (1225,'dietes',149);			
            INSERT INTO coincidencias VALUES (1226,'diente',149);			
            INSERT INTO coincidencias VALUES (1227,'yate',149);			
            INSERT INTO coincidencias VALUES (1228,'yates',149);			
            INSERT INTO coincidencias VALUES (1229,'yete',149);			
            INSERT INTO coincidencias VALUES (1230,'ietes',149);			

            
            
            INSERT INTO coincidencias VALUES (1231,'oneda',150);				
            INSERT INTO coincidencias VALUES (1232,'onena',150);				
            INSERT INTO coincidencias VALUES (1233,'onenas',150);				
            INSERT INTO coincidencias VALUES (1234,'nena',150);				
            INSERT INTO coincidencias VALUES (1235,'bodega',150);				
            INSERT INTO coincidencias VALUES (1236,'bodegas',150);				
            INSERT INTO coincidencias VALUES (1237,'oenas',150);				
            INSERT INTO coincidencias VALUES (1238,'onemas',150);				
            INSERT INTO coincidencias VALUES (1239,'boneda',150);				
            INSERT INTO coincidencias VALUES (1240,'bonebas',150);				


            INSERT INTO coincidencias VALUES (1241,'idea',151);				
            INSERT INTO coincidencias VALUES (1242,'jimenea',151);				
            INSERT INTO coincidencias VALUES (1243,'jienea',151);				
            INSERT INTO coincidencias VALUES (1244,'jinea',151);				
            INSERT INTO coincidencias VALUES (1245,'enea',151);				
            INSERT INTO coincidencias VALUES (1246,'ea',151);				
            INSERT INTO coincidencias VALUES (1247,'fimea',151);				
            INSERT INTO coincidencias VALUES (1248,'fimenea',151);				

            INSERT INTO coincidencias VALUES (1249,'ana',152);				
            INSERT INTO coincidencias VALUES (1250,'paja',152);				
            INSERT INTO coincidencias VALUES (1251,'amana',152);				
            INSERT INTO coincidencias VALUES (1252,'gamana',152);				
            INSERT INTO coincidencias VALUES (1253,'gamaga',152);				
            INSERT INTO coincidencias VALUES (1254,'ara',152);				
            INSERT INTO coincidencias VALUES (1255,'aga',152);				
            INSERT INTO coincidencias VALUES (1256,'aja',152);				
            INSERT INTO coincidencias VALUES (1257,'amala',152);				
            INSERT INTO coincidencias VALUES (1258,'lamala',152);				
			
            INSERT INTO coincidencias VALUES (1259,'dia',153);				
            INSERT INTO coincidencias VALUES (1260,'ia',153);				
            INSERT INTO coincidencias VALUES (1261,'ja dia',153);				
            INSERT INTO coincidencias VALUES (1262,'dandia',153);				
            INSERT INTO coincidencias VALUES (1263,'jandia',153);				
            INSERT INTO coincidencias VALUES (1264,'fandia',153);				
            
            INSERT INTO coincidencias VALUES (1265,'Com sandia',121);				
            INSERT INTO coincidencias VALUES (1266,'come sandia',121);				
            INSERT INTO coincidencias VALUES (1267,'camí sandia',121);				
            INSERT INTO coincidencias VALUES (1268,'Carmen sandia',121);				
            INSERT INTO coincidencias VALUES (1269,'camisa en día',121);				
            INSERT INTO coincidencias VALUES (1270,'Omnia',121);				
            INSERT INTO coincidencias VALUES (1271,'omenaldia',121);				
            INSERT INTO coincidencias VALUES (1272,'omnia',121);				
            INSERT INTO coincidencias VALUES (1273,'omnis',121);				
            INSERT INTO coincidencias VALUES (1274,'omni a',121);				
            INSERT INTO coincidencias VALUES (1275,'home al día',121);				

            INSERT INTO coincidencias VALUES (1276,'BBVA',111);				
            INSERT INTO coincidencias VALUES (1277,'cueva',111);				
            INSERT INTO coincidencias VALUES (1278,'cuevas',111);				
            INSERT INTO coincidencias VALUES (1279,'Eva',111);				
            INSERT INTO coincidencias VALUES (1280,'eBay',111);				
            INSERT INTO coincidencias VALUES (1281,'EBA',111);				
            INSERT INTO coincidencias VALUES (1282,'qué va',111);				
            INSERT INTO coincidencias VALUES (1283,'beba',111);				
            INSERT INTO coincidencias VALUES (1284,'queva',111);				
            INSERT INTO coincidencias VALUES (1285,'prueba',111);				
            INSERT INTO coincidencias VALUES (1286,'nueva',111);				

            INSERT INTO coincidencias VALUES (1287,'Hero',143);			
            INSERT INTO coincidencias VALUES (1288,'eros',143);			
            INSERT INTO coincidencias VALUES (1289,'air',143);			
            INSERT INTO coincidencias VALUES (1290,'Ebro',143);			
            INSERT INTO coincidencias VALUES (1291,'perro',143);			
            INSERT INTO coincidencias VALUES (1292,'ero',143);			
            INSERT INTO coincidencias VALUES (1293,'enero',143);			
            INSERT INTO coincidencias VALUES (1294,'seur',143);			
            INSERT INTO coincidencias VALUES (1295,'ok',143);			
            INSERT INTO coincidencias VALUES (1296,'se o',143);			
            INSERT INTO coincidencias VALUES (1297,'seur',143);			
            INSERT INTO coincidencias VALUES (1298,'o',143);			
            INSERT INTO coincidencias VALUES (1299,'segó',143);			
            INSERT INTO coincidencias VALUES (1300,'sergo',143);			
            INSERT INTO coincidencias VALUES (1301,'ser',143);			
            INSERT INTO coincidencias VALUES (1302,'juego',143);			
            INSERT INTO coincidencias VALUES (1303,'cejó',143);			
            INSERT INTO coincidencias VALUES (1304,'seco',143);			
            INSERT INTO coincidencias VALUES (1305,'sergio',143);			
            INSERT INTO coincidencias VALUES (1306,'Gecko',143);			
            INSERT INTO coincidencias VALUES (1307,'Gesco',143);			
            INSERT INTO coincidencias VALUES (1308,'geco',143);
            
            INSERT INTO coincidencias VALUES (1309,'canal',130);			
            INSERT INTO coincidencias VALUES (1310,'canals',130);			
            INSERT INTO coincidencias VALUES (1311,'canasta',130);			
            INSERT INTO coincidencias VALUES (1312,'canada',130);			
            INSERT INTO coincidencias VALUES (1313,'Ana',130);			
            INSERT INTO coincidencias VALUES (1314,'ANAC',130);			
            INSERT INTO coincidencias VALUES (1315,'anal',130);			
            INSERT INTO coincidencias VALUES (1316,'Anas',130);			
            INSERT INTO coincidencias VALUES (1317,'Anna',130);			
            INSERT INTO coincidencias VALUES (1318,'anata',130);			
            INSERT INTO coincidencias VALUES (1319,'Ana está',130);			
            INSERT INTO coincidencias VALUES (1320,'carne',36);				
            INSERT INTO coincidencias VALUES (1321,'carnet',36);				
            INSERT INTO coincidencias VALUES (1322,'carnes',36);				
            INSERT INTO coincidencias VALUES (1323,'Carmen',36);				
            INSERT INTO coincidencias VALUES (1324,'Anne',36);				
            INSERT INTO coincidencias VALUES (1325,'ane',36);				
            INSERT INTO coincidencias VALUES (1326,'Annie',36);				
            INSERT INTO coincidencias VALUES (1327,'Any',36);				
            INSERT INTO coincidencias VALUES (1328,'Andy',36);				
            INSERT INTO coincidencias VALUES (1329,'came',36);				
            INSERT INTO coincidencias VALUES (1330,'tándem',36);				
            INSERT INTO coincidencias VALUES (1331,'candem',36);				
            INSERT INTO coincidencias VALUES (1332,'cande',36);				
            INSERT INTO coincidencias VALUES (1333,'cannes',36);	
                    
            INSERT INTO coincidencias VALUES (1334,'Utah',42);				
            INSERT INTO coincidencias VALUES (1335,'utad',42);				
            INSERT INTO coincidencias VALUES (1336,'UTA',42);				
            INSERT INTO coincidencias VALUES (1337,'putas',42);				
            INSERT INTO coincidencias VALUES (1338,'futa',42);				
            INSERT INTO coincidencias VALUES (1339,'futas',42);				
            INSERT INTO coincidencias VALUES (1340,'puta',42);				
            INSERT INTO coincidencias VALUES (1341,'uta',42);				
            INSERT INTO coincidencias VALUES (1342,'su ta',42);				
            INSERT INTO coincidencias VALUES (1343,'ruta',42);				
            INSERT INTO coincidencias VALUES (1344,'muta',42);				
            INSERT INTO coincidencias VALUES (1345,'mutas',42);				
            INSERT INTO coincidencias VALUES (1346,'mudas',42);	
            
            INSERT INTO coincidencias VALUES (1347,'gandía',153);			
            INSERT INTO coincidencias VALUES (1348,'Jandía',153);			
            INSERT INTO coincidencias VALUES (1349,'andía',153);			
            INSERT INTO coincidencias VALUES (1350,'Andy',153);			
            INSERT INTO coincidencias VALUES (1351,'al día',153);			
            INSERT INTO coincidencias VALUES (1352,'and día',153);			
            INSERT INTO coincidencias VALUES (1353,'mandía',153);			
            INSERT INTO coincidencias VALUES (1354,'mania',153);			
            INSERT INTO coincidencias VALUES (1355,'aria',153);			
            INSERT INTO coincidencias VALUES (1356,'adidas',153);			
            INSERT INTO coincidencias VALUES (1357,'haría',153);			
            INSERT INTO coincidencias VALUES (1358,'a día',153);			
            INSERT INTO coincidencias VALUES (1359,'adía',153);			
            
            INSERT INTO coincidencias VALUES (1360,'lámpara',152);
            INSERT INTO coincidencias VALUES (1361,'lámparas',152);
            INSERT INTO coincidencias VALUES (1362,'la lámpara',152);
            INSERT INTO coincidencias VALUES (1363,'plan para',152);
            INSERT INTO coincidencias VALUES (1364,'lam para',152);
            
            INSERT INTO coincidencias VALUES (1365,'práctico',9);
            
            INSERT INTO coincidencias VALUES (1366,'rutas',42);
            
            

            INSERT INTO coincidencias VALUES (1367,'sandía',153);
            INSERT INTO coincidencias VALUES (1368,'familia',153);
            INSERT INTO coincidencias VALUES (1369,'gran día',153);
            INSERT INTO coincidencias VALUES (1370,'gandia',153);
            INSERT INTO coincidencias VALUES (1371,'la mía',153);
            
            INSERT INTO coincidencias VALUES (1372,'montaña',154);
            INSERT INTO coincidencias VALUES (1373,'montañas',154);
            INSERT INTO coincidencias VALUES (1374,'mon taña',154);
            INSERT INTO coincidencias VALUES (1375,'montaña y',154);
            INSERT INTO coincidencias VALUES (1376,'montañas y',154);
            
            INSERT INTO coincidencias VALUES (1377,'come sandía',121);
            INSERT INTO coincidencias VALUES (1378,'comer sandía',121);
            INSERT INTO coincidencias VALUES (1379,'cómo es un día',121);
            INSERT INTO coincidencias VALUES (1380,'comezón día',121);
            INSERT INTO coincidencias VALUES (1381,'como es el día',121);
            
            INSERT INTO coincidencias VALUES (1382,'montaña',155);
            INSERT INTO coincidencias VALUES (1383,'montañas',155);
            INSERT INTO coincidencias VALUES (1384,'mon taña',155);
            INSERT INTO coincidencias VALUES (1385,'montaña y',155);
            INSERT INTO coincidencias VALUES (1386,'montañas y',155);
                        
            INSERT INTO coincidencias VALUES (1387,'oca',156);			
            INSERT INTO coincidencias VALUES (1388,'gota',156);			
            INSERT INTO coincidencias VALUES (1389,'hoja',156);			
            INSERT INTO coincidencias VALUES (1390,'jota',156);			
            INSERT INTO coincidencias VALUES (1391,'goca',156);			
            INSERT INTO coincidencias VALUES (1392,'joga',156);		
            
            INSERT INTO coincidencias VALUES (1393,'ena',157);			
            INSERT INTO coincidencias VALUES (1394,'irene',157);			
            INSERT INTO coincidencias VALUES (1395,'jirena',157);			
            INSERT INTO coincidencias VALUES (1396,'iena',157);			
            INSERT INTO coincidencias VALUES (1397,'llena',157);			
            INSERT INTO coincidencias VALUES (1398,'igena',157);			

            INSERT INTO coincidencias VALUES (1399,'casa',158);             
            INSERT INTO coincidencias VALUES (1400,'caza',158);
            INSERT INTO coincidencias VALUES (1401,'casas',158);               
            INSERT INTO coincidencias VALUES (1402,'la casa',158);               
            INSERT INTO coincidencias VALUES (1403,'a casa',158);               
            
            INSERT INTO coincidencias VALUES (1404,'planta',159);
            INSERT INTO coincidencias VALUES (1405,'plantas',159);
            INSERT INTO coincidencias VALUES (1406,'atlanta',159);
            INSERT INTO coincidencias VALUES (1407,'planta de',159);               
            INSERT INTO coincidencias VALUES (1408,'lámparas',159);               
            INSERT INTO coincidencias VALUES (1409,'bandas',159);               
            INSERT INTO coincidencias VALUES (1410,'banda',159);

            INSERT INTO coincidencias VALUES (1411,'bola',160);
            INSERT INTO coincidencias VALUES (1412,'bulla',160);
            INSERT INTO coincidencias VALUES (1413,'bowa',160);
            INSERT INTO coincidencias VALUES (1414,'boys',160);
            INSERT INTO coincidencias VALUES (1415,'moa',160);
            INSERT INTO coincidencias VALUES (1416,'well',160);

            INSERT INTO coincidencias VALUES (1417,'dear',161);
            INSERT INTO coincidencias VALUES (1418,'deere',161);
            INSERT INTO coincidencias VALUES (1419,'beer',161);
            INSERT INTO coincidencias VALUES (1420,'gear',161);
            INSERT INTO coincidencias VALUES (1421,'here',161);

            INSERT INTO coincidencias VALUES (1422,'awana',162);
            INSERT INTO coincidencias VALUES (1423,'deewana',162);
            INSERT INTO coincidencias VALUES (1424,'i wanna',162);
            INSERT INTO coincidencias VALUES (1425,'alana',162);
            INSERT INTO coincidencias VALUES (1426,'equine',162);
            INSERT INTO coincidencias VALUES (1427,'eguana',162);
            INSERT INTO coincidencias VALUES (1428,'the iguana',162);
            INSERT INTO coincidencias VALUES (1429,'iguanas',162);

            INSERT INTO coincidencias VALUES (1430,'salary',163);
            INSERT INTO coincidencias VALUES (1431,'southern',163);
            INSERT INTO coincidencias VALUES (1432,'sumari',163);
            INSERT INTO coincidencias VALUES (1433,'salaries',163);
            INSERT INTO coincidencias VALUES (1434,'gallery',163);
            INSERT INTO coincidencias VALUES (1435,'valerie',163);

            INSERT INTO coincidencias VALUES (1436,'bore',164);
            INSERT INTO coincidencias VALUES (1437,'vore',164);
            INSERT INTO coincidencias VALUES (1438,'war',164);
            INSERT INTO coincidencias VALUES (1439,'more',164);
            INSERT INTO coincidencias VALUES (1440,'4',164);
            INSERT INTO coincidencias VALUES (1441,'four',164);

            INSERT INTO coincidencias VALUES (1442,'tier',165);
            INSERT INTO coincidencias VALUES (1443,'tyr',165);
            INSERT INTO coincidencias VALUES (1444,'cheer',165);
            INSERT INTO coincidencias VALUES (1445,'tiere',165);
            INSERT INTO coincidencias VALUES (1446,'here',165);

            INSERT INTO coincidencias VALUES (1447,'jack',166);
            INSERT INTO coincidencias VALUES (1448,'jak',166);
            INSERT INTO coincidencias VALUES (1449,'jac',166);
            INSERT INTO coincidencias VALUES (1450,'jag',166);
            INSERT INTO coincidencias VALUES (1451,'check',166);
            INSERT INTO coincidencias VALUES (1452,'yep',166);
            INSERT INTO coincidencias VALUES (1453,'yes',166);
            INSERT INTO coincidencias VALUES (1454,'yeah',166);
            INSERT INTO coincidencias VALUES (1455,'yet',166);

            INSERT INTO coincidencias VALUES (1456,'graysons',167);
            INSERT INTO coincidencias VALUES (1457,'greysons',167);
            INSERT INTO coincidencias VALUES (1458,'raisons',167);
            INSERT INTO coincidencias VALUES (1459,'craisins',167);
            INSERT INTO coincidencias VALUES (1460,'racing',167);
            INSERT INTO coincidencias VALUES (1461,'masons',167);
            INSERT INTO coincidencias VALUES (1462,'races',167);
            INSERT INTO coincidencias VALUES (1463,'jasons',167);

            INSERT INTO coincidencias VALUES (1464,'por',168);
            INSERT INTO coincidencias VALUES (1465,'poor',168);
            INSERT INTO coincidencias VALUES (1466,'pore',168);
            INSERT INTO coincidencias VALUES (1467,'core',168);
            INSERT INTO coincidencias VALUES (1468,'horror',168);
            INSERT INTO coincidencias VALUES (1469,'four',168);

            INSERT INTO coincidencias VALUES (1470,'eyedea',169);
            INSERT INTO coincidencias VALUES (1471,'eyedia',169);
            INSERT INTO coincidencias VALUES (1472,'ideas',169);
            INSERT INTO coincidencias VALUES (1473,'ikeas',169);
            INSERT INTO coincidencias VALUES (1474,'i',169);
            

            INSERT INTO coincidencias VALUES (1475,'monkee',170);
            INSERT INTO coincidencias VALUES (1476,'monki',170);
            INSERT INTO coincidencias VALUES (1477,'monky',170);
            INSERT INTO coincidencias VALUES (1478,'monkeys',170);
            INSERT INTO coincidencias VALUES (1479,'bump key',170);
            INSERT INTO coincidencias VALUES (1480,'bunkie',170);
            INSERT INTO coincidencias VALUES (1481,'bunky',170);
            INSERT INTO coincidencias VALUES (1482,'punky',170);

            INSERT INTO coincidencias VALUES (1483,'sisters',171);
            INSERT INTO coincidencias VALUES (1484,'sensors',171);
            INSERT INTO coincidencias VALUES (1485,'assessors',171);
            INSERT INTO coincidencias VALUES (1486,'caesars',171);
            INSERT INTO coincidencias VALUES (1487,'fishers',171);

            INSERT INTO coincidencias VALUES (1488,'ballon',172);
            INSERT INTO coincidencias VALUES (1489,'buffon',172);
            INSERT INTO coincidencias VALUES (1490,'the boon',172);
            INSERT INTO coincidencias VALUES (1491,'the moon',172);
            INSERT INTO coincidencias VALUES (1492,'the boom',172);
            
            INSERT INTO coincidencias VALUES (1493,'tuner',173);
            INSERT INTO coincidencias VALUES (1494,'luna',173);
            INSERT INTO coincidencias VALUES (1495,'turner',173);
            INSERT INTO coincidencias VALUES (1496,'tina',173);
            INSERT INTO coincidencias VALUES (1497,'pune',173);
            INSERT INTO coincidencias VALUES (1498,'kuna',173);

            INSERT INTO coincidencias VALUES (1499,'kane',174);
            INSERT INTO coincidencias VALUES (1500,'pain',174);
            INSERT INTO coincidencias VALUES (1501,'cain',174);
            INSERT INTO coincidencias VALUES (1502,'chain',174);
            INSERT INTO coincidencias VALUES (1503,'cave',174);

            INSERT INTO coincidencias VALUES (1504,'salame',175);
            INSERT INTO coincidencias VALUES (1505,'salome',175);
            INSERT INTO coincidencias VALUES (1506,'salami',175);
            INSERT INTO coincidencias VALUES (1507,'salama',175);
            INSERT INTO coincidencias VALUES (1508,'salon',175);
            INSERT INTO coincidencias VALUES (1509,'solano felony',175);

            INSERT INTO coincidencias VALUES (1510,'polar bears',176);
            INSERT INTO coincidencias VALUES (1511,'polar berry',176);
            INSERT INTO coincidencias VALUES (1512,'polar bearing',176);
            INSERT INTO coincidencias VALUES (1513,'polar bear weigh',176);
            INSERT INTO coincidencias VALUES (1514,'polar bare',176);

            INSERT INTO coincidencias VALUES (1515,'thor',177);
            INSERT INTO coincidencias VALUES (1516,'dora',177);
            INSERT INTO coincidencias VALUES (1517,'dor',177);
            INSERT INTO coincidencias VALUES (1518,'dorr',177);
            INSERT INTO coincidencias VALUES (1519,'tour',177);
            INSERT INTO coincidencias VALUES (1520,'store',177);

            INSERT INTO coincidencias VALUES (1521,'kanga roo',178);
            INSERT INTO coincidencias VALUES (1522,'kangaroos',178);
            INSERT INTO coincidencias VALUES (1523,'tango room',178);
            INSERT INTO coincidencias VALUES (1524,'can go through',178);
            INSERT INTO coincidencias VALUES (1525,'conga room',178);

            INSERT INTO coincidencias VALUES (1526,'salman',179);
            INSERT INTO coincidencias VALUES (1527,'salmen',179);
            INSERT INTO coincidencias VALUES (1528,'salomom',179);
            INSERT INTO coincidencias VALUES (1529,'selmon',179);
            INSERT INTO coincidencias VALUES (1530,'selman',179);

            INSERT INTO coincidencias VALUES (1531,'houma',180);
            INSERT INTO coincidencias VALUES (1532,'kuma',180);
            INSERT INTO coincidencias VALUES (1533,'soma',180);
            INSERT INTO coincidencias VALUES (1534,'boomer',180);
            INSERT INTO coincidencias VALUES (1535,'cool math',180);
            INSERT INTO coincidencias VALUES (1536,'pumbaa',180);
            INSERT INTO coincidencias VALUES (1537,'huma',180);

            INSERT INTO coincidencias VALUES (1538,'play-doh',181);
            INSERT INTO coincidencias VALUES (1539,'hello',181);
            INSERT INTO coincidencias VALUES (1540,'mobile',181);
            INSERT INTO coincidencias VALUES (1541,'slidell',181);
            INSERT INTO coincidencias VALUES (1542,'tomatoes',181);
            INSERT INTO coincidencias VALUES (1543,'Tomato’s',181);
            INSERT INTO coincidencias VALUES (1544,'Tornado',181);

            INSERT INTO coincidencias VALUES (1545,'Google',182);
            INSERT INTO coincidencias VALUES (1546,'Hookah',182);
            INSERT INTO coincidencias VALUES (1547,'Singer',182);
            INSERT INTO coincidencias VALUES (1548,'Ruger',182);
            INSERT INTO coincidencias VALUES (1549,'Booger',182);
            INSERT INTO coincidencias VALUES (1550,'Hoover',182);
            INSERT INTO coincidencias VALUES (1551,'Pooler',182);
            INSERT INTO coincidencias VALUES (1552,'Cooler',182);
            INSERT INTO coincidencias VALUES (1553,'Ruler',182);
            INSERT INTO coincidencias VALUES (1554,'Hulu',182);

            INSERT INTO coincidencias VALUES (1555,'Weather',183);
            INSERT INTO coincidencias VALUES (1556,'Listen',183);
            INSERT INTO coincidencias VALUES (1557,'Rosas',183);
            INSERT INTO coincidencias VALUES (1558,'Rose’s',183);
            INSERT INTO coincidencias VALUES (1559,'Rosa’s',183);
            INSERT INTO coincidencias VALUES (1560,'Rozes',183);
            INSERT INTO coincidencias VALUES (1561,'Rosa',183);

            INSERT INTO coincidencias VALUES (1562,'Diamond',184);
            INSERT INTO coincidencias VALUES (1563,'Diner',184);
            INSERT INTO coincidencias VALUES (1564,'Dining',184);
            INSERT INTO coincidencias VALUES (1565,'Funny',184);
            INSERT INTO coincidencias VALUES (1566,'Bonnie',184);
            INSERT INTO coincidencias VALUES (1567,'Bunnie',184);
            INSERT INTO coincidencias VALUES (1568,'Money',184);
            INSERT INTO coincidencias VALUES (1569,'Burning',184);
            INSERT INTO coincidencias VALUES (1570,'Running',184);
            INSERT INTO coincidencias VALUES (1571,'Learning',184);

            INSERT INTO coincidencias VALUES (1572,'Powerll',185);
            INSERT INTO coincidencias VALUES (1573,'Howell',185);
            INSERT INTO coincidencias VALUES (1574,'Hello',185);
            INSERT INTO coincidencias VALUES (1575,'Kalo',185);
            INSERT INTO coincidencias VALUES (1576,'Tower',185);
            INSERT INTO coincidencias VALUES (1577,'Powell',185);
            INSERT INTO coincidencias VALUES (1578,'Power',185);
            INSERT INTO coincidencias VALUES (1579,'Cowell',185);

            INSERT INTO coincidencias VALUES (1580,'Animal',186);
            INSERT INTO coincidencias VALUES (1581,'Elmo',186);
            INSERT INTO coincidencias VALUES (1582,'Ammo',186);
            INSERT INTO coincidencias VALUES (1583,'Tamil',186);
            INSERT INTO coincidencias VALUES (1584,'Camo',186);
            INSERT INTO coincidencias VALUES (1585,'Campbell',186);
            INSERT INTO coincidencias VALUES (1586,'Hamel',186);
            INSERT INTO coincidencias VALUES (1587,'Camera',186);

            INSERT INTO coincidencias VALUES (1588,'The light',187);
            INSERT INTO coincidencias VALUES (1589,'Zillow',187);
            INSERT INTO coincidencias VALUES (1590,'Moonlight',187);
            INSERT INTO coincidencias VALUES (1591,'Phineas',187);
            INSERT INTO coincidencias VALUES (1592,'Sinous ',187);
            INSERT INTO coincidencias VALUES (1593,'Send yes',187);
            INSERT INTO coincidencias VALUES (1594,'Zenius',187);
            INSERT INTO coincidencias VALUES (1595,'Zegna',187);
            INSERT INTO coincidencias VALUES (1596,'Cydia',187);
            INSERT INTO coincidencias VALUES (1597,'Sinya',187);
            INSERT INTO coincidencias VALUES (1598,'Xenia',187);
            INSERT INTO coincidencias VALUES (1599,'Cynthia',187);

            INSERT INTO coincidencias VALUES (1600,'Full',188);
            INSERT INTO coincidencias VALUES (1601,'Bull',188);
            INSERT INTO coincidencias VALUES (1602,'Cole',188);
            INSERT INTO coincidencias VALUES (1603,'Phone',188);
            INSERT INTO coincidencias VALUES (1604,'Boa',188);
            INSERT INTO coincidencias VALUES (1605,'Bolo ',188);
            INSERT INTO coincidencias VALUES (1606,'Boat',188);
            INSERT INTO coincidencias VALUES (1607,'Ball',188);

            INSERT INTO coincidencias VALUES (1608,'Linda',189);
            INSERT INTO coincidencias VALUES (1609,'Windows',189);
            INSERT INTO coincidencias VALUES (1610,'Windo',189);
            INSERT INTO coincidencias VALUES (1611,'Wendell',189);
            INSERT INTO coincidencias VALUES (1612,'When do',189);
            INSERT INTO coincidencias VALUES (1613,'Wendel',189);

            INSERT INTO coincidencias VALUES (1614,'Cirella',190);
            INSERT INTO coincidencias VALUES (1615,'Guerrilla',190);
            INSERT INTO coincidencias VALUES (1616,'Goreela',190);
            INSERT INTO coincidencias VALUES (1617,'Gorilla',190);
            INSERT INTO coincidencias VALUES (1618,'Varela',190);
            INSERT INTO coincidencias VALUES (1619,'Gorillas',190);
            INSERT INTO coincidencias VALUES (1620,'Gorila',190);
            INSERT INTO coincidencias VALUES (1621,'Gorillaz',190);

            INSERT INTO coincidencias VALUES (1622,'c',191);
            INSERT INTO coincidencias VALUES (1623,'see',191);
            INSERT INTO coincidencias VALUES (1624,'si',191);
            

            INSERT INTO coincidencias VALUES (1625,'Home ',192);
            INSERT INTO coincidencias VALUES (1626,'Hole',192);
            INSERT INTO coincidencias VALUES (1627,'Whole',192);
            INSERT INTO coincidencias VALUES (1628,'Paul',192);
            INSERT INTO coincidencias VALUES (1629,'Polo',192);
            INSERT INTO coincidencias VALUES (1630,'Pool',192);
            INSERT INTO coincidencias VALUES (1631,'Pull',192);
            INSERT INTO coincidencias VALUES (1632,'Poll',192);
            INSERT INTO coincidencias VALUES (1633,'Cole',192);
            INSERT INTO coincidencias VALUES (1634,'Call',192);

            INSERT INTO coincidencias VALUES (1635,'Florida',193);
            INSERT INTO coincidencias VALUES (1636,'Slaughter',193);
            INSERT INTO coincidencias VALUES (1637,'Lotto',193);
            INSERT INTO coincidencias VALUES (1638,'Flutter',193);
            INSERT INTO coincidencias VALUES (1639,'Walter',193);
            INSERT INTO coincidencias VALUES (1640,'Daughter',193);
            INSERT INTO coincidencias VALUES (1641,'Order',193);
            INSERT INTO coincidencias VALUES (1642,'Watery',193);
            INSERT INTO coincidencias VALUES (1643,'Weather',193);
            INSERT INTO coincidencias VALUES (1644,'Order',193);

            INSERT INTO coincidencias VALUES (1645,'Buckle',194);
            INSERT INTO coincidencias VALUES (1646,'Buffalo',194);
            INSERT INTO coincidencias VALUES (1647,'Football',194);
            INSERT INTO coincidencias VALUES (1648,'Geico',194);
            INSERT INTO coincidencias VALUES (1649,'Deco',194);
            INSERT INTO coincidencias VALUES (1650,'Gecko',194);
            INSERT INTO coincidencias VALUES (1651,'Goku',194);
            INSERT INTO coincidencias VALUES (1652,'Geico',194);
            INSERT INTO coincidencias VALUES (1653,'Petco',194);
            INSERT INTO coincidencias VALUES (1654,'Decko',194);

            INSERT INTO coincidencias VALUES (1655,'sip',195);
            INSERT INTO coincidencias VALUES (1656,'sick',195);
            INSERT INTO coincidencias VALUES (1657,'ship',195);
            INSERT INTO coincidencias VALUES (1658,'set',195);
            INSERT INTO coincidencias VALUES (1659,'sep',195);

            INSERT INTO coincidencias VALUES (1660,'Bowl',196);
            INSERT INTO coincidencias VALUES (1661,'Full',196);
            INSERT INTO coincidencias VALUES (1662,'Soul',196);
            INSERT INTO coincidencias VALUES (1663,'Fall',196);
            INSERT INTO coincidencias VALUES (1664,'Phone',196);
            INSERT INTO coincidencias VALUES (1665,'Pool',196);
            INSERT INTO coincidencias VALUES (1666,'Fool',196);
            INSERT INTO coincidencias VALUES (1667,'cool',196);

            INSERT INTO coincidencias VALUES (1668,'Home',197);
            INSERT INTO coincidencias VALUES (1669,'Call',197);
            INSERT INTO coincidencias VALUES (1670,'Paul',197);
            INSERT INTO coincidencias VALUES (1671,'Hall',197);
            INSERT INTO coincidencias VALUES (1672,'Cole',197);
            INSERT INTO coincidencias VALUES (1673,'Hend',197);
            INSERT INTO coincidencias VALUES (1674,'Hande',197);
            INSERT INTO coincidencias VALUES (1675,'Send',197);
            INSERT INTO coincidencias VALUES (1676,'And',197);
            INSERT INTO coincidencias VALUES (1677,'Tent',197);
            INSERT INTO coincidencias VALUES (1678,'Can’t',197);
            INSERT INTO coincidencias VALUES (1679,'Kent',197);
            INSERT INTO coincidencias VALUES (1680,'Hunt',197);

            INSERT INTO coincidencias VALUES (1681,'Paul',198);
            INSERT INTO coincidencias VALUES (1682,'Tall',198);
            INSERT INTO coincidencias VALUES (1683,'Hall',198);
            INSERT INTO coincidencias VALUES (1684,'Car',198);
            INSERT INTO coincidencias VALUES (1685,'Kyle',198);
            INSERT INTO coincidencias VALUES (1686,'Cole',198);
            INSERT INTO coincidencias VALUES (1687,'Cause',198);

            INSERT INTO coincidencias VALUES (1688,'So',199);
            INSERT INTO coincidencias VALUES (1689,'Show',199);
            INSERT INTO coincidencias VALUES (1690,'Sol',199);
            INSERT INTO coincidencias VALUES (1691,'Sao',199);
            INSERT INTO coincidencias VALUES (1692,'Sow',199);
            INSERT INTO coincidencias VALUES (1693,'Soon',199);
            INSERT INTO coincidencias VALUES (1694,'Food',199);
            INSERT INTO coincidencias VALUES (1695,'Sune',199);
            INSERT INTO coincidencias VALUES (1696,'Suit soup',199);

            INSERT INTO coincidencias VALUES (1697,'b',200);
            INSERT INTO coincidencias VALUES (1698,'be',200);
            INSERT INTO coincidencias VALUES (1699,'bea',200);
            INSERT INTO coincidencias VALUES (1700,'beef',200);
            INSERT INTO coincidencias VALUES (1701,'p',200);

            INSERT INTO coincidencias VALUES (1702,'t',201);
            INSERT INTO coincidencias VALUES (1703,'tee',201);
            INSERT INTO coincidencias VALUES (1704,'teeth',201);
            INSERT INTO coincidencias VALUES (1705,'team',201);
            INSERT INTO coincidencias VALUES (1706,'key',201);

            INSERT INTO coincidencias VALUES (1707,'Hope',202);
            INSERT INTO coincidencias VALUES (1708,'Hulk',202);
            INSERT INTO coincidencias VALUES (1709,'Home',202);
            INSERT INTO coincidencias VALUES (1710,'Oak',202);
            INSERT INTO coincidencias VALUES (1711,'Hake',202);
            INSERT INTO coincidencias VALUES (1712,'Hey',202);
            INSERT INTO coincidencias VALUES (1713,'Hague',202);
            INSERT INTO coincidencias VALUES (1714,'Jake',202);
            INSERT INTO coincidencias VALUES (1715,'Kake',202);
            INSERT INTO coincidencias VALUES (1716,'Kate',202);
            INSERT INTO coincidencias VALUES (1717,'Case',202);
            INSERT INTO coincidencias VALUES (1718,'Cape',202);
            
            INSERT INTO coincidencias VALUES (1719,'Solo',203);
            INSERT INTO coincidencias VALUES (1720,'Cell',203);
            INSERT INTO coincidencias VALUES (1721,'Sale',203);
            INSERT INTO coincidencias VALUES (1722,'Fail',203);
            INSERT INTO coincidencias VALUES (1723,'Save',203);
            INSERT INTO coincidencias VALUES (1724,'Sales',203);
            INSERT INTO coincidencias VALUES (1725,'Failed',203);

            INSERT INTO coincidencias VALUES (1726,'p',204);
            INSERT INTO coincidencias VALUES (1727,'pee',204);
            INSERT INTO coincidencias VALUES (1728,'pia',204);
            INSERT INTO coincidencias VALUES (1729,'pa',204);
            INSERT INTO coincidencias VALUES (1730,'PA',204);

            INSERT INTO coincidencias VALUES (1731,'Sound',205);
            INSERT INTO coincidencias VALUES (1732,'Town',205);
            INSERT INTO coincidencias VALUES (1733,'Song',205);
            INSERT INTO coincidencias VALUES (1734,'Phone',205);
            INSERT INTO coincidencias VALUES (1735,'Dowd',205);
            INSERT INTO coincidencias VALUES (1736,'Phone',205);
            INSERT INTO coincidencias VALUES (1737,'Doubt',205);

            INSERT INTO coincidencias VALUES (1738,'Google',206);
            INSERT INTO coincidencias VALUES (1739,'Eagles',206);
            INSERT INTO coincidencias VALUES (1740,'Evil',206);
            INSERT INTO coincidencias VALUES (1741,'Legal',206);
            INSERT INTO coincidencias VALUES (1742,'Egal',206);
            INSERT INTO coincidencias VALUES (1743,'Egil',206);
            INSERT INTO coincidencias VALUES (1744,'Regal',206);
            INSERT INTO coincidencias VALUES (1745,'Egle',206);

            INSERT INTO coincidencias VALUES (1746,'New',207);
            INSERT INTO coincidencias VALUES (1747,'Zoom',207);
            INSERT INTO coincidencias VALUES (1748,'You',207);
            INSERT INTO coincidencias VALUES (1749,'Do',207);
            INSERT INTO coincidencias VALUES (1750,'Sue',207);
            INSERT INTO coincidencias VALUES (1751,'Suh',207);
            INSERT INTO coincidencias VALUES (1752,'Sioux',207);
            INSERT INTO coincidencias VALUES (1753,'Soup',207);

            INSERT INTO coincidencias VALUES (1754,'Bull',208);
            INSERT INTO coincidencias VALUES (1755,'blood',208);
            INSERT INTO coincidencias VALUES (1756,'bare',208);
            INSERT INTO coincidencias VALUES (1757,'behr',208);
            INSERT INTO coincidencias VALUES (1758,'bayer',208);
            INSERT INTO coincidencias VALUES (1759,'fair',208);
            INSERT INTO coincidencias VALUES (1760,'barre',208);

            INSERT INTO coincidencias VALUES (1761,'Dodge',209);
            INSERT INTO coincidencias VALUES (1762,'Stock',209);
            INSERT INTO coincidencias VALUES (1763,'Stop',209);
            INSERT INTO coincidencias VALUES (1764,'Todd',209);
            INSERT INTO coincidencias VALUES (1765,'Doc',209);
            INSERT INTO coincidencias VALUES (1766,'Duck',209);
            INSERT INTO coincidencias VALUES (1767,'Dock',209);
            INSERT INTO coincidencias VALUES (1768,'Doug',209);
            INSERT INTO coincidencias VALUES (1769,'Dogs',209);
            INSERT INTO coincidencias VALUES (1770,'Thug',209);

            INSERT INTO coincidencias VALUES (1771,'Cut',210);
            INSERT INTO coincidencias VALUES (1772,'Car',210);
            INSERT INTO coincidencias VALUES (1773,'Kite',210);
            INSERT INTO coincidencias VALUES (1774,'Type',210);
            INSERT INTO coincidencias VALUES (1775,'Kart',210);
            INSERT INTO coincidencias VALUES (1776,'Carte',210);
            INSERT INTO coincidencias VALUES (1777,'Heart',210);
            INSERT INTO coincidencias VALUES (1778,'Card',210);
            INSERT INTO coincidencias VALUES (1779,'Cars',210);
            INSERT INTO coincidencias VALUES (1780,'Carp',210);

            INSERT INTO coincidencias VALUES (1781,'sunlight',211);
            INSERT INTO coincidencias VALUES (1782,'sun rise',211);
            INSERT INTO coincidencias VALUES (1783,'sonrise',211);
            INSERT INTO coincidencias VALUES (1784,'sunrice',211);
            INSERT INTO coincidencias VALUES (1785,'sun rice',211);

            INSERT INTO coincidencias VALUES (1786,'Poem',212);
            INSERT INTO coincidencias VALUES (1787,'Pawn',212);
            INSERT INTO coincidencias VALUES (1788,'Pair',212);
            INSERT INTO coincidencias VALUES (1789,'Pare',212);
            INSERT INTO coincidencias VALUES (1790,'Hair',212);
            INSERT INTO coincidencias VALUES (1791,'Perry',212);
            INSERT INTO coincidencias VALUES (1792,'Pere',212);

            INSERT INTO coincidencias VALUES (1793,'Call',213);
            INSERT INTO coincidencias VALUES (1794,'Paul',213);
            INSERT INTO coincidencias VALUES (1795,'Hi',213);
            INSERT INTO coincidencias VALUES (1796,'All',213);
            INSERT INTO coincidencias VALUES (1797,'Tyre',213);
            INSERT INTO coincidencias VALUES (1798,'Fire',213);
            INSERT INTO coincidencias VALUES (1799,'Car',213);
            INSERT INTO coincidencias VALUES (1800,'Caillou',213);
            INSERT INTO coincidencias VALUES (1801,'Tired',213);

            INSERT INTO coincidencias VALUES (1802,'Call',214);
            INSERT INTO coincidencias VALUES (1803,'Coal',214);
            INSERT INTO coincidencias VALUES (1804,'Go',214);
            INSERT INTO coincidencias VALUES (1805,'Gold',214);
            INSERT INTO coincidencias VALUES (1806,'Cole',214);
            INSERT INTO coincidencias VALUES (1807,'Call',214);

            INSERT INTO coincidencias VALUES (1808,'Sale',215);
            INSERT INTO coincidencias VALUES (1809,'Hair',215);
            INSERT INTO coincidencias VALUES (1810,'Jail',215);
            INSERT INTO coincidencias VALUES (1811,'Yale',215);
            INSERT INTO coincidencias VALUES (1812,'Syria',215);
            INSERT INTO coincidencias VALUES (1813,'Hello',215);
            INSERT INTO coincidencias VALUES (1814,'Serial ',215);
            INSERT INTO coincidencias VALUES (1815,'Serio',215);
            INSERT INTO coincidencias VALUES (1816,'Siri',215);
            INSERT INTO coincidencias VALUES (1817,'Siria',215);

            INSERT INTO coincidencias VALUES (1818,'Bolo',216);
            INSERT INTO coincidencias VALUES (1819,'Below',216);
            INSERT INTO coincidencias VALUES (1820,'LOL',216);
            INSERT INTO coincidencias VALUES (1821,'Pale',216);
            INSERT INTO coincidencias VALUES (1822,'Fail',216);
            INSERT INTO coincidencias VALUES (1823,'Bail',216);
            INSERT INTO coincidencias VALUES (1824,'Jail',216);
            INSERT INTO coincidencias VALUES (1825,'Bell',216);

            INSERT INTO coincidencias VALUES (1826,'Hormonal',217);
            INSERT INTO coincidencias VALUES (1827,'Hormone',217);
            INSERT INTO coincidencias VALUES (1828,'Honolulu',217);
            INSERT INTO coincidencias VALUES (1829,'Tornadoes',217);
            INSERT INTO coincidencias VALUES (1830,'Tornados',217);

            INSERT INTO coincidencias VALUES (1831,'Call',218);
            INSERT INTO coincidencias VALUES (1832,'Paul',218);
            INSERT INTO coincidencias VALUES (1833,'Carr',218);
            INSERT INTO coincidencias VALUES (1834,'Kar',218);
            INSERT INTO coincidencias VALUES (1835,'Kahr',218);
            INSERT INTO coincidencias VALUES (1836,'Karr',218);

            INSERT INTO coincidencias VALUES (1837,'Floral',219);
            INSERT INTO coincidencias VALUES (1838,'Hello',219);
            INSERT INTO coincidencias VALUES (1839,'Nails',219);
            INSERT INTO coincidencias VALUES (1840,'Snail',219);
            INSERT INTO coincidencias VALUES (1841,'Snell',219);
            INSERT INTO coincidencias VALUES (1842,'Nail',219);
            INSERT INTO coincidencias VALUES (1843,'Zales',219);
            INSERT INTO coincidencias VALUES (1844,'Sales',219);

            INSERT INTO coincidencias VALUES (1845,'Hellow',220);
            INSERT INTO coincidencias VALUES (1846,'Holo',220);
            INSERT INTO coincidencias VALUES (1847,'Polo',220);
            INSERT INTO coincidencias VALUES (1848,'Halo',220);
            INSERT INTO coincidencias VALUES (1849,'Solo',220);
            INSERT INTO coincidencias VALUES (1850,'Pale',220);
            INSERT INTO coincidencias VALUES (1851,'Pea',220);
            INSERT INTO coincidencias VALUES (1852,'Pill ',220);
            INSERT INTO coincidencias VALUES (1853,'jail',220);
            INSERT INTO coincidencias VALUES (1854,'hale',220);
            INSERT INTO coincidencias VALUES (1855,'Hello',220);

            INSERT INTO coincidencias VALUES (1856,'code',221);
            INSERT INTO coincidencias VALUES (1857,'home',221);
            INSERT INTO coincidencias VALUES (1858,'Cold ',221);
            INSERT INTO coincidencias VALUES (1859,'Hello',221);
            INSERT INTO coincidencias VALUES (1860,'Kohl’s',221);
            INSERT INTO coincidencias VALUES (1861,'Coah',221);
            INSERT INTO coincidencias VALUES (1862,'Cole ',221);
            INSERT INTO coincidencias VALUES (1863,'Chode',221);
            INSERT INTO coincidencias VALUES (1864,'Towed',221);
            INSERT INTO coincidencias VALUES (1865,'Tote',221);

            INSERT INTO coincidencias VALUES (1866,'Hi',222);
            INSERT INTO coincidencias VALUES (1867,'Wow',222);
            INSERT INTO coincidencias VALUES (1868,'Paul',222);
            INSERT INTO coincidencias VALUES (1869,'Call',222);
            INSERT INTO coincidencias VALUES (1870,'High',222);
            INSERT INTO coincidencias VALUES (1871,'Hai',222);
            INSERT INTO coincidencias VALUES (1872,'Hái',222);
            INSERT INTO coincidencias VALUES (1873,'Hey',222);
            INSERT INTO coincidencias VALUES (1874,'Cal',222);
            INSERT INTO coincidencias VALUES (1875,'Cowl',222);
            INSERT INTO coincidencias VALUES (1876,'Cowell',222);

            INSERT INTO coincidencias VALUES (1877,'sale',223);
            INSERT INTO coincidencias VALUES (1878,'fail',223);
            INSERT INTO coincidencias VALUES (1879,'save',223);
            INSERT INTO coincidencias VALUES (1880,'cell',223);
            INSERT INTO coincidencias VALUES (1881,'sal',223);

            INSERT INTO coincidencias VALUES (1882,'Bust',224);
            INSERT INTO coincidencias VALUES (1883,'Boss',224);
            INSERT INTO coincidencias VALUES (1884,'Butt',224);
            INSERT INTO coincidencias VALUES (1885,'But',224);
            INSERT INTO coincidencias VALUES (1886,'Pat',224);
            INSERT INTO coincidencias VALUES (1887,'Bad',224);
            INSERT INTO coincidencias VALUES (1888,'Fat',224);
            INSERT INTO coincidencias VALUES (1889,'Back',224);
            INSERT INTO coincidencias VALUES (1890,'Fast',224);

            INSERT INTO coincidencias VALUES (1891,'Home ',225);
            INSERT INTO coincidencias VALUES (1892,'Call',225);
            INSERT INTO coincidencias VALUES (1893,'Paul',225);
            INSERT INTO coincidencias VALUES (1894,'All',225);
            INSERT INTO coincidencias VALUES (1895,'10',225);
            INSERT INTO coincidencias VALUES (1896,'tenn',225);

            INSERT INTO coincidencias VALUES (1897,'Cell',226);
            INSERT INTO coincidencias VALUES (1898,'Sell',226);
            INSERT INTO coincidencias VALUES (1899,'Spell',226);
            INSERT INTO coincidencias VALUES (1900,'Girls',226);
            INSERT INTO coincidencias VALUES (1901,'Gurls',226);
            INSERT INTO coincidencias VALUES (1902,'Pearl',226);
            INSERT INTO coincidencias VALUES (1903,'Curl',226);

            INSERT INTO coincidencias VALUES (1904,'Hello',227);
            INSERT INTO coincidencias VALUES (1905,'Hill',227);
            INSERT INTO coincidencias VALUES (1906,'No',227);
            INSERT INTO coincidencias VALUES (1907,'Eel',227);
            INSERT INTO coincidencias VALUES (1908,'Eels',227);
            INSERT INTO coincidencias VALUES (1909,'Heels',227);
            INSERT INTO coincidencias VALUES (1910,'Yo',227);
            INSERT INTO coincidencias VALUES (1911,'Scheels',227);
            INSERT INTO coincidencias VALUES (1912,'Sears',227);
            INSERT INTO coincidencias VALUES (1913,'Sales',227);
            INSERT INTO coincidencias VALUES (1914,'Seal’s',227);
            INSERT INTO coincidencias VALUES (1915,'Feels',227);
            INSERT INTO coincidencias VALUES (1916,'Zio’s',227);

            INSERT INTO coincidencias VALUES (1917,'Home',228);
            INSERT INTO coincidencias VALUES (1918,'Elan',228);
            INSERT INTO coincidencias VALUES (1919,'Poem',228);
            INSERT INTO coincidencias VALUES (1920,'Alarm',228);
            INSERT INTO coincidencias VALUES (1921,'Penn',228);
            INSERT INTO coincidencias VALUES (1922,'Pen 10',228);
            INSERT INTO coincidencias VALUES (1923,'Pound',228);
            INSERT INTO coincidencias VALUES (1924,'Hand',228);

            INSERT INTO coincidencias VALUES (1925,'Blue',229);
            INSERT INTO coincidencias VALUES (1926,'Flu',229);
            INSERT INTO coincidencias VALUES (1927,'Glu',229);
            INSERT INTO coincidencias VALUES (1928,'Blu',229);
            INSERT INTO coincidencias VALUES (1929,'Clue',229);
            INSERT INTO coincidencias VALUES (1930,'Blue',229);
            INSERT INTO coincidencias VALUES (1931,'Cool',229);

            INSERT INTO coincidencias VALUES (1932,'Slime',230);
            INSERT INTO coincidencias VALUES (1933,'One',230);
            INSERT INTO coincidencias VALUES (1934,'Schwan’s',230);
            INSERT INTO coincidencias VALUES (1935,'Swansea',230);
            INSERT INTO coincidencias VALUES (1936,'Swan',230);
            INSERT INTO coincidencias VALUES (1937,'salons',230);
            INSERT INTO coincidencias VALUES (1938,'slugs',230);
            INSERT INTO coincidencias VALUES (1939,'swabs',230);

            INSERT INTO coincidencias VALUES (1940,'Thug',231);
            INSERT INTO coincidencias VALUES (1941,'Pug',231);
            INSERT INTO coincidencias VALUES (1942,'Bugs',231);
            INSERT INTO coincidencias VALUES (1943,'Buzz',231);
            INSERT INTO coincidencias VALUES (1944,'Book',231);
            INSERT INTO coincidencias VALUES (1945,'Buck',231);

            INSERT INTO coincidencias VALUES (1946,'Dumb',232);
            INSERT INTO coincidencias VALUES (1947,'Thumb',232);
            INSERT INTO coincidencias VALUES (1948,'Sun',232);
            INSERT INTO coincidencias VALUES (1949,'Done',232);
            INSERT INTO coincidencias VALUES (1950,'Gun',232);
            INSERT INTO coincidencias VALUES (1951,'Gumn',232);
            INSERT INTO coincidencias VALUES (1952,'Come',232);

            INSERT INTO coincidencias VALUES (1953,'Round bowl',233);
            INSERT INTO coincidencias VALUES (1954,'Soundbard',233);
            INSERT INTO coincidencias VALUES (1955,'Roundball',233);
            INSERT INTO coincidencias VALUES (1956,'Brownbear ',233);
            INSERT INTO coincidencias VALUES (1957,'Brown baer',233);
            INSERT INTO coincidencias VALUES (1958,'Brown.bear',233);
            INSERT INTO coincidencias VALUES (1959,'Brown bears',233);
            INSERT INTO coincidencias VALUES (1960,'Brown fair',233);
            INSERT INTO coincidencias VALUES (1961,'Brown hair',233);

            INSERT INTO coincidencias VALUES (1962,'Poem',234);
            INSERT INTO coincidencias VALUES (1963,'Call',234);
            INSERT INTO coincidencias VALUES (1964,'Paul',234);
            INSERT INTO coincidencias VALUES (1965,'Pull',234);
            INSERT INTO coincidencias VALUES (1966,'Koin',234);
            INSERT INTO coincidencias VALUES (1967,'Coyne',234);
            INSERT INTO coincidencias VALUES (1968,'Cool',234);
            INSERT INTO coincidencias VALUES (1969,'Car',234);
            INSERT INTO coincidencias VALUES (1970,'calling',234);

            INSERT INTO coincidencias VALUES (1971,'Low',235);
            INSERT INTO coincidencias VALUES (1972,'Well',235);
            INSERT INTO coincidencias VALUES (1973,'Load',235);
            INSERT INTO coincidencias VALUES (1974,'Rouge',235);
            INSERT INTO coincidencias VALUES (1975,'Rome',235);
            INSERT INTO coincidencias VALUES (1976,'Road',235);

            INSERT INTO coincidencias VALUES (1977,'Kilala',236);
            INSERT INTO coincidencias VALUES (1978,'Malala',236);
            INSERT INTO coincidencias VALUES (1979,'Corolla',236);
            INSERT INTO coincidencias VALUES (1980,'Koalas',236);
            INSERT INTO coincidencias VALUES (1981,'Colella',236);
            INSERT INTO coincidencias VALUES (1982,'chihuahua',236);

            INSERT INTO coincidencias VALUES (1983,'Low',237);
            INSERT INTO coincidencias VALUES (1984,'Yelp',237);
            INSERT INTO coincidencias VALUES (1985,'Load',237);
            INSERT INTO coincidencias VALUES (1986,'Boat',237);
            INSERT INTO coincidencias VALUES (1987,'What',237);
            INSERT INTO coincidencias VALUES (1988,'Dope',237);
            INSERT INTO coincidencias VALUES (1989,'Soap',237);
            INSERT INTO coincidencias VALUES (1990,'Robe',237);
            INSERT INTO coincidencias VALUES (1991,'nope',237);
            INSERT INTO coincidencias VALUES (1992,'broke',237);
            INSERT INTO coincidencias VALUES (1993,'chrome',237);
            INSERT INTO coincidencias VALUES (1994,'probe',237);
            INSERT INTO coincidencias VALUES (1995,'throat',237);

            INSERT INTO coincidencias VALUES (1996,'qiwi',238);
            INSERT INTO coincidencias VALUES (1997,'pee wee',238);
            INSERT INTO coincidencias VALUES (1998,'pee-wee',238);
            INSERT INTO coincidencias VALUES (1999,'peewee',238);

            INSERT INTO coincidencias VALUES (2000,'Ma',239);
            INSERT INTO coincidencias VALUES (2001,'Map',239);
            INSERT INTO coincidencias VALUES (2002,'Pop',239);
            INSERT INTO coincidencias VALUES (2003,'Mob',239);
            INSERT INTO coincidencias VALUES (2004,'I’m up',239);
            INSERT INTO coincidencias VALUES (2005,'Maps',239);
            INSERT INTO coincidencias VALUES (2006,'Mom',239);

            INSERT INTO coincidencias VALUES (2007,'Style',240);
            INSERT INTO coincidencias VALUES (2008,'Down',240);
            INSERT INTO coincidencias VALUES (2009,'Sound',240);
            INSERT INTO coincidencias VALUES (2010,'Town',240);
            INSERT INTO coincidencias VALUES (2011,'Song',240);
            INSERT INTO coincidencias VALUES (2012,'Doubt',240);
            INSERT INTO coincidencias VALUES (2013,'dowd',240);
            INSERT INTO coincidencias VALUES (2014,'gap',240);
            INSERT INTO coincidencias VALUES (2015,'gallon',240);

            INSERT INTO coincidencias VALUES (2016,'pug',241);
            INSERT INTO coincidencias VALUES (2017,'thug',241);
            INSERT INTO coincidencias VALUES (2018,'rug',241);
            INSERT INTO coincidencias VALUES (2019,'mug',241);

            INSERT INTO coincidencias VALUES (2020,'Yeah',242);
            INSERT INTO coincidencias VALUES (2021,'Yes',242);
            INSERT INTO coincidencias VALUES (2022,'Yea',242);
            INSERT INTO coincidencias VALUES (2023,'Geer',242);
            INSERT INTO coincidencias VALUES (2024,'Deer',242);
            INSERT INTO coincidencias VALUES (2025,'Beer',242);
            INSERT INTO coincidencias VALUES (2026,'Dear',242);
            INSERT INTO coincidencias VALUES (2027,'here',242);

            INSERT INTO coincidencias VALUES (2028,'canoes',243);
            INSERT INTO coincidencias VALUES (2029,'canoo',243);
            INSERT INTO coincidencias VALUES (2030,'kanoo',243);
            INSERT INTO coincidencias VALUES (2031,'cano',243);

            INSERT INTO coincidencias VALUES (2032,'Club',244);
            INSERT INTO coincidencias VALUES (2033,'Cloud',244);
            INSERT INTO coincidencias VALUES (2034,'Clown',244);
            INSERT INTO coincidencias VALUES (2035,'Clay',244);
            INSERT INTO coincidencias VALUES (2036,'Play',244);
            INSERT INTO coincidencias VALUES (2037,'Pro',244);
            INSERT INTO coincidencias VALUES (2038,'Probe',244);
            INSERT INTO coincidencias VALUES (2039,'Chrome',244);
            INSERT INTO coincidencias VALUES (2040,'Grove',244);
            INSERT INTO coincidencias VALUES (2041,'Crowe',244);
            INSERT INTO coincidencias VALUES (2042,'Call',244);

            INSERT INTO coincidencias VALUES (2043,'Stock',245);
            INSERT INTO coincidencias VALUES (2044,'Stop',245);
            INSERT INTO coincidencias VALUES (2045,'Stuff',245);
            INSERT INTO coincidencias VALUES (2046,'Stuck',245);
            INSERT INTO coincidencias VALUES (2047,'Talk',245);
            INSERT INTO coincidencias VALUES (2048,'Sox',245);
            INSERT INTO coincidencias VALUES (2049,'Sucks',245);
            INSERT INTO coincidencias VALUES (2050,'Fox',245);
            INSERT INTO coincidencias VALUES (2051,'Shocks',245);
            INSERT INTO coincidencias VALUES (2052,'Sex',245);

            INSERT INTO coincidencias VALUES (2053,'Papa',246);
            INSERT INTO coincidencias VALUES (2054,'Puppet',246);
            INSERT INTO coincidencias VALUES (2055,'Puppy',246);
            INSERT INTO coincidencias VALUES (2056,'Popper',246);
            INSERT INTO coincidencias VALUES (2057,'Piper',246);
            INSERT INTO coincidencias VALUES (2058,'Paper',246);
            INSERT INTO coincidencias VALUES (2059,'Copper',246);
            INSERT INTO coincidencias VALUES (2060,'Parker',246);

            INSERT INTO coincidencias VALUES (2061,'Puppies',247);
            INSERT INTO coincidencias VALUES (2062,'Happy',247);
            INSERT INTO coincidencias VALUES (2063,'Purple',247);
            INSERT INTO coincidencias VALUES (2064,'Couple',247);
            INSERT INTO coincidencias VALUES (2065,'Papi',247);
            INSERT INTO coincidencias VALUES (2066,'Poppy',247);
            INSERT INTO coincidencias VALUES (2067,'Puppi',247);
            INSERT INTO coincidencias VALUES (2068,'Papa',247);
            INSERT INTO coincidencias VALUES (2069,'Pepe',247);

            INSERT INTO coincidencias VALUES (2070,'Poco',248);
            INSERT INTO coincidencias VALUES (2071,'Cocola',248);
            INSERT INTO coincidencias VALUES (2072,'Coco',248);
            INSERT INTO coincidencias VALUES (2073,'Coca cola ',248);
            INSERT INTO coincidencias VALUES (2074,'Koko',248);
            INSERT INTO coincidencias VALUES (2075,'Koco',248);
            INSERT INTO coincidencias VALUES (2076,'Coco’s',248);

            INSERT INTO coincidencias VALUES (2077,'Hookah',249);
            INSERT INTO coincidencias VALUES (2078,'Okay',249);
            INSERT INTO coincidencias VALUES (2079,'Snickers',249);
            INSERT INTO coincidencias VALUES (2080,'Snickerz',249);
            INSERT INTO coincidencias VALUES (2081,'Snookers',249);
            INSERT INTO coincidencias VALUES (2082,'Snercker',249);

            INSERT INTO coincidencias VALUES (2083,'Animal',250);
            INSERT INTO coincidencias VALUES (2084,'Ammo',250);
            INSERT INTO coincidencias VALUES (2085,'Elmo',250);
            INSERT INTO coincidencias VALUES (2086,'Normal',250);
            INSERT INTO coincidencias VALUES (2087,'Camo',250);
            INSERT INTO coincidencias VALUES (2088,'Camera',250);
            INSERT INTO coincidencias VALUES (2089,'Campbell',250);
            INSERT INTO coincidencias VALUES (2090,'Tamil',250);

            INSERT INTO coincidencias VALUES (2091,'Symbol',251);
            INSERT INTO coincidencias VALUES (2092,'Cymbal',251);
            INSERT INTO coincidencias VALUES (2093,'Mobile',251);
            INSERT INTO coincidencias VALUES (2094,'Football',251);
            INSERT INTO coincidencias VALUES (2095,'Simple',251);
            INSERT INTO coincidencias VALUES (2096,'Cable',251);
            INSERT INTO coincidencias VALUES (2097,'Table',251);
            INSERT INTO coincidencias VALUES (2098,'Kimball',251);
            INSERT INTO coincidencias VALUES (2099,'Kibel',251);

            INSERT INTO coincidencias VALUES (2100,'peanuts',252);
            INSERT INTO coincidencias VALUES (2101,'p-nut',252);
            INSERT INTO coincidencias VALUES (2102,'pnuts',252);
            INSERT INTO coincidencias VALUES (2103,'penis',252);
            INSERT INTO coincidencias VALUES (2104,'pina’s',252);
            INSERT INTO coincidencias VALUES (2105,'peanuts',252);

            INSERT INTO coincidencias VALUES (2106,'Weather',253);
            INSERT INTO coincidencias VALUES (2107,'Twitter',253);
            INSERT INTO coincidencias VALUES (2108,'Water',253);
            INSERT INTO coincidencias VALUES (2109,'Letter',253);
            INSERT INTO coincidencias VALUES (2110,'Shredder',253);
            INSERT INTO coincidencias VALUES (2111,'Swagger',253);
            INSERT INTO coincidencias VALUES (2112,'sweaters',253);
            INSERT INTO coincidencias VALUES (2113,'spider',253);

            INSERT INTO coincidencias VALUES (2114,'Hello',254);
            INSERT INTO coincidencias VALUES (2115,'Emily',254);
            INSERT INTO coincidencias VALUES (2116,'LOL',254);
            INSERT INTO coincidencias VALUES (2117,'Tomatoes',254);
            INSERT INTO coincidencias VALUES (2118,'Tomato’s',254);
            INSERT INTO coincidencias VALUES (2119,'Tornado',254);

            INSERT INTO coincidencias VALUES (2120,'Honey',255);
            INSERT INTO coincidencias VALUES (2121,'Hello',255);
            INSERT INTO coincidencias VALUES (2122,'Penny',255);
            INSERT INTO coincidencias VALUES (2123,'Color',255);
            INSERT INTO coincidencias VALUES (2124,'Tommy',255);
            INSERT INTO coincidencias VALUES (2125,'Tammy',255);
            INSERT INTO coincidencias VALUES (2126,'Tell me',255);
            INSERT INTO coincidencias VALUES (2127,'Tommie',255);

            INSERT INTO coincidencias VALUES (2128,'State',256);
            INSERT INTO coincidencias VALUES (2129,'Stake',256);
            INSERT INTO coincidencias VALUES (2130,'States',256);
            INSERT INTO coincidencias VALUES (2131,'Stick',256);
            INSERT INTO coincidencias VALUES (2132,'Dick',256);
            INSERT INTO coincidencias VALUES (2133,'Speak',256);
            INSERT INTO coincidencias VALUES (2134,'Seek',256);
            INSERT INTO coincidencias VALUES (2135,'Fake',256);

            INSERT INTO coincidencias VALUES (2136,'Swimsuits',257);
            INSERT INTO coincidencias VALUES (2137,'Vincent',257);
            INSERT INTO coincidencias VALUES (2138,'Swimsuits',257);
            INSERT INTO coincidencias VALUES (2139,'Swim suit',257);
            INSERT INTO coincidencias VALUES (2140,'Slim suite',257);
            INSERT INTO coincidencias VALUES (2141,'Flame suit',257);
            INSERT INTO coincidencias VALUES (2142,'Sims 2',257);
            INSERT INTO coincidencias VALUES (2143,'Seems to it',257);
            INSERT INTO coincidencias VALUES (2144,'Simpson',257);

            INSERT INTO coincidencias VALUES (2145,'Sign ',258);
            INSERT INTO coincidencias VALUES (2146,'Son',258);
            INSERT INTO coincidencias VALUES (2147,'Song',258);
            INSERT INTO coincidencias VALUES (2148,'Fun',258);
            INSERT INTO coincidencias VALUES (2149,'Done',258);
            INSERT INTO coincidencias VALUES (2150,'Son',258);
            INSERT INTO coincidencias VALUES (2151,'Then',258);
            INSERT INTO coincidencias VALUES (2152,'Sin',258);
            INSERT INTO coincidencias VALUES (2153,'send',258);

            INSERT INTO coincidencias VALUES (2154,'frisby',259);
            INSERT INTO coincidencias VALUES (2155,'frisbi',259);
            INSERT INTO coincidencias VALUES (2156,'presby',259);
            INSERT INTO coincidencias VALUES (2157,'Frisbees',259);
            INSERT INTO coincidencias VALUES (2158,'Crispy',259);
            INSERT INTO coincidencias VALUES (2159,'Presby',259);
            INSERT INTO coincidencias VALUES (2160,'Frisbie',259);

            INSERT INTO coincidencias VALUES (2161,'Pulled up',260);
            INSERT INTO coincidencias VALUES (2162,'Supportive',260);
            INSERT INTO coincidencias VALUES (2163,'Cicada',260);
            INSERT INTO coincidencias VALUES (2164,'Dakota',260);
            INSERT INTO coincidencias VALUES (2165,'Potatoes',260);
            INSERT INTO coincidencias VALUES (2166,'Potatoe',260);

            INSERT INTO coincidencias VALUES (2167,'tooth paste',261);

            INSERT INTO coincidencias VALUES (2168,'Iron man',262);
            INSERT INTO coincidencias VALUES (2169,'Spiderman',262);
            INSERT INTO coincidencias VALUES (2170,'Firemen',262);
            INSERT INTO coincidencias VALUES (2171,'Fire men',262);
            INSERT INTO coincidencias VALUES (2172,'Fire man',262);
            INSERT INTO coincidencias VALUES (2173,'Tireman',262);
            INSERT INTO coincidencias VALUES (2174,'Spider-man',262);

            INSERT INTO coincidencias VALUES (2175,'Spot',263);
            INSERT INTO coincidencias VALUES (2176,'Fox',263);
            INSERT INTO coincidencias VALUES (2177,'Spa',263);
            INSERT INTO coincidencias VALUES (2178,'Spots',263);
            INSERT INTO coincidencias VALUES (2179,'Stock',263);
            INSERT INTO coincidencias VALUES (2180,'Sports ',263);
            INSERT INTO coincidencias VALUES (2181,'Spot',263);
            INSERT INTO coincidencias VALUES (2182,'Spots',263);
            INSERT INTO coincidencias VALUES (2183,'Slots',263);
            INSERT INTO coincidencias VALUES (2184,'Sparks',263);

            INSERT INTO coincidencias VALUES (2185,'Koka Kola',264);
            INSERT INTO coincidencias VALUES (2186,'Coke a cola',264);
            INSERT INTO coincidencias VALUES (2187,'Koka kola',264);
            INSERT INTO coincidencias VALUES (2188,'Coco cola',264);

            INSERT INTO coincidencias VALUES (2189,'The cloud',265);
            INSERT INTO coincidencias VALUES (2190,'The clown',265);
            INSERT INTO coincidencias VALUES (2191,'The plow',265);
            INSERT INTO coincidencias VALUES (2192,'The crowd',265);
            INSERT INTO coincidencias VALUES (2193,'Silk flowers',265);
            INSERT INTO coincidencias VALUES (2194,'Silk cloth',265);
            INSERT INTO coincidencias VALUES (2195,'Silk plows',265);

            INSERT INTO coincidencias VALUES (2196,'type',266);
            INSERT INTO coincidencias VALUES (2197,'cape',266);
            INSERT INTO coincidencias VALUES (2198,'hype ',266);
            INSERT INTO coincidencias VALUES (2199,'skype',266);
            INSERT INTO coincidencias VALUES (2200,'pies',266);
            INSERT INTO coincidencias VALUES (2201,'fight',266);
            INSERT INTO coincidencias VALUES (2202,'pike',266);

            INSERT INTO coincidencias VALUES (2203,'Polo ',267);
            INSERT INTO coincidencias VALUES (2204,'Paul ',267);
            INSERT INTO coincidencias VALUES (2205,'Hello ',267);
            INSERT INTO coincidencias VALUES (2206,'Pull ',267);
            INSERT INTO coincidencias VALUES (2207,'Call',267);
            INSERT INTO coincidencias VALUES (2208,'Tale',267);
            INSERT INTO coincidencias VALUES (2209,'Kale',267);
            INSERT INTO coincidencias VALUES (2210,'Taylor ',267);
            INSERT INTO coincidencias VALUES (2211,'Tell',267);

            INSERT INTO coincidencias VALUES (2212,'temple',268);
            INSERT INTO coincidencias VALUES (2213,'Pencil',268);
            INSERT INTO coincidencias VALUES (2214,'Sample',268);
            INSERT INTO coincidencias VALUES (2215,'Pom pom',268);
            INSERT INTO coincidencias VALUES (2216,'Bumpkin',268);

            INSERT INTO coincidencias VALUES (2217,'skype',269);
            INSERT INTO coincidencias VALUES (2218,'Sky',269);
            INSERT INTO coincidencias VALUES (2219,'Scar',269);
            INSERT INTO coincidencias VALUES (2220,'Skarf',269);
            INSERT INTO coincidencias VALUES (2221,'Scarff',269);
            INSERT INTO coincidencias VALUES (2222,'Scars',269);
            INSERT INTO coincidencias VALUES (2223,'Car',269);
            INSERT INTO coincidencias VALUES (2224,'Score',269);
            INSERT INTO coincidencias VALUES (2225,'Skaar',269);

            INSERT INTO coincidencias VALUES (2226,'Poor',270);
            INSERT INTO coincidencias VALUES (2227,'Call',270);
            INSERT INTO coincidencias VALUES (2228,'Paul',270);
            INSERT INTO coincidencias VALUES (2229,'Pair',270);
            INSERT INTO coincidencias VALUES (2230,'Hair',270);
            INSERT INTO coincidencias VALUES (2231,'Pare',270);
            INSERT INTO coincidencias VALUES (2232,'pier',270);
            INSERT INTO coincidencias VALUES (2233,'pierre',270);
            INSERT INTO coincidencias VALUES (2234,'here',270);
            INSERT INTO coincidencias VALUES (2235,'beer',270);

            INSERT INTO coincidencias VALUES (2236,'asleep',271);
            INSERT INTO coincidencias VALUES (2237,'the sleep',271);
            INSERT INTO coincidencias VALUES (2238,'slip',271);
            INSERT INTO coincidencias VALUES (2239,'I sleep',271);
            INSERT INTO coincidencias VALUES (2240,'Fleet',271);
            INSERT INTO coincidencias VALUES (2241,'Fleek',271);
            INSERT INTO coincidencias VALUES (2242,'Please',271);
            INSERT INTO coincidencias VALUES (2243,'Free',271);

            INSERT INTO coincidencias VALUES (2244,'home',272);
            INSERT INTO coincidencias VALUES (2245,'Hall',272);
            INSERT INTO coincidencias VALUES (2246,'Paul',272);
            INSERT INTO coincidencias VALUES (2247,'Call',272);
            INSERT INTO coincidencias VALUES (2248,'Poor',272);
            INSERT INTO coincidencias VALUES (2249,'Call',272);
            INSERT INTO coincidencias VALUES (2250,'Pair',272);
            INSERT INTO coincidencias VALUES (2251,'Hair',272);
            INSERT INTO coincidencias VALUES (2252,'Pare',272);

            INSERT INTO coincidencias VALUES (2253,'Muffler',273);
            INSERT INTO coincidencias VALUES (2254,'Rockwell',273);
            INSERT INTO coincidencias VALUES (2255,'Liquid',273);
            INSERT INTO coincidencias VALUES (2256,'Nicholas',273);
            INSERT INTO coincidencias VALUES (2257,'reckless',273);
            INSERT INTO coincidencias VALUES (2258,'Nikolas',273);
            INSERT INTO coincidencias VALUES (2259,'Nicolaus',273);
            INSERT INTO coincidencias VALUES (2260,'Necklaces',273);
            INSERT INTO coincidencias VALUES (2261,'Annapolis',273);
            INSERT INTO coincidencias VALUES (2262,'Net worth',273);
            INSERT INTO coincidencias VALUES (2263,'Net worth',273);

            INSERT INTO coincidencias VALUES (2264,'Call',274);
            INSERT INTO coincidencias VALUES (2265,'Home',274);
            INSERT INTO coincidencias VALUES (2267,'Horn',274);
            INSERT INTO coincidencias VALUES (2268,'Kin',274);
            INSERT INTO coincidencias VALUES (2269,'Ken',274);
            INSERT INTO coincidencias VALUES (2270,'Cam',274);
            INSERT INTO coincidencias VALUES (2271,'I can',274);
            INSERT INTO coincidencias VALUES (2272,'10',274);

            INSERT INTO coincidencias VALUES (2273,'Hi',275);
            INSERT INTO coincidencias VALUES (2274,'Paul',275);
            INSERT INTO coincidencias VALUES (2275,'Car',275);
            INSERT INTO coincidencias VALUES (2276,'call',275);
            INSERT INTO coincidencias VALUES (2277,'pause',275);
            INSERT INTO coincidencias VALUES (2278,'posh',275);
            INSERT INTO coincidencias VALUES (2279,'pass',275);
            INSERT INTO coincidencias VALUES (2280,'paul ',275);
  
            INSERT INTO coincidencias VALUES (2281,'Call',276); 
            INSERT INTO coincidencias VALUES (2282,'Paul',276); 
            INSERT INTO coincidencias VALUES (2283,'Home',276); 
            INSERT INTO coincidencias VALUES (2284,'Cool',276);    
            INSERT INTO coincidencias VALUES (2285,'Pool',276); 
            INSERT INTO coincidencias VALUES (2286,'KO',276); 
            INSERT INTO coincidencias VALUES (2287,'Cale',276); 
            INSERT INTO coincidencias VALUES (2288,'Kayla',276); 
            INSERT INTO coincidencias VALUES (2289,'Kayou',276); 
            INSERT INTO coincidencias VALUES (2290,'Que año',276); 

            INSERT INTO coincidencias VALUES (2291,'Sensing',277); 
            INSERT INTO coincidencias VALUES (2292,'Fencing',277);
            INSERT INTO coincidencias VALUES (2293,'Pencil',277);
            INSERT INTO coincidencias VALUES (2294,'Sunday',277);
            INSERT INTO coincidencias VALUES (2295,'Son screen',277);
            INSERT INTO coincidencias VALUES (2296,'Sunscreens',277);
            INSERT INTO coincidencias VALUES (2297,'Sun screen',277);
            INSERT INTO coincidencias VALUES (2298,'Phone screen',277);

            INSERT INTO coincidencias VALUES (2299,'Helena',278);
            INSERT INTO coincidencias VALUES (2300,'Hello',278);
            INSERT INTO coincidencias VALUES (2301,'Toe nail',278);
            INSERT INTO coincidencias VALUES (2302,'Toenails',278);
            INSERT INTO coincidencias VALUES (2303,'Toe nails',278);
            INSERT INTO coincidencias VALUES (2304,'Total',278);

            INSERT INTO coincidencias VALUES (2305,'Map',279);
            INSERT INTO coincidencias VALUES (2306,'Net',279);
            INSERT INTO coincidencias VALUES (2307,'Yep',279);
            INSERT INTO coincidencias VALUES (2308,'Net',279);
            INSERT INTO coincidencias VALUES (2309,'Mix',279);
            INSERT INTO coincidencias VALUES (2310,'Nerf',279);
            INSERT INTO coincidencias VALUES (2311,'Nek',279);
            INSERT INTO coincidencias VALUES (2312,'Nex',279);
            INSERT INTO coincidencias VALUES (2313,'Nick',279);

            INSERT INTO coincidencias VALUES (2314,'carat',280);
            INSERT INTO coincidencias VALUES (2315,'karat',280);
            INSERT INTO coincidencias VALUES (2316,'carrots',280);
            INSERT INTO coincidencias VALUES (2317,'parrot',280);
            INSERT INTO coincidencias VALUES (2318,'carriage',280);
            INSERT INTO coincidencias VALUES (2319,'Carol',280);
            INSERT INTO coincidencias VALUES (2320,'Karen',280);

            INSERT INTO coincidencias VALUES (2321,'andale',281);
            INSERT INTO coincidencias VALUES (2322,'Handle',281);
            INSERT INTO coincidencias VALUES (2323,'And all',281);
            INSERT INTO coincidencias VALUES (2324,'Sandro',281);
            INSERT INTO coincidencias VALUES (2325,'Sandra’s',281);
            INSERT INTO coincidencias VALUES (2326,'Sandos',281);
            INSERT INTO coincidencias VALUES (2327,'Sandro’s',281);
            INSERT INTO coincidencias VALUES (2328,'Santos ',281);
            INSERT INTO coincidencias VALUES (2329,'Randall’s',281);
            INSERT INTO coincidencias VALUES (2330,'Handles',281);
            INSERT INTO coincidencias VALUES (2331,'Candles',281);

            INSERT INTO coincidencias VALUES (2332,'sale',282);
            INSERT INTO coincidencias VALUES (2333,'fail',282);
            INSERT INTO coincidencias VALUES (2334,'save',282);
            INSERT INTO coincidencias VALUES (2335,'cell',282);
            INSERT INTO coincidencias VALUES (2336,'sello',282);
            INSERT INTO coincidencias VALUES (2337,'sailor',282);

            INSERT INTO coincidencias VALUES (2338,'Full',283);
            INSERT INTO coincidencias VALUES (2339,'Fool',283);
            INSERT INTO coincidencias VALUES (2340,'Cool',283);
            INSERT INTO coincidencias VALUES (2341,'Pull',283);
            INSERT INTO coincidencias VALUES (2342,'Pou',283);
            INSERT INTO coincidencias VALUES (2343,'Col',283);
            INSERT INTO coincidencias VALUES (2344,'Poole',283);
            INSERT INTO coincidencias VALUES (2345,'School',283);

            INSERT INTO coincidencias VALUES (2346,'Play-doh',284);
            INSERT INTO coincidencias VALUES (2347,'Hello',284);
            INSERT INTO coincidencias VALUES (2348,'Mobile',284);
            INSERT INTO coincidencias VALUES (2349,'Slidell',284);
            INSERT INTO coincidencias VALUES (2350,'Tornado',284);
            INSERT INTO coincidencias VALUES (2351,'Toledo',284);
            INSERT INTO coincidencias VALUES (2352,'Tomatoes',284);

            INSERT INTO coincidencias VALUES (2353,'bootz',285);
            INSERT INTO coincidencias VALUES (2354,'foots',285);
            INSERT INTO coincidencias VALUES (2355,'hoots',285);
            INSERT INTO coincidencias VALUES (2356,'boats',285);
            INSERT INTO coincidencias VALUES (2357,'bots',285);
            INSERT INTO coincidencias VALUES (2358,'food',285);
            INSERT INTO coincidencias VALUES (2359,'books',285);
            INSERT INTO coincidencias VALUES (2360,'fruits',285);
            INSERT INTO coincidencias VALUES (2361,'foods',285);

            INSERT INTO coincidencias VALUES (2362,'burn',286);
            INSERT INTO coincidencias VALUES (2363,'cern',286);
            INSERT INTO coincidencias VALUES (2364,'firm',286);
            INSERT INTO coincidencias VALUES (2365,'vern',286);
            INSERT INTO coincidencias VALUES (2366,'her',286);
            INSERT INTO coincidencias VALUES (2367,'fur',286);
            INSERT INTO coincidencias VALUES (2368,'fer',286);

            INSERT INTO coincidencias VALUES (2369,'Dope',287);
            INSERT INTO coincidencias VALUES (2370,'Pope',287);
            INSERT INTO coincidencias VALUES (2371,'Help',287);
            INSERT INTO coincidencias VALUES (2372,'Nope',287);
            INSERT INTO coincidencias VALUES (2373,'Soak',287);
            INSERT INTO coincidencias VALUES (2374,'so',287);

            INSERT INTO coincidencias VALUES (2375,'t',288);
            INSERT INTO coincidencias VALUES (2376,'teen',288);
            INSERT INTO coincidencias VALUES (2377,'teeth',288);
            INSERT INTO coincidencias VALUES (2378,'team',288);
            INSERT INTO coincidencias VALUES (2379,'key',288);

            INSERT INTO coincidencias VALUES (2380,'Some glasses',289);
            INSERT INTO coincidencias VALUES (2381,'Sunglass',289);
            INSERT INTO coincidencias VALUES (2382,'Son clases',289);
            INSERT INTO coincidencias VALUES (2383,'San clases',289);
            INSERT INTO coincidencias VALUES (2384,'Sungrazer',289);

            INSERT INTO coincidencias VALUES (2385,'Call',290);
            INSERT INTO coincidencias VALUES (2386,'Cole',290);
            INSERT INTO coincidencias VALUES (2387,'Cool',290);
            INSERT INTO coincidencias VALUES (2388,'Calling',290);
            INSERT INTO coincidencias VALUES (2389,'Join',290);
            INSERT INTO coincidencias VALUES (2390,'Home',290);
            INSERT INTO coincidencias VALUES (2391,'Cohen',290);
            INSERT INTO coincidencias VALUES (2392,'Car',290);
            INSERT INTO coincidencias VALUES (2393,'koi',290);

            INSERT INTO coincidencias VALUES (2394,'Ola',291);
            INSERT INTO coincidencias VALUES (2395,'Hola',291);
            INSERT INTO coincidencias VALUES (2396,'Ella',291);
            INSERT INTO coincidencias VALUES (2397,'Hello',291);
            INSERT INTO coincidencias VALUES (2398,'Ford',291);
            INSERT INTO coincidencias VALUES (2399,'4',291);
            INSERT INTO coincidencias VALUES (2400,'for',291);

            INSERT INTO coincidencias VALUES (2401,'An apple',292);
            INSERT INTO coincidencias VALUES (2402,'Renapo',292);
            INSERT INTO coincidencias VALUES (2403,'Pine apple',292);

            INSERT INTO coincidencias VALUES (2404,'Stocking',293);
            INSERT INTO coincidencias VALUES (2405,'Stocker',293);
            INSERT INTO coincidencias VALUES (2406,'Stopping',293);
            INSERT INTO coincidencias VALUES (2407,'Talking',293);
            INSERT INTO coincidencias VALUES (2408,'Soccer',293);
            INSERT INTO coincidencias VALUES (2409,'Tokens',293);
            INSERT INTO coincidencias VALUES (2410,'Stalking',293);
            INSERT INTO coincidencias VALUES (2411,'starkey',293);

            INSERT INTO coincidencias VALUES (2412,'Home',294);
            INSERT INTO coincidencias VALUES (2413,'Poem',294);
            INSERT INTO coincidencias VALUES (2414,'Horn',294);
            INSERT INTO coincidencias VALUES (2415,'Paul',294);
            INSERT INTO coincidencias VALUES (2416,'Fan',294);
            INSERT INTO coincidencias VALUES (2417,'Tom',294);
            INSERT INTO coincidencias VALUES (2418,'Pon',294);
            INSERT INTO coincidencias VALUES (2419,'Pad',294);
            INSERT INTO coincidencias VALUES (2420,'Pam',294);
            INSERT INTO coincidencias VALUES (2421,'Pen',294);
            INSERT INTO coincidencias VALUES (2422,'penn',294);

            INSERT INTO coincidencias VALUES (2423,'Hub',295);
            INSERT INTO coincidencias VALUES (2424,'Pub',295);
            INSERT INTO coincidencias VALUES (2425,'Herb',295);
            INSERT INTO coincidencias VALUES (2426,'Head',295);
            INSERT INTO coincidencias VALUES (2427,'Had',295);
            INSERT INTO coincidencias VALUES (2428,'Chat',295);
            INSERT INTO coincidencias VALUES (2429,'Pop',295);
            INSERT INTO coincidencias VALUES (2430,'SAT',295);
            INSERT INTO coincidencias VALUES (2431,'Tough',295);
            INSERT INTO coincidencias VALUES (2432,'Top',295);
            INSERT INTO coincidencias VALUES (2433,'Thai',295);
            INSERT INTO coincidencias VALUES (2434,'Tongue',295);
            INSERT INTO coincidencias VALUES (2435,'Talk',295);

            INSERT INTO coincidencias VALUES (2436,'Of coruse',296);
            INSERT INTO coincidencias VALUES (2437,'Up corn',296);
            INSERT INTO coincidencias VALUES (2438,'Pop Corn',296);
            INSERT INTO coincidencias VALUES (2439,'Pop korn',296);
            INSERT INTO coincidencias VALUES (2440,'Popcorning',296);
            INSERT INTO coincidencias VALUES (2441,'Popcorned',296);
            INSERT INTO coincidencias VALUES (2442,'Pop chord',296);

            INSERT INTO coincidencias VALUES (2443,'Ospital',297);
            INSERT INTO coincidencias VALUES (2444,'Spittle',297);
            INSERT INTO coincidencias VALUES (2445,'Capital',297);
            INSERT INTO coincidencias VALUES (2446,'hospitals',297);

            INSERT INTO coincidencias VALUES (2447,'Polo',298);
            INSERT INTO coincidencias VALUES (2448,'Hello',298);
            INSERT INTO coincidencias VALUES (2449,'Call',298);
            INSERT INTO coincidencias VALUES (2450,'Paul',298);
            INSERT INTO coincidencias VALUES (2451,'Cool',298);
            INSERT INTO coincidencias VALUES (2452,'Home',298);
            INSERT INTO coincidencias VALUES (2453,'Taylor',298);
            INSERT INTO coincidencias VALUES (2454,'Kale',298);
            INSERT INTO coincidencias VALUES (2455,'Pale',298);
            INSERT INTO coincidencias VALUES (2456,'Tell',298);
            INSERT INTO coincidencias VALUES (2457,'tale',298);

            INSERT INTO coincidencias VALUES (2458,'Powell',299);
            INSERT INTO coincidencias VALUES (2459,'Howell',299);
            INSERT INTO coincidencias VALUES (2460,'Hello',299);
            INSERT INTO coincidencias VALUES (2461,'Kalo',299);
            INSERT INTO coincidencias VALUES (2462,'Power',299);
            INSERT INTO coincidencias VALUES (2463,'Shower',299);
            INSERT INTO coincidencias VALUES (2464,'Howard',299);
            INSERT INTO coincidencias VALUES (2465,'Tower',299);
            INSERT INTO coincidencias VALUES (2466,'tallow',299);

            INSERT INTO coincidencias VALUES (2467,'Tesla',300);
            INSERT INTO coincidencias VALUES (2468,'What',300);
            INSERT INTO coincidencias VALUES (2469,'Cartier',300);
            INSERT INTO coincidencias VALUES (2470,'Tortillas',300);
            INSERT INTO coincidencias VALUES (2471,'Cartia',300);
            INSERT INTO coincidencias VALUES (2472,'Car tia',300);

            INSERT INTO coincidencias VALUES (2473,'Sinhala',301);
            INSERT INTO coincidencias VALUES (2474,'Synonym',301);
            INSERT INTO coincidencias VALUES (2475,'Cinnamon',301);
            INSERT INTO coincidencias VALUES (2476,'Similar',301);
            INSERT INTO coincidencias VALUES (2477,'Sinema',301);
            INSERT INTO coincidencias VALUES (2478,'Panama',301);
            INSERT INTO coincidencias VALUES (2479,'Cinnamon',301);
            INSERT INTO coincidencias VALUES (2480,'Cinéma',301);

            INSERT INTO coincidencias VALUES (2481,'Coach',302);
            INSERT INTO coincidencias VALUES (2482,'Pope',302);
            INSERT INTO coincidencias VALUES (2483,'Hope',302);
            INSERT INTO coincidencias VALUES (2484,'Colt ',302);
            INSERT INTO coincidencias VALUES (2485,'Haut',302);
            INSERT INTO coincidencias VALUES (2486,'Cult',302);
            INSERT INTO coincidencias VALUES (2487,'Code',302);
            INSERT INTO coincidencias VALUES (2488,'coats',302);

            INSERT INTO coincidencias VALUES (2489,'Fall',303);
            INSERT INTO coincidencias VALUES (2490,'Paul',303);
            INSERT INTO coincidencias VALUES (2491,'Call',303);
            INSERT INTO coincidencias VALUES (2492,'Song',303);

            INSERT INTO coincidencias VALUES (2493,'Pulled up',304);
            INSERT INTO coincidencias VALUES (2494,'Supportive',304);
            INSERT INTO coincidencias VALUES (2495,'Cicada',304);
            INSERT INTO coincidencias VALUES (2496,'Dakota',304);
            INSERT INTO coincidencias VALUES (2497,'Potatoe',304);
            INSERT INTO coincidencias VALUES (2498,'potatoes',304);

            INSERT INTO coincidencias VALUES (2499,'Medium',305);
            INSERT INTO coincidencias VALUES (2500,'Easy and',305);
            INSERT INTO coincidencias VALUES (2501,'Juicy m',305);
            INSERT INTO coincidencias VALUES (2502,'Museum',305);
            INSERT INTO coincidencias VALUES (2503,'museums',305);

            INSERT INTO coincidencias VALUES (2504,'Hot',306);
            INSERT INTO coincidencias VALUES (2505,'Hi',306);
            INSERT INTO coincidencias VALUES (2506,'Heart',306);
            INSERT INTO coincidencias VALUES (2507,'Popped',306);
            INSERT INTO coincidencias VALUES (2508,'Pop',306);
            INSERT INTO coincidencias VALUES (2509,'Hot',306);
            INSERT INTO coincidencias VALUES (2510,'Papa',306);

            INSERT INTO coincidencias VALUES (2511,'Step',307);
            INSERT INTO coincidencias VALUES (2512,'Steps',307);
            INSERT INTO coincidencias VALUES (2513,'Stats',307);
            INSERT INTO coincidencias VALUES (2514,'Stitch',307);
            INSERT INTO coincidencias VALUES (2515,'Stic',307);
            INSERT INTO coincidencias VALUES (2516,'Steak',307);
            INSERT INTO coincidencias VALUES (2517,'dick',307);

            INSERT INTO coincidencias VALUES (2518,'Lamb',308);
            INSERT INTO coincidencias VALUES (2519,'Slam',308);
            INSERT INTO coincidencias VALUES (2520,'Club',308);
            INSERT INTO coincidencias VALUES (2521,'Phlegm',308);
            INSERT INTO coincidencias VALUES (2522,'Come',308);
            INSERT INTO coincidencias VALUES (2523,'Prom',308);
            INSERT INTO coincidencias VALUES (2524,'Club',308);
            INSERT INTO coincidencias VALUES (2525,'call',308);
            
            INSERT INTO coincidencias VALUES (2526,'Septic',309);
            INSERT INTO coincidencias VALUES (2527,'Setting',309);
            INSERT INTO coincidencias VALUES (2528,'Sercus',309);
            INSERT INTO coincidencias VALUES (2529,'Surkus',309);
            INSERT INTO coincidencias VALUES (2530,'Circus',309);
            INSERT INTO coincidencias VALUES (2531,'service',309);

            INSERT INTO coincidencias VALUES (2532,'Kaiser',310);
            INSERT INTO coincidencias VALUES (2533,'Either',310);
            INSERT INTO coincidencias VALUES (2534,'Ida',310);
            INSERT INTO coincidencias VALUES (2535,'Weather',310);
            INSERT INTO coincidencias VALUES (2536,'Cyber',310);
            INSERT INTO coincidencias VALUES (2537,'Tyger',310);
            INSERT INTO coincidencias VALUES (2538,'Tyga',310);
            INSERT INTO coincidencias VALUES (2539,'Thaiger',310);
            INSERT INTO coincidencias VALUES (2540,'tigard',310);

            INSERT INTO coincidencias VALUES (2541,'Phone',311);
            INSERT INTO coincidencias VALUES (2542,'Stone',311);
            INSERT INTO coincidencias VALUES (2543,'Song',311);
            INSERT INTO coincidencias VALUES (2544,'Boat',311);
            INSERT INTO coincidencias VALUES (2545,'Bones',311);
            INSERT INTO coincidencias VALUES (2546,'cone',311);

            INSERT INTO coincidencias VALUES (2547,'Open up',312);
            INSERT INTO coincidencias VALUES (2548,'Hooking up',312);
            INSERT INTO coincidencias VALUES (2549,'Hope not',312);
            INSERT INTO coincidencias VALUES (2550,'Cutco knives',312);
            INSERT INTO coincidencias VALUES (2551,'Call car note',312);
            INSERT INTO coincidencias VALUES (2552,'Call clannad',312);

            INSERT INTO coincidencias VALUES (2553,'c',313);
            INSERT INTO coincidencias VALUES (2554,'see',313);
            INSERT INTO coincidencias VALUES (2555,'CE',313);
            INSERT INTO coincidencias VALUES (2556,'SI',313);

            INSERT INTO coincidencias VALUES (2557,'cats',314);
            INSERT INTO coincidencias VALUES (2558,'kat',314);
            INSERT INTO coincidencias VALUES (2559,'katz',314);
            INSERT INTO coincidencias VALUES (2560,'katt',314);
            INSERT INTO coincidencias VALUES (2561,'cast',314);

            INSERT INTO coincidencias VALUES (2562,'Time',315);
            INSERT INTO coincidencias VALUES (2563,'Sign',315);
            INSERT INTO coincidencias VALUES (2564,'Fine',315);
            INSERT INTO coincidencias VALUES (2565,'Fun',315);
            INSERT INTO coincidencias VALUES (2566,'Sponch',315);
            INSERT INTO coincidencias VALUES (2567,'Spunj',315);
            INSERT INTO coincidencias VALUES (2568,'Punch ',315);
            INSERT INTO coincidencias VALUES (2569,'Spanish ',315);

            INSERT INTO coincidencias VALUES (2570,'Pipe',316);
            INSERT INTO coincidencias VALUES (2571,'Type',316);
            INSERT INTO coincidencias VALUES (2572,'Hope',316);
            INSERT INTO coincidencias VALUES (2573,'Hot',316);
            INSERT INTO coincidencias VALUES (2574,'Cards',316);
            INSERT INTO coincidencias VALUES (2575,'Cars',316);
            INSERT INTO coincidencias VALUES (2576,'card',316);

            INSERT INTO coincidencias VALUES (2577,'Gouda',317);
            INSERT INTO coincidencias VALUES (2578,'Buddha',317);
            INSERT INTO coincidencias VALUES (2579,'Cooter',317);
            INSERT INTO coincidencias VALUES (2580,'Scooters',317);
            INSERT INTO coincidencias VALUES (2581,'Schooner',317);
            INSERT INTO coincidencias VALUES (2582,'Schooners',317);
            INSERT INTO coincidencias VALUES (2583,'Scooter’s',317);

            INSERT INTO coincidencias VALUES (2584,'Puppies',318);
            INSERT INTO coincidencias VALUES (2585,'Pepe',318);
            INSERT INTO coincidencias VALUES (2586,'Happy',318);
            INSERT INTO coincidencias VALUES (2587,'Pepper',318);
            INSERT INTO coincidencias VALUES (2588,'Poppy',318);
            INSERT INTO coincidencias VALUES (2589,'Papi',318);

            INSERT INTO coincidencias VALUES (2590,'Simple',319);
            INSERT INTO coincidencias VALUES (2591,'Sample',319);
            INSERT INTO coincidencias VALUES (2592,'Temple',319);
            INSERT INTO coincidencias VALUES (2593,'Champu',319);
            INSERT INTO coincidencias VALUES (2594,'Shampoos',319);
            INSERT INTO coincidencias VALUES (2595,'Champoux',319);
            INSERT INTO coincidencias VALUES (2596,'shampooed',319);

            INSERT INTO coincidencias VALUES (2597,'Ethical',320);
            INSERT INTO coincidencias VALUES (2598,'Icicle',320);
            INSERT INTO coincidencias VALUES (2599,'Ithaca',320);
            INSERT INTO coincidencias VALUES (2600,'bicycles',320);

            INSERT INTO coincidencias VALUES (2601,'Hello',321);
            INSERT INTO coincidencias VALUES (2602,'Hell',321);
            INSERT INTO coincidencias VALUES (2603,'Helen',321);
            INSERT INTO coincidencias VALUES (2604,'Kohl’s',321);
            INSERT INTO coincidencias VALUES (2605,'Dos',321);
            INSERT INTO coincidencias VALUES (2606,'Toast',321);
            INSERT INTO coincidencias VALUES (2607,'chose',321);

            INSERT INTO coincidencias VALUES (2608,'thrush',322);
            INSERT INTO coincidencias VALUES (2609,'crush',322);
            INSERT INTO coincidencias VALUES (2610,'rush',322);
            INSERT INTO coincidencias VALUES (2611,'brushed',322);
            INSERT INTO coincidencias VALUES (2612,'breath',322);
            INSERT INTO coincidencias VALUES (2613,'brass',322);
            INSERT INTO coincidencias VALUES (2614,'bras',322);

            INSERT INTO coincidencias VALUES (2615,'buss',323);
            INSERT INTO coincidencias VALUES (2616,'buzz',323);
            INSERT INTO coincidencias VALUES (2617,'bust',323);
            INSERT INTO coincidencias VALUES (2618,'but',323);

            INSERT INTO coincidencias VALUES (2619,'Call',324);
            INSERT INTO coincidencias VALUES (2620,'Home',324);
            INSERT INTO coincidencias VALUES (2621,'Paul',324);
            INSERT INTO coincidencias VALUES (2622,'Hulk',324);
            INSERT INTO coincidencias VALUES (2623,'Carat',324);
            INSERT INTO coincidencias VALUES (2624,'Karat',324);
            INSERT INTO coincidencias VALUES (2625,'Parrot ',324);
            INSERT INTO coincidencias VALUES (2626,'Kara',324);

            INSERT INTO coincidencias VALUES (2627,'Powerll',325);
            INSERT INTO coincidencias VALUES (2628,'Howell',325);
            INSERT INTO coincidencias VALUES (2629,'Hello',325);
            INSERT INTO coincidencias VALUES (2630,'Kalo',325);
            INSERT INTO coincidencias VALUES (2631,'Tower',325);
            INSERT INTO coincidencias VALUES (2632,'Cowell',325);
            INSERT INTO coincidencias VALUES (2633,'towell',325);

            INSERT INTO coincidencias VALUES (2634,'Happy',326);
            INSERT INTO coincidencias VALUES (2635,'hockey',326);
            INSERT INTO coincidencias VALUES (2636,'Coffee',326);
            INSERT INTO coincidencias VALUES (2637,'Puppy',326);
            INSERT INTO coincidencias VALUES (2638,'Taksi',326);
            INSERT INTO coincidencias VALUES (2639,'Cassie',326);
            INSERT INTO coincidencias VALUES (2640,'Taxis',326);
            INSERT INTO coincidencias VALUES (2641,'tennessee',326);

            INSERT INTO coincidencias VALUES (2642,'Symbol',327);
            INSERT INTO coincidencias VALUES (2643,'Cymbal',327);
            INSERT INTO coincidencias VALUES (2644,'Mobile',327);
            INSERT INTO coincidencias VALUES (2645,'Football',327);
            INSERT INTO coincidencias VALUES (2646,'Simple',327);
            INSERT INTO coincidencias VALUES (2647,'Cable',327);
            INSERT INTO coincidencias VALUES (2648,'Kimball',327);
            INSERT INTO coincidencias VALUES (2649,'Pitbull',327);
            INSERT INTO coincidencias VALUES (2650,'campbell',327);

            INSERT INTO coincidencias VALUES (2651,'Phil',328);
            INSERT INTO coincidencias VALUES (2652,'Sales',328);
            INSERT INTO coincidencias VALUES (2653,'Cell',328);
            INSERT INTO coincidencias VALUES (2654,'Bealls',328);
            INSERT INTO coincidencias VALUES (2655,'Bells',328);
            INSERT INTO coincidencias VALUES (2656,'Bill’s',328);
            INSERT INTO coincidencias VALUES (2657,'Pill’s',328);

            INSERT INTO coincidencias VALUES (2658,'Full',329);
            INSERT INTO coincidencias VALUES (2659,'Bull',329);
            INSERT INTO coincidencias VALUES (2660,'Cole',329);
            INSERT INTO coincidencias VALUES (2661,'Phone',329);
            INSERT INTO coincidencias VALUES (2662,'Boa',329);
            INSERT INTO coincidencias VALUES (2663,'Paul',329);
            INSERT INTO coincidencias VALUES (2664,'Call',329);

            INSERT INTO coincidencias VALUES (2665,'Sex',330);
            INSERT INTO coincidencias VALUES (2666,'Set',330);
            INSERT INTO coincidencias VALUES (2667,'Suck',330);
            INSERT INTO coincidencias VALUES (2668,'Chex',330);
            INSERT INTO coincidencias VALUES (2669,'Text',330);
            INSERT INTO coincidencias VALUES (2670,'Check',330);
            INSERT INTO coincidencias VALUES (2671,'Chest',330);

            INSERT INTO coincidencias VALUES (2672,'Sacin',331);
            INSERT INTO coincidencias VALUES (2673,'Soften',331);
            INSERT INTO coincidencias VALUES (2674,'Boston',331);
            INSERT INTO coincidencias VALUES (2675,'Sauces',331);
            INSERT INTO coincidencias VALUES (2676,'Saucing',331);
            INSERT INTO coincidencias VALUES (2677,'Faucet',331);
            INSERT INTO coincidencias VALUES (2678,'syosset',331);

            INSERT INTO coincidencias VALUES (2679,'Poem',332);
            INSERT INTO coincidencias VALUES (2680,'Home',332);
            INSERT INTO coincidencias VALUES (2681,'Call',332);
            INSERT INTO coincidencias VALUES (2682,'Oil',332);
            INSERT INTO coincidencias VALUES (2683,'All',332);
            INSERT INTO coincidencias VALUES (2684,'Coin',332);
            INSERT INTO coincidencias VALUES (2685,'Toys',332);
            INSERT INTO coincidencias VALUES (2686,'Kohl’s',332);

            INSERT INTO coincidencias VALUES (2687,'Turkey',333);
            INSERT INTO coincidencias VALUES (2688,'Cooking',333);
            INSERT INTO coincidencias VALUES (2689,'Kookie',333);
            INSERT INTO coincidencias VALUES (2690,'Cuckoo',333);
            INSERT INTO coincidencias VALUES (2691,'Kuki',333);
            INSERT INTO coincidencias VALUES (2692,'pookie',333);

            INSERT INTO coincidencias VALUES (2693,'Credit one',334);
            INSERT INTO coincidencias VALUES (2694,'Kreditkart',334);
            INSERT INTO coincidencias VALUES (2695,'Credit card',334);
            INSERT INTO coincidencias VALUES (2696,'Credit-cards',334);
            INSERT INTO coincidencias VALUES (2697,'Credit-card',334);

            INSERT INTO coincidencias VALUES (2698,'Plus',335);
            INSERT INTO coincidencias VALUES (2699,'Call',335);
            INSERT INTO coincidencias VALUES (2700,'Club',335);
            INSERT INTO coincidencias VALUES (2701,'Clay',335);
            INSERT INTO coincidencias VALUES (2702,'Klay',335);
            INSERT INTO coincidencias VALUES (2703,'Pray ',335);
            INSERT INTO coincidencias VALUES (2704,'la',335);

            INSERT INTO coincidencias VALUES (2705,'Cam',336);
            INSERT INTO coincidencias VALUES (2706,'Kamp',336);
            INSERT INTO coincidencias VALUES (2707,'Can',336);
            INSERT INTO coincidencias VALUES (2708,'Pam',336);

            INSERT INTO coincidencias VALUES (2709,'Pool',337);
            INSERT INTO coincidencias VALUES (2710,'Cool',337);
            INSERT INTO coincidencias VALUES (2711,'Cole',337);
            INSERT INTO coincidencias VALUES (2712,'Call',337);
            INSERT INTO coincidencias VALUES (2713,'Paul',337);
            INSERT INTO coincidencias VALUES (2714,'Cream',337);
            INSERT INTO coincidencias VALUES (2715,'Queen',337);
            INSERT INTO coincidencias VALUES (2716,'Green',337);
            INSERT INTO coincidencias VALUES (2717,'please',337);

            INSERT INTO coincidencias VALUES (2718,'citi',338);
            INSERT INTO coincidencias VALUES (2719,'sitti',338);
            INSERT INTO coincidencias VALUES (2720,'siti',338);
            INSERT INTO coincidencias VALUES (2721,'cities',338);

            INSERT INTO coincidencias VALUES (2722,'plait',339);
            INSERT INTO coincidencias VALUES (2723,'play',339);
            INSERT INTO coincidencias VALUES (2724,'great',339);
            INSERT INTO coincidencias VALUES (2725,'late',339);
            INSERT INTO coincidencias VALUES (2726,'place',339);
            INSERT INTO coincidencias VALUES (2727,'played',339);
            INSERT INTO coincidencias VALUES (2728,'please',339);

            INSERT INTO coincidencias VALUES (2729,'trane',340);
            INSERT INTO coincidencias VALUES (2730,'trained',340);
            INSERT INTO coincidencias VALUES (2731,'drain',340);
            INSERT INTO coincidencias VALUES (2732,'terrain',340);

            INSERT INTO coincidencias VALUES (2733,'parquet',341);
            INSERT INTO coincidencias VALUES (2734,'parc',341);
            INSERT INTO coincidencias VALUES (2735,'mark',341);
            INSERT INTO coincidencias VALUES (2736,'pork',341);

            INSERT INTO coincidencias VALUES (2737,'shoes',342);
            INSERT INTO coincidencias VALUES (2738,'shoo',342);
            INSERT INTO coincidencias VALUES (2739,'chew',342);
            INSERT INTO coincidencias VALUES (2740,'sure',342);

            INSERT INTO coincidencias VALUES (2741,'pause',343);
            INSERT INTO coincidencias VALUES (2742,'boss',343);
            INSERT INTO coincidencias VALUES (2743,'pual',343);
            INSERT INTO coincidencias VALUES (2744,'call',343);

            INSERT INTO coincidencias VALUES (2745,'twitter',344);
            INSERT INTO coincidencias VALUES (2746,'flutter',344);
            INSERT INTO coincidencias VALUES (2747,'shredder',344);
            INSERT INTO coincidencias VALUES (2748,'slitter',344);

            INSERT INTO coincidencias VALUES (2749,'plain',345);
            INSERT INTO coincidencias VALUES (2750,'playing',345);
            INSERT INTO coincidencias VALUES (2751,'pain',345);
            INSERT INTO coincidencias VALUES (2752,'play',345);

            INSERT INTO coincidencias VALUES (2753,'Phone',346);
            INSERT INTO coincidencias VALUES (2754,'Stone',346);
            INSERT INTO coincidencias VALUES (2755,'Song',346);
            INSERT INTO coincidencias VALUES (2756,'Bose',346);
            INSERT INTO coincidencias VALUES (2757,'Boat',346);
            INSERT INTO coincidencias VALUES (2758,'bows',346);

            INSERT INTO coincidencias VALUES (2759,'Mom',347);
            INSERT INTO coincidencias VALUES (2760,'Mama',347);
            INSERT INTO coincidencias VALUES (2761,'Mommy',347);
            INSERT INTO coincidencias VALUES (2762,'Bonnie',347);
            INSERT INTO coincidencias VALUES (2763,'Many',347);
            INSERT INTO coincidencias VALUES (2764,'rodney',347);

            INSERT INTO coincidencias VALUES (2765,'Hot',348);
            INSERT INTO coincidencias VALUES (2766,'Hug',348);
            INSERT INTO coincidencias VALUES (2767,'Fog',348);
            INSERT INTO coincidencias VALUES (2768,'Call',348);
            INSERT INTO coincidencias VALUES (2769,'Hulk ',348);
            INSERT INTO coincidencias VALUES (2770,'bug',348);

            INSERT INTO coincidencias VALUES (2771,'Red moon',349);
            INSERT INTO coincidencias VALUES (2772,'Lagoon',349);
            INSERT INTO coincidencias VALUES (2773,'Raccoon',349);
            INSERT INTO coincidencias VALUES (2774,'McCune',349);

            INSERT INTO coincidencias VALUES (2775,'Weather',350);
            INSERT INTO coincidencias VALUES (2776,'Windsor',350);
            INSERT INTO coincidencias VALUES (2777,'Window',350);
            INSERT INTO coincidencias VALUES (2778,'Lizards',350);
            INSERT INTO coincidencias VALUES (2779,'Lizard',350);
            INSERT INTO coincidencias VALUES (2780,'Wizard’s',350);

            INSERT INTO coincidencias VALUES (2781,'Soon',351);
            INSERT INTO coincidencias VALUES (2782,'Noon',351);
            INSERT INTO coincidencias VALUES (2783,'Phone',351);
            INSERT INTO coincidencias VALUES (2784,'Mood',351);
            INSERT INTO coincidencias VALUES (2785,'Food',351);
            INSERT INTO coincidencias VALUES (2786,'Boone ',351);
            INSERT INTO coincidencias VALUES (2787,'loon',351);

            INSERT INTO coincidencias VALUES (2788,'ki',352);
            INSERT INTO coincidencias VALUES (2789,'kee',352);
            INSERT INTO coincidencias VALUES (2790,'tea',352);
            INSERT INTO coincidencias VALUES (2791,'keith',352);
            INSERT INTO coincidencias VALUES (2792,'k',352);
            INSERT INTO coincidencias VALUES (2793,'que',352);
            INSERT INTO coincidencias VALUES (2794,'okay',352);

            INSERT INTO coincidencias VALUES (2795,'Wal',353);
            INSERT INTO coincidencias VALUES (2796,'Wow',353);
            INSERT INTO coincidencias VALUES (2797,'Law',353);
            INSERT INTO coincidencias VALUES (2798,'Wahl',353);
            INSERT INTO coincidencias VALUES (2799,'well',353);

            INSERT INTO coincidencias VALUES (2800,'t',354);
            INSERT INTO coincidencias VALUES (2801,'tee',354);
            INSERT INTO coincidencias VALUES (2802,'teeth',354);
            INSERT INTO coincidencias VALUES (2803,'team',354);
            INSERT INTO coincidencias VALUES (2804,'te',354);
            INSERT INTO coincidencias VALUES (2805,'key',354);

            INSERT INTO coincidencias VALUES (2806,'Map',355);
            INSERT INTO coincidencias VALUES (2807,'Maps',355);
            INSERT INTO coincidencias VALUES (2808,'Math',355);
            INSERT INTO coincidencias VALUES (2809,'Mass',355);
            INSERT INTO coincidencias VALUES (2810,'matt',355);

            INSERT INTO coincidencias VALUES (2811,'c',356);
            INSERT INTO coincidencias VALUES (2812,'see',356);
            INSERT INTO coincidencias VALUES (2813,'si',356);
            INSERT INTO coincidencias VALUES (2814,'CA',356);

            INSERT INTO coincidencias VALUES (2815,'Lok',357);
            INSERT INTO coincidencias VALUES (2816,'Luke',357);
            INSERT INTO coincidencias VALUES (2817,'Play',357);
            INSERT INTO coincidencias VALUES (2818,'Blake',357);
            INSERT INTO coincidencias VALUES (2819,'La',357);
            INSERT INTO coincidencias VALUES (2820,'The lake',357);

            INSERT INTO coincidencias VALUES (2821,'me',358);
            INSERT INTO coincidencias VALUES (2822,'mee',358);
            INSERT INTO coincidencias VALUES (2823,'mii',358);
            INSERT INTO coincidencias VALUES (2824,'nee',358);
            INSERT INTO coincidencias VALUES (2825,'new',358);
            INSERT INTO coincidencias VALUES (2826,'hats',359);
            INSERT INTO coincidencias VALUES (2827,'hack',359);
            INSERT INTO coincidencias VALUES (2828,'hatch',359);
            INSERT INTO coincidencias VALUES (2829,'pat',359);

            INSERT INTO coincidencias VALUES (2830,'Lyon',360);
            INSERT INTO coincidencias VALUES (2831,'Lowell',360);
            INSERT INTO coincidencias VALUES (2832, 'Elan',360);
            INSERT INTO coincidencias VALUES (2833,'The lion',360);
            INSERT INTO coincidencias VALUES (2834,'Flying',360);
            INSERT INTO coincidencias VALUES (2835,'ryan',360);

            INSERT INTO coincidencias VALUES (2836,'Yo',361);
            INSERT INTO coincidencias VALUES (2837,'Al',361);
            INSERT INTO coincidencias VALUES (2838,'You',361);
            INSERT INTO coincidencias VALUES (2839,'Ear',361);
            INSERT INTO coincidencias VALUES (2840,'Heel',361);
            INSERT INTO coincidencias VALUES (2841,'yeah',361);


            INSERT INTO coincidencias VALUES (2842,'did',362);
            INSERT INTO coincidencias VALUES (2843,'dish',362);
            INSERT INTO coincidencias VALUES (2844,'ditch',362);
            INSERT INTO coincidencias VALUES (2845,'dick',362);
            INSERT INTO coincidencias VALUES (2846,'this',362);

            INSERT INTO coincidencias VALUES (2847,'Mom',363);
            INSERT INTO coincidencias VALUES (2848,'Mall',363);
            INSERT INTO coincidencias VALUES (2849,'Mam',363);
            INSERT INTO coincidencias VALUES (2850,'More',363);
            INSERT INTO coincidencias VALUES (2851,'Small',363);
            INSERT INTO coincidencias VALUES (2852,'Mad',363);
            INSERT INTO coincidencias VALUES (2853,'Mass',363);
            INSERT INTO coincidencias VALUES (2854,'men',363);

            INSERT INTO coincidencias VALUES (2855,'Yeah',364);
            INSERT INTO coincidencias VALUES (2856,'Yes',364);
            INSERT INTO coincidencias VALUES (2857,'Ya',364);
            INSERT INTO coincidencias VALUES (2858,'Yea',364);
            INSERT INTO coincidencias VALUES (2859,'Yet',364);
            INSERT INTO coincidencias VALUES (2860,'Beer',364);
            INSERT INTO coincidencias VALUES (2861,'Here',364);
            INSERT INTO coincidencias VALUES (2862,'Air',364);
            INSERT INTO coincidencias VALUES (2863,'dear',364);

            INSERT INTO coincidencias VALUES (2864,'heart',365);
            INSERT INTO coincidencias VALUES (2865,'arc',365);
            INSERT INTO coincidencias VALUES (2866,'ark',365);
            INSERT INTO coincidencias VALUES (2867,'arch',365);
            INSERT INTO coincidencias VALUES (2868,'AR',365);
            INSERT INTO coincidencias VALUES (2869,'R',365);
            INSERT INTO coincidencias VALUES (2870,'Are',365);
            INSERT INTO coincidencias VALUES (2871,'car',365);

            INSERT INTO coincidencias VALUES (2872,'chip',366);
            INSERT INTO coincidencias VALUES (2873,'shipp',366);
            INSERT INTO coincidencias VALUES (2874,'shape',366);
            INSERT INTO coincidencias VALUES (2875,'sheep',366);
            INSERT INTO coincidencias VALUES (2876,'kik',366);
            INSERT INTO coincidencias VALUES (2877,'chick',366);
            INSERT INTO coincidencias VALUES (2878,'chip',366);

            INSERT INTO coincidencias VALUES (2879,'Home',367);
            INSERT INTO coincidencias VALUES (2880,'Horn',367);
            INSERT INTO coincidencias VALUES (2881,'Poem',367);
            INSERT INTO coincidencias VALUES (2882,'Elan',367);
            INSERT INTO coincidencias VALUES (2883,'Hands',367);
            INSERT INTO coincidencias VALUES (2884,'Camp',367);
            INSERT INTO coincidencias VALUES (2885,'Hamp',367);
            INSERT INTO coincidencias VALUES (2886,'Ham',367);
            INSERT INTO coincidencias VALUES (2887,'Hande',367);
            INSERT INTO coincidencias VALUES (2888,'Honda',367);
            INSERT INTO coincidencias VALUES (2889,'hunt',367);

            INSERT INTO coincidencias VALUES (2890,'Aloe',368);
            INSERT INTO coincidencias VALUES (2891,'All',368);
            INSERT INTO coincidencias VALUES (2892,'Hour',368);
            INSERT INTO coincidencias VALUES (2893,'Al',368);
            INSERT INTO coincidencias VALUES (2894,'I will',368);
            INSERT INTO coincidencias VALUES (2895,'Al',368);
            INSERT INTO coincidencias VALUES (2896,'Oh I will',368);

            INSERT INTO coincidencias VALUES (2897,'Hot',369);
            INSERT INTO coincidencias VALUES (2898,'Hunt',369);
            INSERT INTO coincidencias VALUES (2899,'What',369);
            INSERT INTO coincidencias VALUES (2900,'Hart',369);
            INSERT INTO coincidencias VALUES (2901,'Hard',369);
            INSERT INTO coincidencias VALUES (2902,'Part',369);
            INSERT INTO coincidencias VALUES (2903,'Hearts',369);
            INSERT INTO coincidencias VALUES (2904,'Harp',369);

            INSERT INTO coincidencias VALUES (2905,'Home',370);
            INSERT INTO coincidencias VALUES (2906,'Hell',370);
            INSERT INTO coincidencias VALUES (2907,'hello',370);
            INSERT INTO coincidencias VALUES (2908,'Call',370);
            INSERT INTO coincidencias VALUES (2909,'Tow',370);
            INSERT INTO coincidencias VALUES (2910,'Toes',370);
            INSERT INTO coincidencias VALUES (2911,'Tongue',370);
            INSERT INTO coincidencias VALUES (2912,'Toy',370);

            INSERT INTO coincidencias VALUES (2913,'Mall',371);
            INSERT INTO coincidencias VALUES (2914,'Call',371);
            INSERT INTO coincidencias VALUES (2915,'More',371);
            INSERT INTO coincidencias VALUES (2916,'Small',371);
            INSERT INTO coincidencias VALUES (2917,'Mayo',371);
            INSERT INTO coincidencias VALUES (2918,'Male',371);
            INSERT INTO coincidencias VALUES (2919,'Nail',371);
            INSERT INTO coincidencias VALUES (2920,'Mayor',371);

            INSERT INTO coincidencias VALUES (2921,'me',372);
            INSERT INTO coincidencias VALUES (2922,'mee',372);
            INSERT INTO coincidencias VALUES (2923,'mi',372);
            INSERT INTO coincidencias VALUES (2924,'new',372);
            INSERT INTO coincidencias VALUES (2925,'nice',372);

            INSERT INTO coincidencias VALUES (2926,'Home',373);
            INSERT INTO coincidencias VALUES (2927,'Call',373);
            INSERT INTO coincidencias VALUES (2928,'Hulk',373);
            INSERT INTO coincidencias VALUES (2929,'Whole',373);
            INSERT INTO coincidencias VALUES (2930,'Hall',373);
            INSERT INTO coincidencias VALUES (2931,'Call',373);

            INSERT INTO coincidencias VALUES (2932,'Go',374);
            INSERT INTO coincidencias VALUES (2933,'So',374);
            INSERT INTO coincidencias VALUES (2934,'The',374);
            INSERT INTO coincidencias VALUES (2935,'Cell',374);
            INSERT INTO coincidencias VALUES (2936,'No',374);
            INSERT INTO coincidencias VALUES (2937,'Doe',374);
            INSERT INTO coincidencias VALUES (2938,'No',374);
            INSERT INTO coincidencias VALUES (2939,'The',374);
            INSERT INTO coincidencias VALUES (2940,'though',374);

            INSERT INTO coincidencias VALUES (2941,'Hula',375);
            INSERT INTO coincidencias VALUES (2942,'Coller',375);
            INSERT INTO coincidencias VALUES (2943,'Lula',375);
            INSERT INTO coincidencias VALUES (2944,'Hello',375);
            INSERT INTO coincidencias VALUES (2945,'Tuner',375);
            INSERT INTO coincidencias VALUES (2946,'Luna',375);
            INSERT INTO coincidencias VALUES (2947,'Zona',375);
            INSERT INTO coincidencias VALUES (2948,'To know',375);

            INSERT INTO coincidencias VALUES (2949,'Sola',376);
            INSERT INTO coincidencias VALUES (2950,'Solo',376);
            INSERT INTO coincidencias VALUES (2951,'Chore',376);
            INSERT INTO coincidencias VALUES (2952,'Shor',376);
            INSERT INTO coincidencias VALUES (2953,'Shower',376);
            INSERT INTO coincidencias VALUES (2954,'Sore',376);

            INSERT INTO coincidencias VALUES (2955,'Mol',377);
            INSERT INTO coincidencias VALUES (2956,'Bowl',377);
            INSERT INTO coincidencias VALUES (2957,'Mall',377);
            INSERT INTO coincidencias VALUES (2958,'Call',377);
            INSERT INTO coincidencias VALUES (2959,'Mold',377);
            INSERT INTO coincidencias VALUES (2960,'Mol',377);
            INSERT INTO coincidencias VALUES (2961,'More',377);

            INSERT INTO coincidencias VALUES (2962,'Call ',378);
            INSERT INTO coincidencias VALUES (2963,'paul',378);
            INSERT INTO coincidencias VALUES (2964,'Hall',378);
            INSERT INTO coincidencias VALUES (2965,'Home',378);
            INSERT INTO coincidencias VALUES (2966,'Hello',378);
            INSERT INTO coincidencias VALUES (2967,'Tails',378);
            INSERT INTO coincidencias VALUES (2968,'Tale',378);
            INSERT INTO coincidencias VALUES (2969,'Tales',378);
            INSERT INTO coincidencias VALUES (2970,'kale',378);

            INSERT INTO coincidencias VALUES (2971,'Wow',379);
            INSERT INTO coincidencias VALUES (2972,'While',379);
            INSERT INTO coincidencias VALUES (2973,'Will',379);
            INSERT INTO coincidencias VALUES (2974,'Wel',379);
            INSERT INTO coincidencias VALUES (2975,'Whale',379);
            INSERT INTO coincidencias VALUES (2976,'Wale',379);
            INSERT INTO coincidencias VALUES (2977,'Will',379);

            INSERT INTO coincidencias VALUES (2978,'Choir',380);
            INSERT INTO coincidencias VALUES (2979,'Quiet',380);
            INSERT INTO coincidencias VALUES (2980,'Liar',380);
            INSERT INTO coincidencias VALUES (2981,'While',380);
            INSERT INTO coincidencias VALUES (2982,'Liar',380);
            INSERT INTO coincidencias VALUES (2983,'Meijer',380);
            INSERT INTO coincidencias VALUES (2984,'Fire',380);
            INSERT INTO coincidencias VALUES (2985,'Why are',380);

            INSERT INTO coincidencias VALUES (2986,'Mountain',381);
            INSERT INTO coincidencias VALUES (2987,'McKee',381);
            INSERT INTO coincidencias VALUES (2988,'Monkee',381);
            INSERT INTO coincidencias VALUES (2989,'Monkeys',381);
            INSERT INTO coincidencias VALUES (2990,'Munki',381);
            INSERT INTO coincidencias VALUES (2991,'Bunky',381);

            INSERT INTO coincidencias VALUES (2992,'AOL',382);
            INSERT INTO coincidencias VALUES (2993,'LOL',382);
            INSERT INTO coincidencias VALUES (2994,'Al',382);
            INSERT INTO coincidencias VALUES (2995,'Ayo',382);
            INSERT INTO coincidencias VALUES (2996,'AO',382);

            INSERT INTO coincidencias VALUES (2997,'Home',383);
            INSERT INTO coincidencias VALUES (2998,'Hope',383);
            INSERT INTO coincidencias VALUES (2999,'Call',383);
            INSERT INTO coincidencias VALUES (3000,'Whole',383);
            INSERT INTO coincidencias VALUES (3001,'Hall',383);

            INSERT INTO coincidencias VALUES (3002,'Close',384);
            INSERT INTO coincidencias VALUES (3003,'Clothes',384);
            INSERT INTO coincidencias VALUES (3004,'Float',384);
            INSERT INTO coincidencias VALUES (3005,'Club',384);
            INSERT INTO coincidencias VALUES (3006,'Rode',384);
            INSERT INTO coincidencias VALUES (3007,'Rhode',384);
            INSERT INTO coincidencias VALUES (3008,'Rogue',384);
            INSERT INTO coincidencias VALUES (3009,'Load',384);

            INSERT INTO coincidencias VALUES (3010,'inc',385);
            INSERT INTO coincidencias VALUES (3011,'inked',385);
            INSERT INTO coincidencias VALUES (3012,'pink',385);
            INSERT INTO coincidencias VALUES (3013,'think',385);
            INSERT INTO coincidencias VALUES (3014,'in',385);
            INSERT INTO coincidencias VALUES (3015,'game',385);
            INSERT INTO coincidencias VALUES (3016,'wink',385);

            INSERT INTO coincidencias VALUES (3017,'Wow',386);
            INSERT INTO coincidencias VALUES (3018,'While',386);
            INSERT INTO coincidencias VALUES (3019,'Will',386);
            INSERT INTO coincidencias VALUES (3020,'LOL',386);
            INSERT INTO coincidencias VALUES (3021,'Whale',386);
            INSERT INTO coincidencias VALUES (3022,'Wale',386);
            INSERT INTO coincidencias VALUES (3023,'Wel',386);
            INSERT INTO coincidencias VALUES (3024,'Will',386);

            INSERT INTO coincidencias VALUES (3025,'Home',387);
            INSERT INTO coincidencias VALUES (3026,'Phone',387);
            INSERT INTO coincidencias VALUES (3027,'Call',387);
            INSERT INTO coincidencias VALUES (3028,'Mall',387);
            INSERT INTO coincidencias VALUES (3029,'more',387);
            INSERT INTO coincidencias VALUES (3030,'small',387);

            INSERT INTO coincidencias VALUES (3031,'Home',388);
            INSERT INTO coincidencias VALUES (3032,'Hall',388);
            INSERT INTO coincidencias VALUES (3033,'Call',388);
            INSERT INTO coincidencias VALUES (3034,'Hole',388);
            INSERT INTO coincidencias VALUES (3035,'Penn',388);
            INSERT INTO coincidencias VALUES (3036,'10',388);
            INSERT INTO coincidencias VALUES (3037,'pet',388);
            INSERT INTO coincidencias VALUES (3038,'penny',388);

            INSERT INTO coincidencias VALUES (3039,'Brown',389);
            INSERT INTO coincidencias VALUES (3040,'Well',389);
            INSERT INTO coincidencias VALUES (3041,'Round',389);
            INSERT INTO coincidencias VALUES (3042,'Wow',389);
            INSERT INTO coincidencias VALUES (3043,'Clown',389);
            INSERT INTO coincidencias VALUES (3044,'Crown',389);
            INSERT INTO coincidencias VALUES (3045,'Proud',389);
            INSERT INTO coincidencias VALUES (3046,'frowned',389);

            INSERT INTO coincidencias VALUES (3047,'Whell',390);
            INSERT INTO coincidencias VALUES (3048,'Real',390);
            INSERT INTO coincidencias VALUES (3049,'Leo',390);
            INSERT INTO coincidencias VALUES (3050,'Well',390);
            INSERT INTO coincidencias VALUES (3051,'Wheelz',390);
            INSERT INTO coincidencias VALUES (3052,'Wales',390);
            INSERT INTO coincidencias VALUES (3053,'Wills',390);
            INSERT INTO coincidencias VALUES (3054,'Whales',390);

            INSERT INTO coincidencias VALUES (3055,'Al',391);
            INSERT INTO coincidencias VALUES (3056,'Howell',391);
            INSERT INTO coincidencias VALUES (3057,'All',391);
            INSERT INTO coincidencias VALUES (3058,'Powell',391);
            INSERT INTO coincidencias VALUES (3059,'Towel',391);
            INSERT INTO coincidencias VALUES (3060,'Power',391);

            INSERT INTO coincidencias VALUES (3061,'Home',392);
            INSERT INTO coincidencias VALUES (3062,'Hall',392);
            INSERT INTO coincidencias VALUES (3063,'Haul',392);
            INSERT INTO coincidencias VALUES (3064,'Call',392);
            INSERT INTO coincidencias VALUES (3065,'Paul',392);
            INSERT INTO coincidencias VALUES (3066,'Head',392);
            INSERT INTO coincidencias VALUES (3067,'Penn',392);
            INSERT INTO coincidencias VALUES (3068,'Hand',392);

            INSERT INTO coincidencias VALUES (3069,'Stock',393);
            INSERT INTO coincidencias VALUES (3070,'Shop',393);
            INSERT INTO coincidencias VALUES (3071,'Start',393);
            INSERT INTO coincidencias VALUES (3072,'Starr',393);
            INSERT INTO coincidencias VALUES (3073,'Staarr',393);
            INSERT INTO coincidencias VALUES (3074,'Store',393);

            INSERT INTO coincidencias VALUES (3075,'Mad ',394);
            INSERT INTO coincidencias VALUES (3076,'mud ',394);
            INSERT INTO coincidencias VALUES (3077,'man',394);
            INSERT INTO coincidencias VALUES (3078,'Bad',394);
            INSERT INTO coincidencias VALUES (3079,'Bug',394);
            INSERT INTO coincidencias VALUES (3080,'Pug',394);
            INSERT INTO coincidencias VALUES (3081,'Luck',394);
            INSERT INTO coincidencias VALUES (3082,'Map',394);

            INSERT INTO coincidencias VALUES (3083,'Sound',395);
            INSERT INTO coincidencias VALUES (3084,'Song',395);
            INSERT INTO coincidencias VALUES (3085,'Town',395);
            INSERT INTO coincidencias VALUES (3086,'Phone',395);
            INSERT INTO coincidencias VALUES (3087,'Down',395);
            INSERT INTO coincidencias VALUES (3088,'Gallon',395);
            INSERT INTO coincidencias VALUES (3089,'Gout',395);
            INSERT INTO coincidencias VALUES (3090,'Gallery',395);
            INSERT INTO coincidencias VALUES (3091,'Doubt',395);

            INSERT INTO coincidencias VALUES (3092,'Yes',396);
            INSERT INTO coincidencias VALUES (3093,'yea',396);
            INSERT INTO coincidencias VALUES (3094,'Yeah',396);
            INSERT INTO coincidencias VALUES (3095,'Yet',396);
            INSERT INTO coincidencias VALUES (3096,'Year',396);
            INSERT INTO coincidencias VALUES (3097,'Here',396);
            INSERT INTO coincidencias VALUES (3098,'Beer',396);
            INSERT INTO coincidencias VALUES (3099,'Dear',396);

            INSERT INTO coincidencias VALUES (3100,'Lowe’s',397);
            INSERT INTO coincidencias VALUES (3101,'Load',397);
            INSERT INTO coincidencias VALUES (3102,'Well',397);
            INSERT INTO coincidencias VALUES (3103,'Rode',397);
            INSERT INTO coincidencias VALUES (3104,'Rhode',397);
            INSERT INTO coincidencias VALUES (3105,'The rode',397);
            INSERT INTO coincidencias VALUES (3106,'Load',397);

            INSERT INTO coincidencias VALUES (3107,'Wow',398);
            INSERT INTO coincidencias VALUES (3108,'Well',398);
            INSERT INTO coincidencias VALUES (3109,'Clown',398);
            INSERT INTO coincidencias VALUES (3110,'While',398);
            INSERT INTO coincidencias VALUES (3111,'Brown',398);
            INSERT INTO coincidencias VALUES (3112,'Crown',398);
            INSERT INTO coincidencias VALUES (3113,'Proud',398);
            INSERT INTO coincidencias VALUES (3114,'Braun',398);

            INSERT INTO coincidencias VALUES (3115,'ro',399);
            INSERT INTO coincidencias VALUES (3116,'roe',399);
            INSERT INTO coincidencias VALUES (3117,'rowe',399);
            INSERT INTO coincidencias VALUES (3118,'raw',399);
            INSERT INTO coincidencias VALUES (3119,'rose',399);
            INSERT INTO coincidencias VALUES (3120,'bro',399);

            INSERT INTO coincidencias VALUES (3121,'Lowes',400);
            INSERT INTO coincidencias VALUES (3122,'Low',400);
            INSERT INTO coincidencias VALUES (3123,'Love',400);
            INSERT INTO coincidencias VALUES (3124,'Ros',400);
            INSERT INTO coincidencias VALUES (3125,'Rows',400);
            INSERT INTO coincidencias VALUES (3126,'rosa',400);

            INSERT INTO coincidencias VALUES (3127,'Epic',401);
            INSERT INTO coincidencias VALUES (3128,'Pizza',401);
            INSERT INTO coincidencias VALUES (3129,'Open',401);
            INSERT INTO coincidencias VALUES (3130,'Brush',401);
            INSERT INTO coincidencias VALUES (3131,'The brush',401);
            INSERT INTO coincidencias VALUES (3132,'What brush',401);
            INSERT INTO coincidencias VALUES (3133,'Who’s brush',401);

            INSERT INTO coincidencias VALUES (3134,'Course',402);
            INSERT INTO coincidencias VALUES (3135,'Call',402);
            INSERT INTO coincidencias VALUES (3136,'Cool',402);
            INSERT INTO coincidencias VALUES (3137,'Whores',402);
            INSERT INTO coincidencias VALUES (3138,'Hoarse',402);
            INSERT INTO coincidencias VALUES (3139,'Course',402);
            INSERT INTO coincidencias VALUES (3140,'Horus',402);

            INSERT INTO coincidencias VALUES (3141,'Audio',403);
            INSERT INTO coincidencias VALUES (3142,'Fabio',403);
            INSERT INTO coincidencias VALUES (3143,'Pablo',403);
            INSERT INTO coincidencias VALUES (3144,'Perio',403);
            INSERT INTO coincidencias VALUES (3145,'Patios',403);
            INSERT INTO coincidencias VALUES (3146,'How do',403);
            INSERT INTO coincidencias VALUES (3147,'Mario',403);

            INSERT INTO coincidencias VALUES (3148,'Sale',404);
            INSERT INTO coincidencias VALUES (3149,'Hair',404);
            INSERT INTO coincidencias VALUES (3150,'Jail',404);
            INSERT INTO coincidencias VALUES (3151,'Yale',404);
            INSERT INTO coincidencias VALUES (3152,'Syria',404);
            INSERT INTO coincidencias VALUES (3153,'Hello',404);
            INSERT INTO coincidencias VALUES (3154,'Serial',404);
            INSERT INTO coincidencias VALUES (3155,'Syria',404);
            INSERT INTO coincidencias VALUES (3156,'Cereals',404);
            INSERT INTO coincidencias VALUES (3157,'serio',404);

            INSERT INTO coincidencias VALUES (3158,'Basset',405);
            INSERT INTO coincidencias VALUES (3159,'Docket',405);
            INSERT INTO coincidencias VALUES (3160,'Facet',405);
            INSERT INTO coincidencias VALUES (3161,'Stop it',405);
            INSERT INTO coincidencias VALUES (3162,'Gap kids',405);
            INSERT INTO coincidencias VALUES (3163,'Jackets',405);
            INSERT INTO coincidencias VALUES (3164,'Decades',405);
            INSERT INTO coincidencias VALUES (3165,'The kids',405);

            INSERT INTO coincidencias VALUES (3166,'Mom',406);
            INSERT INTO coincidencias VALUES (3167,'Mall',406);
            INSERT INTO coincidencias VALUES (3168,'Small',406);
            INSERT INTO coincidencias VALUES (3169,'Long',406);
            INSERT INTO coincidencias VALUES (3170,'More',406);
            INSERT INTO coincidencias VALUES (3171,'Men',406);
            INSERT INTO coincidencias VALUES (3172,'Mann',406);
            INSERT INTO coincidencias VALUES (3173,'Mane',406);
            INSERT INTO coincidencias VALUES (3174,'Mad',406);

            INSERT INTO coincidencias VALUES (3175,'Open',407);
            INSERT INTO coincidencias VALUES (3176,'Listen',407);
            INSERT INTO coincidencias VALUES (3177,'System',407);
            INSERT INTO coincidencias VALUES (3178,'Kitten',407);
            INSERT INTO coincidencias VALUES (3179,'Kitchn',407);
            INSERT INTO coincidencias VALUES (3180,'Pigeon',407);
            INSERT INTO coincidencias VALUES (3181,'Kitchener',407);

            INSERT INTO coincidencias VALUES (3182,'Silk',408);
            INSERT INTO coincidencias VALUES (3183,'Self',408);
            INSERT INTO coincidencias VALUES (3184,'Belk',408);
            INSERT INTO coincidencias VALUES (3185,'Melk',408);
            INSERT INTO coincidencias VALUES (3186,'Mielke',408);
            INSERT INTO coincidencias VALUES (3187,'Melc',408);

            INSERT INTO coincidencias VALUES (3188,'Set',409);
            INSERT INTO coincidencias VALUES (3189,'Search',409);
            INSERT INTO coincidencias VALUES (3190,'Sec',409);
            INSERT INTO coincidencias VALUES (3191,'Sex',409);
            INSERT INTO coincidencias VALUES (3192,'Shert',409);
            INSERT INTO coincidencias VALUES (3193,'Cert',409);
            INSERT INTO coincidencias VALUES (3194,'Shirts',409);

            INSERT INTO coincidencias VALUES (3195,'Now',410);
            INSERT INTO coincidencias VALUES (3196,'Mouth',410);
            INSERT INTO coincidencias VALUES (3197,'Map',410);
            INSERT INTO coincidencias VALUES (3198,'No',410);
            INSERT INTO coincidencias VALUES (3199,'Mass',410);
            INSERT INTO coincidencias VALUES (3200,'Maps ',410);

            INSERT INTO coincidencias VALUES (3201,'Boston',411);
            INSERT INTO coincidencias VALUES (3202,'Aspen',411);
            INSERT INTO coincidencias VALUES (3203,'Austin',411);
            INSERT INTO coincidencias VALUES (3204,'Bath room',411);
            INSERT INTO coincidencias VALUES (3205,'Bathrooms',411);

            INSERT INTO coincidencias VALUES (3206,'Home',412);
            INSERT INTO coincidencias VALUES (3207,'Hello',412);
            INSERT INTO coincidencias VALUES (3208,'Helm',412);
            INSERT INTO coincidencias VALUES (3209,'Poem',412);
            INSERT INTO coincidencias VALUES (3210,'Call',412);
            INSERT INTO coincidencias VALUES (3211,'Cole',412);
            INSERT INTO coincidencias VALUES (3212,'Cold',412);

            INSERT INTO coincidencias VALUES (3213,'Mall',413);
            INSERT INTO coincidencias VALUES (3214,'Mail',413);
            INSERT INTO coincidencias VALUES (3215,'More',413);
            INSERT INTO coincidencias VALUES (3216,'Male',413);
            INSERT INTO coincidencias VALUES (3217,'Snail',413);
            INSERT INTO coincidencias VALUES (3218,'Male',413);
            INSERT INTO coincidencias VALUES (3219,'Dale',413);

            INSERT INTO coincidencias VALUES (3220,'bass',414);
            INSERT INTO coincidencias VALUES (3221,'path',414);
            INSERT INTO coincidencias VALUES (3222,'math',414);
            INSERT INTO coincidencias VALUES (3223,'bad',414);

            INSERT INTO coincidencias VALUES (3224,'Glass',415);
            INSERT INTO coincidencias VALUES (3225,'Club',415);
            INSERT INTO coincidencias VALUES (3226,'Glad',415);
            INSERT INTO coincidencias VALUES (3227,'Play',415);
            INSERT INTO coincidencias VALUES (3228,'Globs',415);
            INSERT INTO coincidencias VALUES (3229,'Glove ',415);

            INSERT INTO coincidencias VALUES (3230,'Lotto',416);
            INSERT INTO coincidencias VALUES (3231,'Weather',416);
            INSERT INTO coincidencias VALUES (3232,'Daughter',416);
            INSERT INTO coincidencias VALUES (3233,'What',416);
            INSERT INTO coincidencias VALUES (3234,'Horror',416);
            INSERT INTO coincidencias VALUES (3235,'Warrior',416);
            INSERT INTO coincidencias VALUES (3236,'Laura ',416);

            INSERT INTO coincidencias VALUES (3237,'street',417);
            INSERT INTO coincidencias VALUES (3238,'sweet',417);
            INSERT INTO coincidencias VALUES (3239,'crete',417);
            INSERT INTO coincidencias VALUES (3240,'treats',417);

            INSERT INTO coincidencias VALUES (3241,'Hot',418);
            INSERT INTO coincidencias VALUES (3242,'fat',418);
            INSERT INTO coincidencias VALUES (3243,'App',418);
            INSERT INTO coincidencias VALUES (3244,'Hats',418);
            INSERT INTO coincidencias VALUES (3245,'Hatch',418);
            INSERT INTO coincidencias VALUES (3246,'Hack',418);

            INSERT INTO coincidencias VALUES (3247,'noah',419);
            INSERT INTO coincidencias VALUES (3248,'Now',419);
            INSERT INTO coincidencias VALUES (3249,'No',419);
            INSERT INTO coincidencias VALUES (3250,'Lowe’s',419);
            INSERT INTO coincidencias VALUES (3251,'Know’s',419);
            INSERT INTO coincidencias VALUES (3252,'Noh’s',419);
            INSERT INTO coincidencias VALUES (3253,'Noes',419);

            INSERT INTO coincidencias VALUES (3254,'Home',420);
            INSERT INTO coincidencias VALUES (3255,'Pump',420);
            INSERT INTO coincidencias VALUES (3256,'Hulk',420);
            INSERT INTO coincidencias VALUES (3257,'Punk',420);
            INSERT INTO coincidencias VALUES (3258,'Pans',420);
            INSERT INTO coincidencias VALUES (3259,'Pance',420);
            INSERT INTO coincidencias VALUES (3260,'Ponce',420);
            INSERT INTO coincidencias VALUES (3261,'Pens',420);

            INSERT INTO coincidencias VALUES (3262,'Life',421);
            INSERT INTO coincidencias VALUES (3263,'Like',421);
            INSERT INTO coincidencias VALUES (3264,'Right',421);
            INSERT INTO coincidencias VALUES (3265,'Light',421);
            INSERT INTO coincidencias VALUES (3266,'Rise',421);
            INSERT INTO coincidencias VALUES (3267,'Arise',421);
            INSERT INTO coincidencias VALUES (3268,'Fry’s',421);
            INSERT INTO coincidencias VALUES (3269,'Price',421);

            INSERT INTO coincidencias VALUES (3270,'Element',422);
            INSERT INTO coincidencias VALUES (3271,'Elephants',422);
            INSERT INTO coincidencias VALUES (3272,'Elephant',422);
            INSERT INTO coincidencias VALUES (3273,'Elefant',422);

            INSERT INTO coincidencias VALUES (3274,'Son',423);
            INSERT INTO coincidencias VALUES (3275,'Send',423);
            INSERT INTO coincidencias VALUES (3276,'Fun',423);
            INSERT INTO coincidencias VALUES (3277,'Sunn',423);
            INSERT INTO coincidencias VALUES (3278,'Sin',423);

            INSERT INTO coincidencias VALUES (3279,'Sod',424);
            INSERT INTO coincidencias VALUES (3280,'Bob',424);
            INSERT INTO coincidencias VALUES (3281,'Dog',424);
            INSERT INTO coincidencias VALUES (3282,'God',424);
            INSERT INTO coincidencias VALUES (3283,'Song',424);
            INSERT INTO coincidencias VALUES (3284,'Prague',424);
            INSERT INTO coincidencias VALUES (3285,'Prog',424);
            INSERT INTO coincidencias VALUES (3286,'Prag',424);
            INSERT INTO coincidencias VALUES (3287,'Frog',424);

            INSERT INTO coincidencias VALUES (3288,'zoup',425);
            INSERT INTO coincidencias VALUES (3289,'suit',425);
            INSERT INTO coincidencias VALUES (3290,'poop',425);
            INSERT INTO coincidencias VALUES (3291,'soap',425);
            INSERT INTO coincidencias VALUES (3292,'suits',425);
            INSERT INTO coincidencias VALUES (3293,'soon',425);

            INSERT INTO coincidencias VALUES (3294,'sandwhich',426);
            INSERT INTO coincidencias VALUES (3295,'sand-witch',426);
            INSERT INTO coincidencias VALUES (3296,'zenwich',426);
            INSERT INTO coincidencias VALUES (3297,'sand witch',426);

            INSERT INTO coincidencias VALUES (3298,'Father',427);
            INSERT INTO coincidencias VALUES (3299,'Spider',427);
            INSERT INTO coincidencias VALUES (3300,'Bother',427);
            INSERT INTO coincidencias VALUES (3301,'Tyler',427);
            INSERT INTO coincidencias VALUES (3302,'Meijer',427);
            INSERT INTO coincidencias VALUES (3303,'Fyre',427);
            INSERT INTO coincidencias VALUES (3304,'Buyer',427);
            INSERT INTO coincidencias VALUES (3305,'sire',427);

            INSERT INTO coincidencias VALUES (3306,'kandi',428);
            INSERT INTO coincidencias VALUES (3307,'candi',428);
            INSERT INTO coincidencias VALUES (3308,'kandy',428);
            INSERT INTO coincidencias VALUES (3309,'kandee',428);

            INSERT INTO coincidencias VALUES (3310,'koffee',429);
            INSERT INTO coincidencias VALUES (3311,'coffey',429);
            INSERT INTO coincidencias VALUES (3312,'koffi',429);
            INSERT INTO coincidencias VALUES (3313,'copy',429);

            INSERT INTO coincidencias VALUES (3314,'pastor',430);
            INSERT INTO coincidencias VALUES (3315,'pasco',430);
            INSERT INTO coincidencias VALUES (3316,'pasture',430);
            INSERT INTO coincidencias VALUES (3317,'Pasteur',430);

            INSERT INTO coincidencias VALUES (3318,'sota',431);
            INSERT INTO coincidencias VALUES (3319,'yoda',431);
            INSERT INTO coincidencias VALUES (3320,'soto',431);

            INSERT INTO coincidencias VALUES (3321,'taku',432);
            INSERT INTO coincidencias VALUES (3322,'tacos',432);
            INSERT INTO coincidencias VALUES (3323,'paco',432);
            INSERT INTO coincidencias VALUES (3324,'tackle',432);

            INSERT INTO coincidencias VALUES (3325,'chocolat',433);
            INSERT INTO coincidencias VALUES (3326,'chocolates',433);

            INSERT INTO coincidencias VALUES (3327,'big sur',434);
            INSERT INTO coincidencias VALUES (3328,'bieksa',434);
            INSERT INTO coincidencias VALUES (3329,'pixza',434);
            INSERT INTO coincidencias VALUES (3330,'picture',434);

            INSERT INTO coincidencias VALUES (3331,'skirts',435);
            INSERT INTO coincidencias VALUES (3332,'skurt',435);
            INSERT INTO coincidencias VALUES (3333,'spirits',435);
            INSERT INTO coincidencias VALUES (3334,'spirit',435);

      ]]
    return query
    end

return tabla_coincidencias

