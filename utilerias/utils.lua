local base = require("utilerias.base" )
local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local physics = require "physics"

local utils={}

local currentScene
local currentButton
local varError = 0
local imgClick = ""
local markX,markY=0,0
local isBegan =false

---------------------------------------------------------------------------------------
---------*********************SONIDOS INSTRUCCIONES******************-----------
-- Manda el audio de la imagen/seleccion
local function sonarPalabra(event)
    
    if ( event.completed ) then
        
        local sonido = audio.loadSound( "sounds/sounds/objetos/"..currentButton..".mp3")
        local audioChannel = audio.play( sonido, { channel=audioChannel, onComplete= currentScene.successful }  )
        if audioChannel > 0 then 
            audio.setVolume( 1, { channel=audioChannel } )
        end     
    end
end

function utils.primerSonido(nameScene,button)
    --poner codigo 
    
    composer.showOverlay( "bloqueo" ,{ isModal = true } )
    
    currentScene = require( nameScene )
    currentButton = button
    
    local sonido = audio.loadSound( "sounds/sounds/principales/pronounce.mp3")
    local audioChannel = audio.play( sonido, { channel=audioChannel , onComplete= sonarPalabra} )
    if audioChannel > 0 then 
        audio.setVolume( 1, { channel=audioChannel } )
    end
end

---------*********************FIN SONIDOS INSTRUCCIONES******************-----------

--Este metodo retorna la validacion de speech recognicer para saber si se pronuncio correctamente
-- Regresa un true o un false
function utils.validarVoz(nombreImagen)
    
    print("nombre imagen: "..tostring(nombreImagen))
    
   --[[local coincidencia = SpeechRecognized.LanzarSpeech()
    
    coincidencia = string.lower(coincidencia)
    nombreImagen = string.lower(nombreImagen)
    print("coincidencia: "..tostring(coincidencia)) 
    
    local boolPalabra = base.consultarExistencia(nombreImagen,coincidencia)]]--
    
     local boolPalabra = true      
    
    if boolPalabra or (imgClick~=nombreImagen) then
        varError = 0;
    elseif not(boolPalabra) then
        varError = varError+1;
        
        if varError > 1 then
            varError = 0
            boolPalabra = true
        end
    end
    
    imgClick = nombreImagen
    
    return boolPalabra
end

--Metodo para poner ids a las imagenes
function utils.renombrarImagenes(tipoJuego,object) --the market no tiene tipojuego, tampoco ruleta
    
    if tipoJuego == 1 then--la compra memorma set 1
        if object.myName==1 then 
            object.id= "pear"  
        elseif object.myName ==2 then
            object.id= "chips"
        elseif object.myName ==3 then
            object.id= "tomato"
        elseif object.myName ==4 then
            object.id= "taco"
        elseif object.myName ==5 then
            object.id= "steak"
        elseif object.myName ==6 then
            object.id= "cart"
        end
    elseif tipoJuego == 2 then--la compra memorma set 2
        if object.myName==1 then 
            object.id= "pepper"  
        elseif object.myName ==2 then
            object.id= "teapot"
        elseif object.myName ==3 then
            object.id= "butter"
        elseif object.myName ==4 then
            object.id= "pasta"
        elseif object.myName ==5 then
            object.id= "pickle"
        elseif object.myName ==6 then
            object.id= "napkin"
        end
    elseif tipoJuego == 3 then--la compra memorama set 3 
        if object.myName==1 then 
            object.id= "soup"   
        elseif object.myName ==2 then
            object.id= "cup"
        elseif object.myName ==3 then
            object.id= "meat"
        elseif object.myName ==4 then
            object.id= "pot"
        elseif object.myName ==5 then
            object.id= "fork"
        elseif object.myName ==6 then
            object.id= "cookbook"
        end
    elseif tipoJuego == 4 then-- el viaje memorama set 1
        if object.myName==1 then
            object.id= "sandals"
        elseif object.myName ==2 then
            object.id= "shampoo"
        elseif object.myName ==3 then
            object.id= "shirt"
        elseif object.myName ==4 then
            object.id= "sweater"
        elseif object.myName ==5 then
            object.id= "taxi"
        elseif object.myName ==6 then
            object.id= "towel"
        end
    elseif tipoJuego == 5 then--el viaje memorama set 2
        if object.myName==1 then 
            object.id= "brushes"
        elseif object.myName ==2 then
            object.id= "button"    
        elseif object.myName ==3 then 
            object.id= "glasses"     
        elseif object.myName ==4 then
            object.id= "hotel"      
        elseif object.myName ==5 then
            object.id= "tshirt"     
        elseif object.myName ==6 then
            object.id= "whistle"
        end
    elseif tipoJuego == 6 then--el viaje memorama set 3
        if object.myName==1 then 
            object.id= "belt"
        elseif object.myName ==2 then
            object.id= "boots"
        elseif object.myName ==3 then 
            object.id= "ship" 
        elseif object.myName ==4 then
            object.id= "socks"   
        elseif object.myName ==5 then
            object.id= "swimsuit"  
        elseif object.myName ==6 then 
            object.id= "toothbrush"
        end
    elseif tipoJuego == 7 then  --penny memorama set 1
        if object.myName==1 then 
            object.id= "pail"    
        elseif object.myName ==2 then
            object.id= "baboon"
        elseif object.myName ==3 then
            object.id= "bear"
        elseif object.myName ==4 then
            object.id= "boa"
        elseif object.myName ==5 then
            object.id= "puma"
        elseif object.myName ==6 then
            object.id= "pea"
        end
    elseif tipoJuego == 8 then  --penny memorama set 2
        if object.myName==1 then 
            object.id= "hippo"    
        elseif object.myName ==2 then
            object.id= "mailbox"
        elseif object.myName ==3 then
            object.id= "pumpkin"
        elseif object.myName ==4 then
            object.id= "robin"
        elseif object.myName ==5 then
            object.id= "shampoo"
        elseif object.myName ==6 then
            object.id= "strawberry"
        end
    elseif tipoJuego == 9 then  --penny memorama set 3
        if object.myName==1 then 
            object.id= "icecube"    
        elseif object.myName ==2 then
            object.id= "lightbulb"
        elseif object.myName ==3 then
            object.id= "lollipop"
        elseif object.myName ==4 then
            object.id= "robe"
        elseif object.myName ==5 then
            object.id= "rope"
        elseif object.myName ==6 then
            object.id= "soap"   
             end
    elseif tipoJuego == 10 then  --los sonidos memorama set 1
        if object.myName==1 then 
            object.id= "camp"    
        elseif object.myName ==2 then
            object.id= "cave"
        elseif object.myName ==3 then
            object.id= "park"
        elseif object.myName ==4 then
            object.id= "picnic"
        elseif object.myName ==5 then
            object.id= "turtle"
        elseif object.myName ==6 then
            object.id= "toad"   
        end
    elseif tipoJuego == 11 then  --los sonidos memorama set 2
        if object.myName==1 then 
            object.id= "backpack"    
        elseif object.myName ==2 then
            object.id= "cheetah"
        elseif object.myName ==3 then  
            object.id= "chicken"
        elseif object.myName ==4 then 
            object.id= "hippo"
        elseif object.myName ==5 then 
            object.id= "mountain"
        elseif object.myName ==6 then
            object.id= "octopus"
        end
    elseif tipoJuego == 12 then -- los sonidos memorama set 3
        if object.myName==1 then 
            object.id= "map"    
        elseif object.myName ==2 then  
            object.id= "flipflop"
        elseif object.myName ==3 then  
            object.id= "tent"
        elseif object.myName ==4 then
            object.id= "bat"
        elseif object.myName ==5 then
            object.id= "firework"
        elseif object.myName ==6 then
            object.id= "duck"
        end
    elseif tipoJuego == 13 then  -- poppy memorama set 1
        if object.myName == 1 then 
            object.id= "ball"    
        elseif object.myName ==2 then
            object.id= "blanket"
        elseif object.myName ==3 then
            object.id= "pig"
        elseif object.myName ==4 then
            object.id= "puppy"
        elseif object.myName ==5 then
            object.id= "tiger"
        elseif object.myName ==6 then
            object.id= "toad"
        end
        
    elseif tipoJuego == 14 then-- poppy memorama set 2
        if object.myName==1 then 
            object.id= "butterfly"    
        elseif object.myName ==2 then
            object.id= "frisbee"
        elseif object.myName ==3 then
            object.id= "grasshopper"
        elseif object.myName ==4 then
            object.id= "kibble"
        elseif object.myName ==5 then
            object.id= "teepee"
        elseif object.myName ==6 then
            object.id= "water"
            
        end
    elseif tipoJuego == 15 then  --poppy memorama set 3
        if object.myName==1 then 
            object.id= "basket"    
        elseif object.myName ==2 then
            object.id= "cat"
        elseif object.myName ==3 then
            object.id= "robe"
        elseif object.myName ==4 then
            object.id= "rope"             
        elseif object.myName ==5 then
            object.id= "scrub"
        elseif object.myName ==6 then
            object.id= "sheep"
        end
    elseif tipoJuego == 16 then  --danny memorama set 1 
        if object.myName==1 then 
            object.id= "doll"    
        elseif object.myName ==2 then
            object.id= "door"
        elseif object.myName ==3 then
            object.id= "down"
        elseif object.myName ==4 then
            object.id= "tea" 
        elseif object.myName ==5 then
            object.id= "tomato" 
        elseif object.myName ==6 then
            object.id= "towel" 
        end
    elseif tipoJuego == 17 then  --danny memorama set 2
        if object.myName==1 then 
            object.id= "butter"    
        elseif object.myName ==2 then
            object.id= "computer"
        elseif object.myName ==3 then
            object.id= "hotel"
        elseif object.myName ==4 then
            object.id= "soda"
        elseif object.myName ==5 then
            object.id= "tornado" 
        elseif object.myName ==6 then
            object.id= "window" 
        end
    elseif tipoJuego == 18 then  --danny memorama set 3
        if object.myName==1 then 
            object.id= "bat"    
        elseif object.myName ==2 then
            object.id= "bread"
        elseif object.myName ==3 then
            object.id= "hand"
        elseif object.myName ==4 then
            object.id= "heart"
        elseif object.myName ==5 then
            object.id= "hot" 
        elseif object.myName ==6 then
            object.id= "toad" 
        end
    elseif tipoJuego == 19 then  -- cally memorama set 1
        if object.myName==1 then 
            object.id= "call"    
        elseif object.myName ==2 then
            object.id= "coin"
        elseif object.myName ==3 then
            object.id= "gear"
        elseif object.myName ==4 then
            object.id= "glue"             
        elseif object.myName ==5 then
            object.id= "gum"
        elseif object.myName ==6 then
            object.id= "kiwi" 
        end
    elseif tipoJuego == 20 then  --cally memorama set 2
        if object.myName==1 then 
            object.id= "bicycle"    
        elseif object.myName ==2 then
            object.id= "cougar"
        elseif object.myName ==3 then
            object.id= "magnet"
        elseif object.myName ==4 then
            object.id= "necklace"             
        elseif object.myName ==5 then
            object.id= "pumpkin"             
        elseif object.myName ==6 then
            object.id= "sunglasses"
        end
        elseif tipoJuego == 21 then  --cally memorama set 3
        if object.myName==1 then 
            object.id= "book"    
        elseif object.myName ==2 then
            object.id= "egg"
        elseif object.myName ==3 then
            object.id= "flag"
        elseif object.myName ==4 then
            object.id= "milk"             
        elseif object.myName ==5 then
            object.id= "mug"             
        elseif object.myName ==6 then
            object.id= "stick"
        end
        elseif tipoJuego == 22 then  --zara memorama set 1
        if object.myName==1 then 
            object.id= "scissors"    
        elseif object.myName ==2 then
            object.id= "sea"
        elseif object.myName ==3 then
            object.id= "sew"
        elseif object.myName ==4 then
            object.id= "zero"             
        elseif object.myName ==5 then
            object.id= "zip"             
        elseif object.myName ==6 then
            object.id= "zoo"
        end 
        elseif tipoJuego == 23 then  --zara memorama set 2
        if object.myName==1 then 
            object.id= "baseball"    
        elseif object.myName ==2 then
            object.id= "frisbee"
        elseif object.myName ==3 then
            object.id= "glasses"
        elseif object.myName ==4 then
            object.id= "lizard"             
        elseif object.myName ==5 then
            object.id= "pencil"             
        elseif object.myName ==6 then
            object.id= "wizard"
        end 
        elseif tipoJuego == 24 then  --zara memorama set 3
        if object.myName==1 then 
            object.id= "bus"    
        elseif object.myName ==2 then
            object.id= "coins"
        elseif object.myName ==3 then
            object.id= "gloves"
        elseif object.myName ==4 then
            object.id= "house"             
        elseif object.myName ==5 then
            object.id= "necklace"             
        elseif object.myName ==6 then
            object.id= "paws"
        end 
        elseif tipoJuego == 25 then  --la compra diferencias set 1
        if object.myName==1 then 
            object.id= "tomato"    
        elseif object.myName ==2 then
            object.id= "cake"
        elseif object.myName ==3 then
            object.id= "tea"
        elseif object.myName ==4 then
            object.id= "carrot"
        elseif object.myName ==5 then
            object.id= "chips"
        elseif object.myName ==6 then
            object.id= "pear"
         end 
        elseif tipoJuego == 26 then  --la compra diferencias set 2
        if object.myName==1 then 
            object.id= "apple"    
        elseif object.myName ==2 then
            object.id= "pasta"
        elseif object.myName ==3 then
            object.id= "water"
        elseif object.myName ==4 then
            object.id= "bacon" 
        elseif object.myName ==5 then
            object.id= "pickle" 
        elseif object.myName ==6 then
            object.id= "pumpkin" 
        end 
        elseif tipoJuego == 27 then  --la compra diferencias set 3
        if object.myName==1 then 
            object.id= "pot"    
        elseif object.myName ==2 then
            object.id= "fork"
        elseif object.myName ==3 then
            object.id= "grape"
        elseif object.myName ==4 then
            object.id= "carrot"
        elseif object.myName ==5 then
            object.id= "milk"
        elseif object.myName ==6 then
            object.id= "soup"
        end 
         elseif tipoJuego == 28 then  --el viaje diferencias set 1
        if object.myName==1 then 
            object.id= "sandals"    
        elseif object.myName ==2 then
            object.id= "sweater"
        elseif object.myName ==3 then
            object.id= "shampoo"
        elseif object.myName ==4 then
            object.id= "taxi"
        elseif object.myName ==5 then
            object.id= "towel"
        elseif object.myName ==6 then
            object.id= "ship"
        
        end 
         elseif tipoJuego == 29 then  --el viaje diferencias set 2
        if object.myName==1 then 
            object.id= "button"    
        elseif object.myName ==2 then
            object.id= "glasses"
        elseif object.myName ==3 then
            object.id= "hotel" 
        elseif object.myName ==4 then
            object.id= "t-shirt" 
        elseif object.myName ==5 then
            object.id= "brushes"
        elseif object.myName ==6 then
            object.id= "whistle"
        end 
         elseif tipoJuego == 30 then  --el viaje diferencias set 3
        if object.myName==1 then 
            object.id= "belt"    
        elseif object.myName ==2 then
            object.id= "bus"
        elseif object.myName ==3 then
            object.id= "toothbrush"
        elseif object.myName ==4 then
            object.id= "necklace"
        elseif object.myName ==5 then
            object.id= "socks"
        elseif object.myName ==6 then
            object.id= "swimsuit"
        end 
         elseif tipoJuego == 31 then  --PENNY diferencias set 1
        if object.myName==1 then 
            object.id= "baboon"    
        elseif object.myName ==2 then
            object.id= "bat"
        elseif object.myName ==3 then
            object.id= "brown bear"
        elseif object.myName ==4 then
            object.id= "pail"
        elseif object.myName ==5 then
            object.id= "pea"
        elseif object.myName ==6 then
            object.id= "puma"
        end 
         elseif tipoJuego == 32 then  --penny diferencias set 2
        if object.myName==1 then 
            object.id= "apple" -- aqui   
        elseif object.myName ==2 then
            object.id= "cowboy" -- aqui
        elseif object.myName ==3 then
            object.id= "hippo" --aqui
        elseif object.myName ==4 then
            object.id= "robin"
        elseif object.myName ==5 then
            object.id= "shampoo"
        elseif object.myName ==6 then
            object.id= "strawberry"
        end 
        elseif tipoJuego == 33 then  --penny diferencias set 3
        if object.myName==1 then 
            object.id= "crab"    
        elseif object.myName ==2 then
            object.id= "lightbulb"
        elseif object.myName ==3 then
            object.id= "lollipop"
        elseif object.myName ==4 then
            object.id= "rope"
        elseif object.myName ==5 then
            object.id= "soap"
        elseif object.myName ==6 then
            object.id= "tub"
        end
        elseif tipoJuego == 34 then  --arrastrar sonidos set 1
        if object.myName==1 then 
            object.id= "camel"    
        elseif object.myName ==2 then
            object.id= "cow"
        elseif object.myName ==3 then
            object.id= "park"
        elseif object.myName ==4 then
            object.id= "puppy"
        elseif object.myName ==5 then
            object.id= "tiger"
        elseif object.myName ==6 then
            object.id= "turtle"
        end     
        elseif tipoJuego == 35 then  --arrastrar sonidos set 2
        if object.myName==1 then 
            object.id= "backpack "    
        elseif object.myName ==2 then
            object.id= "butterfly"
        elseif object.myName ==3 then
            object.id= "cheetah"
        elseif object.myName ==4 then
            object.id= "grasshopper"
        elseif object.myName ==5 then
            object.id= "hippo"
         elseif object.myName ==6 then
            object.id= "raincoat"
        end 
        elseif tipoJuego == 36 then  --arrastrar sonidos set 3
        if object.myName==1 then 
            object.id= "basket"    
        elseif object.myName ==2 then
            object.id= "cat"
        elseif object.myName ==3 then
            object.id= "firework"
        elseif object.myName ==4 then
            object.id= "rope"
        elseif object.myName ==5 then
            object.id= "sheep"
        elseif object.myName ==6 then
            object.id= "yak"
        end 
        elseif tipoJuego == 37 then  --danny diferencias set 1
        if object.myName==1 then 
            object.id= "daisy"    
        elseif object.myName ==2 then
            object.id= "deer"
        elseif object.myName ==3 then
            object.id= "dog"
        elseif object.myName ==4 then
            object.id= "tea"
        elseif object.myName ==5 then
            object.id= "teapot"
        elseif object.myName ==6 then
            object.id= "toad"
        end 
        elseif tipoJuego == 38 then  --danny diferencias set 2 
        if object.myName==1 then 
            object.id= "button"    
        elseif object.myName ==2 then
            object.id= "computer"
        elseif object.myName ==3 then
            object.id= "kitten"
        elseif object.myName ==4 then
            object.id= "ladder"
        elseif object.myName ==5 then
            object.id= "ladybug"
        elseif object.myName ==6 then
            object.id= "window"
        end 
        elseif tipoJuego == 39 then  --danny diferencias set 3
        if object.myName==1 then 
            object.id= "bat"    
        elseif object.myName ==2 then
            object.id= "bread"
        elseif object.myName ==3 then
            object.id= "heart"
        elseif object.myName ==4 then
            object.id= "red"
        elseif object.myName ==5 then
            object.id= "swimsuit"
        elseif object.myName ==6 then
            object.id= "toad"
        end 
        elseif tipoJuego == 40 then  --cally diferencias set 1 
        if object.myName==1 then 
            object.id= "cake"   --aqui 
        elseif object.myName ==2 then
            object.id= "camel"   --aqui
        elseif object.myName ==3 then
            object.id= "car"
        elseif object.myName ==4 then
            object.id= "gecko"
         elseif object.myName ==5 then
            object.id= "girl"
         elseif object.myName ==6 then
            object.id= "gorila"
            
        end 
        elseif tipoJuego == 41 then  --cally diferencias set 2
        if object.myName==1 then 
            object.id= "bicycle"    
        elseif object.myName ==2 then
            object.id= "eagle"--aqui
        elseif object.myName ==3 then
            object.id= "kangaroo"
        elseif object.myName ==4 then
            object.id= "monkey"
        elseif object.myName ==5 then
            object.id= "raccoon"
        elseif object.myName ==6 then
            object.id= "tiger"
        end 
        elseif tipoJuego == 42 then  --cally diferencias set 3 
        if object.myName==1 then 
            object.id= "cake"    
        elseif object.myName ==2 then
            object.id= "dog"
        elseif object.myName ==3 then
            object.id= "milk"
        elseif object.myName ==4 then
            object.id= "mug"
        elseif object.myName ==5 then
            object.id= "pig"
        elseif object.myName ==6 then
            object.id= "yak"
        end 
        elseif tipoJuego == 43 then  --zara diferencias set 1
        if object.myName==1 then 
            object.id= "salami"    
        elseif object.myName ==2 then
            object.id= "scissors"
        elseif object.myName ==3 then
            object.id= "swans"
        elseif object.myName ==4 then
            object.id= "zane"
         elseif object.myName ==5 then
            object.id= "zebra"
         elseif object.myName ==6 then
            object.id= "zinnias"
        end 
        elseif tipoJuego == 44 then  --zara diferencias set 2
        if object.myName==1 then 
            object.id= "baseball"    
        elseif object.myName ==2 then
            object.id= "daisy"
        elseif object.myName ==3 then
            object.id= "dinosaur"
        elseif object.myName ==4 then
            object.id= "popsicle"
        elseif object.myName ==5 then
            object.id= "raisins"
        elseif object.myName ==6 then
            object.id= "roses"
        end 
        elseif tipoJuego == 45 then  --zara diferencias set 3
        if object.myName==1 then 
            object.id= "bus"    
        elseif object.myName ==2 then
            object.id= "gloves"
        elseif object.myName ==3 then
            object.id= "horse"
        elseif object.myName ==4 then
            object.id= "necklase"
        elseif object.myName ==5 then
            object.id= "nose"
        elseif object.myName ==6 then
            object.id= "paws"
        end 

         elseif tipoJuego == 46 then  --arrastrar penny set 1
            if object.myName==1 then 
                object.id= "baboon"    
            elseif object.myName ==2 then
                object.id= "boa"
            elseif object.myName ==3 then
                object.id= "bowl"
            elseif object.myName ==4 then
                object.id= "pole"
            elseif object.myName ==5 then
                object.id= "pour"
            elseif object.myName ==6 then
                object.id= "puma"
        end 

        elseif tipoJuego == 47 then  --arrastrar penny set 2
            if object.myName==1 then 
                object.id= "frisbee"    
            elseif object.myName ==2 then
                object.id= "mailbox"
            elseif object.myName ==3 then
                object.id= "robin"
            elseif object.myName ==4 then
                object.id= "apple"
            elseif object.myName ==5 then
                object.id= "hippo"
            elseif object.myName ==6 then
                object.id= "teapot"
        end 

        elseif tipoJuego == 48 then  --arrastrar penny set 3
            if object.myName==1 then 
                object.id= "crab"    
            elseif object.myName ==2 then
                object.id= "robe"
            elseif object.myName ==3 then
                object.id= "web"
            elseif object.myName ==4 then
                object.id= "mop"
            elseif object.myName ==5 then
                object.id= "rope"
            elseif object.myName ==6 then
                object.id= "ship"
        end 

        elseif tipoJuego == 49 then  --arrastrar sonidos set 1
        if object.myName==1 then 
            object.id= "puppy"    
        elseif object.myName ==2 then
            object.id= "cave"
        elseif object.myName ==3 then
            object.id= "pig"
        elseif object.myName ==4 then
            object.id= "tiger"
        elseif object.myName ==5 then
            object.id= "camp"
        elseif object.myName ==6 then
            object.id= "toad"
        end     
        elseif tipoJuego == 50 then  --arrastrar sonidos set 2
        if object.myName==1 then 
            object.id= "backpack"    
        elseif object.myName ==2 then
            object.id= "raincoat"
        elseif object.myName ==3 then
            object.id= "grasshopper"
        elseif object.myName ==4 then
            object.id= "butterfly"
        elseif object.myName ==5 then
            object.id= "mountain"
         elseif object.myName ==6 then
            object.id= "teepee"
        end 
        elseif tipoJuego == 51 then  --arrastrar sonidos set 3
        if object.myName==1 then 
            object.id= "map"    
        elseif object.myName ==2 then
            object.id= "tent"
        elseif object.myName ==3 then
            object.id= "firework"
        elseif object.myName ==4 then
            object.id= "stick"
        elseif object.myName ==5 then
            object.id= "rope"
        elseif object.myName ==6 then
            object.id= "basket"
        end 
        elseif tipoJuego == 52 then  --arrastrar danny set 1
            if object.myName==1 then 
                object.id= "deer"    
            elseif object.myName ==2 then
                object.id= "dog"
            elseif object.myName ==3 then
                object.id= "door"
            elseif object.myName ==4 then
                object.id= "tea"
            elseif object.myName ==5 then
                object.id= "tire"
            elseif object.myName ==6 then
                object.id= "tomato"
        end 
        elseif tipoJuego == 53 then  --arrastrar danny set 2
            if object.myName==1 then 
                object.id= "window"    
            elseif object.myName ==2 then
                object.id= "button"
            elseif object.myName ==3 then
                object.id= "hotel"
            elseif object.myName ==4 then
                object.id= "water"
            elseif object.myName ==5 then
                object.id= "tornado"
            elseif object.myName ==6 then
                object.id= "ladder"
        end 
        elseif tipoJuego == 54 then  --arrastrar danny set 3
            if object.myName==1 then 
                object.id= "bread"    
            elseif object.myName ==2 then
                object.id= "hand"
            elseif object.myName ==3 then
                object.id= "heart"
            elseif object.myName ==4 then
                object.id= "swimsuit"
            elseif object.myName ==5 then
                object.id= "bat"
            elseif object.myName ==6 then
                object.id= "toad"
        end 
        elseif tipoJuego == 55 then  --arrastrar cally set 1
            if object.myName==1 then 
                object.id= "cake"    
            elseif object.myName ==2 then
                object.id= "camel"
            elseif object.myName ==3 then
                object.id= "gear"
            elseif object.myName ==4 then
                object.id= "gorilla"
            elseif object.myName ==5 then
                object.id= "gown"
            elseif object.myName ==6 then
                object.id= "kiwi"
        end 
        elseif tipoJuego == 56 then  --arrastrar cally set 2
            if object.myName==1 then 
                object.id= "eagle"    
            elseif object.myName ==2 then
               object.id = "kangaroo"
            elseif object.myName ==3 then
                object.id= "magnet"
            elseif object.myName ==4 then
                object.id= "monkey"
            elseif object.myName ==5 then
                object.id= "necklace"
            elseif object.myName ==6 then
                object.id= "pumpkin"
        end 
        elseif tipoJuego == 57 then  --arrastrar cally set 3
            if object.myName==1 then 
                object.id= "cake"    
            elseif object.myName ==2 then
               object.id = "book"
            elseif object.myName ==3 then
                object.id= "mug"
            elseif object.myName ==4 then
                object.id= "pig"
            elseif object.myName ==5 then
                object.id= "yak"
            elseif object.myName ==6 then
                object.id= "bug"
        end 
        elseif tipoJuego == 58 then  --arrastrar zara set 1
            if object.myName==1 then 
                object.id= "scissors"    
            elseif object.myName ==2 then
               object.id = "zebra"
            elseif object.myName ==3 then
                object.id= "sea"
            elseif object.myName ==4 then
                object.id= "seals"
            elseif object.myName ==5 then
                object.id= "zinnias"
            elseif object.myName ==6 then
                object.id= "zoo"
        end 
        elseif tipoJuego == 59 then  --arrastrar zara set 2
            if object.myName==1 then 
                object.id= "glasses"    
            elseif object.myName ==2 then
               object.id = "frisbee"
            elseif object.myName ==3 then
                object.id= "pencil"
            elseif object.myName ==4 then
                object.id= "popsicle"
            elseif object.myName ==5 then
                object.id= "raisins"
            elseif object.myName ==6 then
                object.id= "roses"
        end 
        elseif tipoJuego == 60 then  --arrastrar zara set 3
            if object.myName==1 then 
                object.id= "bus"    
            elseif object.myName ==2 then
               object.id = "horse"
            elseif object.myName ==3 then
                object.id= "mouse"
            elseif object.myName ==4 then
                object.id= "coins"
            elseif object.myName ==5 then
                object.id= "nose"
            elseif object.myName ==6 then
                object.id= "sunrise"
        end 

        elseif tipoJuego == 61 then  --el viaje, el viaje lugares
        if object.myName==1 then 
            object.id= "city"    
        elseif object.myName ==2 then
           object.id = "cinema"
        elseif object.myName ==3 then
            object.id= "circus"
        elseif object.myName ==4 then
            object.id= "camp"
        elseif object.myName ==5 then
            object.id= "sea"
        end 
        elseif tipoJuego == 62 then  --el viaje, el viaje transporte
        if object.myName==1 then 
            object.id= "train"    
        elseif object.myName ==2 then
           object.id = "car"
        elseif object.myName ==3 then
            object.id= "plane"
        elseif object.myName ==4 then
            object.id= "taxi"
        end 
        elseif tipoJuego == 63 then  --el viaje, el viaje pagar
        if object.myName==1 then 
            object.id= "bills"    
        elseif object.myName ==2 then
           object.id = "checks"
        elseif object.myName ==3 then
            object.id= "coins"
        elseif object.myName ==4 then
            object.id= "credit cards"
        end 
        elseif tipoJuego == 64 then  --la compra, la compra
        if object.myName==1 then 
            object.id= "peanut"    
        elseif object.myName ==2 then
           object.id = "steak"
        elseif object.myName ==3 then
            object.id= "pumpkin"
        elseif object.myName ==4 then
            object.id= "pear"
        elseif object.myName ==5 then 
            object.id= "coca"    
        elseif object.myName ==6 then
           object.id = "chocolate"
        elseif object.myName ==7 then
            object.id= "taco"
        elseif object.myName ==8 then
            object.id= "chips"
        end
        elseif tipoJuego == 65 then  --poppy the house, park
        if object.myName==1 then 
            object.id= "ball"    
        elseif object.myName ==2 then
           object.id = "fresbee"
        elseif object.myName ==3 then
            object.id= "stick"
        elseif object.myName ==4 then
            object.id= "bone"
        end 
        elseif tipoJuego == 66 then  --poppy the house, bathroom
        if object.myName==1 then 
            object.id= "shampoo"    
        elseif object.myName ==2 then
           object.id = "soap"
        elseif object.myName ==3 then
            object.id= "sponge"
        elseif object.myName ==4 then
            object.id= "towel"
        end 
        elseif tipoJuego == 67 then  --poppy the house, kitchen
        if object.myName==1 then 
            object.id= "treat"    
        elseif object.myName ==2 then
           object.id = "bowl"
        elseif object.myName ==3 then
            object.id= "plate"
        elseif object.myName ==4 then
            object.id= "cookie"
        end 
        elseif tipoJuego == 68 then  --the trip arrastra set 1
        if object.myName==1 then 
            object.id= "sandals"    
        elseif object.myName ==2 then
           object.id = "toy"
        elseif object.myName ==3 then
            object.id= "ship"
        elseif object.myName ==4 then
            object.id= "shampoo"
        elseif object.myName ==5 then
            object.id= "swimsuit"
        elseif object.myName ==6 then
            object.id= "towel"
        end 
        elseif tipoJuego == 69 then  --the trip arrastra set 2
        if object.myName==1 then 
            object.id= "brushes"    
        elseif object.myName ==2 then
           object.id = "glasses"
        elseif object.myName ==3 then
            object.id= "pencil"
        elseif object.myName ==4 then
            object.id= "button"
        elseif object.myName ==5 then
            object.id= "water"
        elseif object.myName ==6 then
            object.id= "tshirt"
        end 
        elseif tipoJuego == 70 then  --the trip arrastra set 3
        if object.myName==1 then 
            object.id= "toothbrush"    
        elseif object.myName ==2 then
           object.id = "belt"
        elseif object.myName ==3 then
            object.id= "necklace"
        elseif object.myName ==4 then
            object.id= "socks"
        elseif object.myName ==5 then
            object.id= "swimsuit"
        elseif object.myName ==6 then
            object.id= "shirt"
        end 
        elseif tipoJuego == 71 then  --poppy
        if object.myName==1 then 
            object.id= "play"    
        elseif object.myName ==2 then
           object.id = "tub"
        elseif object.myName ==3 then
            object.id= "towel"
        elseif object.myName ==4 then
            object.id= "tidy"
        elseif object.myName ==5 then
            object.id= "treat"
        elseif object.myName ==6 then
            object.id= "sleep"  
        end
        
    end
end

--"sounds/intentalo de nuevo recuerda pronunciar correctamente.mp3")

-- pone los sonidos y los elimina de momoria
-- el primer parametro sera el sonido que primerSonidoa
-- el segundo parametro es para saber si quieres que haya bloqueo
--                            si es 0 se pone bloqueo y se elimina
--                            si es 1 solo se elimina el bloqueo (es el caso de que venga de validar voz)
-- el tercer parametro es una funcion (en la clase que lo invoca se pone que 
--                                      hará cuando tome valor esta funcion)
function utils.reproducirSonido( sonido, bloqueo, fnCallback )
    print(sonido)
    if bloqueo == 0 then
        composer.showOverlay( "bloqueo" ,{ isModal = true } )
    end
    
    local sound = audio.loadSound( "sounds/"..sonido..".mp3" )
    
    local audioChannel = audio.play( sound, { channel = audioChannel,
        onComplete= function()
            audioChannel = nil
            audio.dispose( audioChannel )
            if bloqueo == 0 or bloqueo == 1 then
                composer.removeScene( "bloqueo" )
            end
            if fnCallback~=nil then
                fnCallback()
            end
    end  } )
    if audioChannel > 0 then 
        --audio.setVolume( 0.07, { channel=laserChannel } )
        --print( "channel new " .. laserChannel )
    end
    
end

--Método para generar el drag & drop
function utils.dragging( myImage, toImage, nameScene, fnCallback )
    
    function myImage:touch( event )
        if event.phase == "began" then
            
            -- begin focus  ---- +++ para que no mueva otras piezas mientras arrastras +++ ----
            display.getCurrentStage():setFocus( self, event.id )
            self.isFocus = true
            
            myImage:toFront()
            
            markX = self.x    -- store x location of object
            markY = self.y    -- store y location of object
            isBegan = true    -- Esta variable sirve para saber si se ha comenzado a tocar la imagen con un touch
            --print ("Began dragging "..markX.." myImage.id"..myImage.id)
        elseif event.phase == "moved" then
            --print (tostring(isBegan))
            if isBegan== false then --Hay casos que no entra a began porque la imagen comienza "en movimiento"
                -- cuando pasa esto la lo que se requiere es la posicion original de la imagen que se quiere mover
                markX = self.x    -- store x location of object
                markY = self.y    -- store y location of object
                isBegan=true --Se marca la bandera para indicarq que la imagen ha comenzado a moverse ya
            end
            --Procedimiento normal
            local x = (event.x - event.xStart) + markX
            local y = (event.y - event.yStart) + markY
            
            self.x, self.y = x, y    -- move object based on calculations above
            
            
            
            --Para apagar la bandera cuando se libera
        elseif event.phase == "ended" or event.phase == "cancelled" then
            isBegan=false --apagar la bandera para evitar conflictos con el bloqueo temporal del sonido y draggins futuros    
            currentScene = require( nameScene )
            currentScene.imagenSeleccionada = myImage
            currentScene.imagenColisionada = toImage
            currentScene.posIniX = markX
            currentScene.posIniY = markY
            
            --end focus
            display.getCurrentStage():setFocus( self, nil )
            self.isFocus = false
            
            if utils.colision(myImage,toImage) then                 
                --print ("El nombre de la imagen sleccionada: "..currentScene.imagenSeleccionada.id)
                fnCallback(true)-- devuelve si colisiono
            else
                fnCallback(false)
            end
            
            -- print(" final "..tostring(isBegan))
        end
        
        return true
    end
    
    
    
    -- make 'myObject' listen for touch events
    myImage:addEventListener( "touch", myImage )
    
end

function utils.obtenerPreferencesJuego(nombreJuego)
    row = base.consultarJuego(nombreJuego)
    if row ==nil then
        print("el juego: "..nombreJuego.. " no existe, verificar el nombre en la tabla minijuegos ")
    end
    return row
end

function utils.actualizarPreferenciaJuego(campo,valorNuevo,nombreJuego)
    base.actualizarJuegoSuper(campo, valorNuevo, nombreJuego)
end

function utils.colision(obj1, obj2) 
    
    --Sirve para colicionar la el touch del usuario con su dedo y una imagen enespecifico
    if obj1.w==nil then
        obj1.w=0
    end
    if obj1.h==nil then
        obj1.h=0
    end
    
    --Objeto 1
    posicionX1delObj1=(obj1.x-obj1.w/2)
    posicionX2delObj1=(obj1.x+obj1.w/2)
    posicionY1delObj1=(obj1.y-obj1.h/2)
    posicionY2delObj1=(obj1.y+obj1.h/2)
    --Objeto 2
    posicionX1delObj2=(obj2.x-obj2.w/2)
    posicionX2delObj2=(obj2.x+obj2.w/2)
    posicionY1delObj2=(obj2.y-obj2.h/2)
    posicionY2delObj2=(obj2.y+obj2.h/2)
    
    if (posicionX2delObj1>=posicionX1delObj2 and posicionX1delObj1<=posicionX2delObj2)
        and(posicionY2delObj1>=posicionY1delObj2 and posicionY1delObj1<=posicionY2delObj2)
        
   	then
   	return true
    else
   	return false
    end
    
end

function utils.tipoImagen()
    if  gameState.tipodejuego==1 then
        path="images/lacompra/"
    elseif  gameState.tipodejuego==2 then
        path="images/elviaje/"
    elseif  gameState.tipodejuego==3 then
        path="images/penny_benny/"
    elseif  gameState.tipodejuego==4 then
        path="images/lossonidos/"
    elseif  gameState.tipodejuego==5 then
        path="images/popi/"
    elseif  gameState.tipodejuego==6 then
        path="images/danny_tanny/"
    elseif  gameState.tipodejuego==7 then
        path="images/cally_gilly/"
    elseif  gameState.tipodejuego==8 then
        path="images/zara_zane/"
    end     
end

function utils.resetearMiniJuego(nombreMiniJuego)
    base.resetearMiniJuego(nombreMiniJuego)    
end

--Indica si un elemento existe dentro de una tabla
function utils.contains(table, element)
    for _, value in pairs(table) do
        if value == element then
            return true
        end
    end
    return false
end

function utils.ponerGlobos(destino)
    
    local destination = destino
    
    if destination == nil or destination == "" then
        destination = "menus.menuDesbloquear"
    end
    
    local group = display.newGroup()
    
    utils.reproducirSonido("sounds/principales/globos", 0, 
    function() 
        physics.stop()
        group:removeSelf()
        composer.gotoScene ( destination, { effect = "fade"} )
    end)
    
    physics.start()
    
    local globo = display.newImage( group,"images/menu/confeti 2.png" )
    globo:translate( centerX, centerY )
    
    
    physics.setGravity( 0, 2 )
    
    physics.addBody( globo )
    
end

return utils
