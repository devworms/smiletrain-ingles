-- Tabla de Palabras, solo para la creacion de la base de datos

local tbl_variables={}
        
    function tbl_variables.query()
        local query =  

        [[ 

                    INSERT INTO variables VALUES ('nivel2j3arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j3arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j3diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j3diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j3memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j3memorama', 'false');
		    INSERT INTO variables VALUES ('nivel2j3recuerda', 'false');
		    INSERT INTO variables VALUES ('nivel3j3recuerda', 'false');
		     
		    INSERT INTO variables VALUES ('nivel2j4arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j4arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j4diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j4diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j4memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j4memorama', 'false');
		    INSERT INTO variables VALUES ('nivel2j4recuerda', 'false');
		    INSERT INTO variables VALUES ('nivel3j4recuerda', 'false');

		    INSERT INTO variables VALUES ('nivel2j6arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j6arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j6diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j6diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j6memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j6memorama', 'false');
		    INSERT INTO variables VALUES ('nivel2j6recuerda', 'false');
		    INSERT INTO variables VALUES ('nivel3j6recuerda', 'false');

		    INSERT INTO variables VALUES ('nivel2j7arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j7arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j7diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j7diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j7memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j7memorama', 'false');
		    INSERT INTO variables VALUES ('nivel2j7recuerda', 'false');
		    INSERT INTO variables VALUES ('nivel3j7recuerda', 'false');

		    INSERT INTO variables VALUES ('nivel2j8arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j8arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j8diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j8diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j8memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j8memorama', 'false');
		    INSERT INTO variables VALUES ('nivel2j8recuerda', 'false');
		    INSERT INTO variables VALUES ('nivel3j8recuerda', 'false');  

		    INSERT INTO variables VALUES ('nivel2j5memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j5memorama', 'false'); 

		    INSERT INTO variables VALUES ('nivel2j2arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel3j2arrastra', 'false');
		    INSERT INTO variables VALUES ('nivel2j2diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j2diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j2memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j2memorama', 'false');  

		    INSERT INTO variables VALUES ('nivel2j1diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel3j1diferencias', 'false');
		    INSERT INTO variables VALUES ('nivel2j1memorama', 'false');
		    INSERT INTO variables VALUES ('nivel3j1memorama', 'false');

         ]]
        return query
    end

return tbl_variables
