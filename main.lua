--require "CiderDebugger";
local composer = require( "composer" )
local base = require("utilerias.base" )
utils = require("utilerias.utils" )

display.setStatusBar( display.HiddenStatusBar )
system.setIdleTimer( false ) -- para no apagar la pantalla

centerX = display.contentWidth/2
centerY = display.contentHeight/2

path = ""
pathGame = ""
pathSelection = ""
destinoDeJuegoSeleccionado = ""
destinoBack = ""
musicafondochanel = nil
readIt = ""

  optionsTextMenu = 
        {
            --parent = group,
            text = "",
            width = 800,
            x=centerX,
            y=centerY/3.5,
            align = "center",--required for multi-line and alignment
            font = "gotham",
            fontSize = 37
        }

--[[

TRY CATH con la funcion pcall()

function foo () 
  if unexpected_condition then error() end
  print( "pathBaseResource: "..pathBaseResource )
end

  
if pcall(foo) then
      -- no errors while running `foo'      
  else
      -- `foo' raised an error: take appropriate actions
end

]]
composer.gotoScene ( "home", { effect = "fade"} )

------HANDLE SYSTEM EVENTS------
function systemEvents(event)
   print("systemEvent " .. event.type)
   if ( event.type == "applicationSuspend" ) then
      print( "suspending..........................." )
   elseif ( event.type == "applicationResume" ) then
      print( "resuming............................." )
   elseif ( event.type == "applicationExit" ) then
      --Cerrar la base de datos
      base.cerrar()
      print( "exiting.............................." )
   elseif ( event.type == "applicationStart" ) then
      base.abrir()
            
   end
   return true
end

Runtime:addEventListener( "system", systemEvents )

-- Called when a key event has been received
function onKeyEventPress( event )
    
    if ( event.keyName == "back" and composer.getSceneName( "current" )~= "home" and event.phase == "down" ) then
        local platformName = system.getInfo( "platformName" )
        if ( platformName == "Android" ) then
            
            audio.stop()
            
            utils.tipoImagen()
            utils.reproducirSonido("boton")
            
            composer.gotoScene ( destinoBack, { effect = "fade"} )
   
            print("back: " .. destinoBack)
          
            return true
        end
    end

    -- IMPORTANT! Return false to indicate that this app is NOT overriding the received key
    -- This lets the operating system execute its default handling of the key
    return false
end

-- Add the key event listener
Runtime:addEventListener( "key", onKeyEventPress )
