-----------------------------------------------------------------------------------------
-- papas.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
local ventanas = {}

local video = ""
local textLoad
local SpriteNuevo
destinoBack = "extras.papas"

----------------------------------------------------------------------------------------
local function btnTap(event)
    local audioChannel = audio.stop() 
    if (musicafondochanel~=nil) then
        audio.stop(musicafondochanel)  
        audio.dispose(musicafondochanel)
        musicafondochanel=nil
    end

    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    
    return true
end
----------------------------------------------------------------------------------------
-- @param event
-- @return
local onCompleteVideo = function( event )
        print( "video session ended" )
        
end

local function vTap(event)
    local audioChannel = audio.stop() 
    if (musicafondochanel~=nil) then
        audio.stop(musicafondochanel)  
        audio.dispose(musicafondochanel)
        musicafondochanel=nil
    end

    video = event.target.myName 
    media.playVideo( video , true, onCompleteVideo )
    
end

function scene:create( event )
    
    local group = self.view
    
    local background = display.newImage( group,"images/papas/fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()

    --[[local btnInfo = display.newImage( group,"images/menu/btninfo.png" )
    btnInfo:translate( centerX*1.75, centerY*1.9 )
    btnInfo :addEventListener("tap", btnTap)
    btnInfo.destination = "extras.info_papas"]]
    
    local imgWidth, imgHeight = 210, centerY / 1.5
    local sumWidth, sumHeight = 300, 120
    
    for count = 1, 9 do
        ventanas[count] = display.newImage(group,"images/papas/help/"..count..".png")
        
        ventanas[count]:translate(imgWidth, imgHeight)
        imgWidth = imgWidth + sumWidth
            
            if(count%3 == 0) then
                imgHeight = imgHeight + sumHeight
                imgWidth = imgWidth - 900
            end
        
        ventanas[count].myName = "images/papas/help/" .. count ..".mp4"  
        ventanas[count]:addEventListener( "tap", vTap )
    end
    
    local topsign = display.newImage( group,"images/top-sign.png" )
        topsign:translate( centerX, centerY/5 )
    local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text="Help your Child Learn their Sounds"
        inst.xScale=.7
        inst.yScale=.7
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "extras.papas"

    --local musicafondo =audio.loadStream( "sounds/sounds/home/fondo.mp3" )
    --musicafondochanel= audio.play( musicafondo, { channel=audioChannel, loops= -1 }  )

    utils.reproducirSonido("sounds/menu/menuhelpp")
    
end        
 ------------------------------------------------------------------------------------------


function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene
