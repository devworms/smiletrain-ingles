--[[local aspectRatio = display.pixelHeight / display.pixelWidth
application = {
    content = {
        width = aspectRatio > 1.5 and 576 or math.floor( 1024 / aspectRatio ),
        height = aspectRatio < 1.5 and 1024 or math.floor( 576 * aspectRatio ),
        scale = "letterbox",
        fps = 30,
    },
}
]]


application = {
	content = {
		width = 576,
		height = 1024, 
		scale = "zoomStretch",
		fps = 35,
		
	},
 
}

-- https://coronalabs.com/blog/2012/12/04/the-ultimate-config-lua-file/
--[[application = {
	content = {
		width = 320,
		height = 480, 
		scale = "letterBox",
		fps = 35,
		
	},
 
}]]
