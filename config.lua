local platformName = system.getInfo( "platformName" )

print("platformName::: "..platformName)

if ( platformName ~= "Android" ) then
	application =
		{
		        content =
		        {	
		            width = 576,
		            height = 1024,
		            scale = "letterBox", -- Android: zoomStretch / iOS: letterBox
		            fps = 35,
		        },
		        license =
				{
					google =
					{
						key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhB1uiBWHu5dT8xoj64lv3L2SMPTSvGTY4JAqUzn1kM0m/fP6k7TJ6flMpCoCTP9KVWDuvdxx6nwVCDoT/jzd3HwTl3T65PDQGqMWceFIfhAQbsIeFlx3Ht20fdiVSLeQqSBCFjwS0w/7RMYc9zmczT3KZiMhrfAqlM4dboUhIafVWH6u637/Xht0PVnexZwehbX5gDbJTXQXvmYgxDiAwC27IvM+aY+hHVKWMFE3GQ4pIPelnzO1Y1tzPY8rvqlxONHC5RotXryXxGg59IFRU1KlHbl6/2ed7qPDy7cmn9EUvpbo6kd411tNJeF7i9RxYiJrBoxQSEkcx1zXQPvymQIDAQAB"
					},
				},
		}
else
	application =
		{
		        content =
		        {	
		            width = 576,
		            height = 1024,
		            scale = "zoomStretch", -- Android: zoomStretch / iOS: letterBox
		            fps = 35,
		        },
		}
end


-- https://coronalabs.com/blog/2012/12/04/the-ultimate-config-lua-file/
--[[application = {
	content = {
		width = 320,
		height = 480, 
		scale = "letterBox",
		fps = 35,
		
	},
 
}]]
