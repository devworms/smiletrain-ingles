local composer = require( "composer" )
local scene = composer.newScene()
local gameState=require("utilerias.gameState")
local lfs = require("lfs") --sistema de archivos
local  destino
local textLoad
local banderaPop = false
local codePaciente = ""
local dialogText
local btnVideos 
local totalVideos
local array ={}
local actual 
local video --= native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
local m=0
local bandera= false
local btnAntes
local btnNext
local btnInicio
local btnPause
local btnJ1
local musicafondo

destinoBack = "menus.menuHistory"

local function videoListener( event )
     print( "Event phase: " .. event.phase )
      
    if event.errorCode then
        native.showAlert( "Error!", event.errorMessage, { "OK" } )
    end
end
local onComplete = function ( event)
print("si funciona :)")
    -- body
end
local  function dameArray( totalVideos, destino )
   
               
                    for i=1, totalVideos do
                        array[i]=destino.."escena"..i..".mp4"
                        print(array[i])
                    end
return array
end 
local  function pause(event)
    if(bandera==false) then
    video:pause()
    video:pause()
    print("Pausando video")
    bandera=true
    elseif(bandera==true)then
    video:play()

    print("Reproduciendo video")
    bandera=false
    end

end 

local function btnTap(event)

    local audioChannel = audio.stop()
            
    if (musicafondochanel ~= nil) then
        audio.stop(musicafondochanel)  
        audio.dispose(musicafondochanel)
        musicafondochanel=nil
    end
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")
            
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    
    return true
end 
 
local function btnPlay(event)     
            m=1

            audio.stop()
            musicafondochanel= audio.play( musicafondo,{ channel=0, loops=-1} )
            dameArray(totalVideos, destino)
            botonesVideo()
            --video = native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
            video:load( array[1] )
            print("Reproduciendo",array[1])
            
            video:addEventListener( "video", videoListener )
           
            video:play()
            btnJ1:removeSelf()
                    
end
local  function inicio( event )
            video:pause()
            btnJ1.isVisible=false
            btnJ1.isEnabled=false
            --video = native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
            video:addEventListener( "video", videoListener )
            video:load( array[m] )
            video:play()
            print("Reproduciendo", array[m])
            if(m<1)then
                btnAntes.isVisible=false
                btnAntes.isEnabled=false
                btnNext.isVisible=true
                btnNext.isEnabled=true
            
            elseif(m>=totalVideos)then
                btnAntes.isVisible=true
                btnAntes.isEnabled=true
            end
           
            
            -- body
end


local function gettipoJuego()
    if  gameState.tipodejuego == 1 then -- la compra
        destino="videos/lacompra/" 
        totalVideos=13
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif  gameState.tipodejuego == 2 then -- el viaje
        destino="videos/elViaje/" 
         totalVideos=17
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif  gameState.tipodejuego == 3 then -- penny
        destino="videos/penny/"
         totalVideos=23
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)

    elseif  gameState.tipodejuego == 4 then -- los sonidos
        destino="videos/losSonidos/"
          totalVideos=13
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif  gameState.tipodejuego == 5 then -- popi
        destino="videos/popi/"
         totalVideos=16
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif gameState.tipodejuego == 6 then  -- danny
        destino="videos/danny/"
         totalVideos=20
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif gameState.tipodejuego == 7 then  -- cally
        destino="videos/cally/"
         totalVideos=25
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    elseif gameState.tipodejuego == 8 then  -- zara
        destino="videos/zara/"
        totalVideos=12
        print("ruta:",destino)
        print("Total de videos: ",totalVideos)
    end    
end 
local  function antes( event )
        video:pause()
        if(m==1)then
        btnAntes.isVisible=false
        btnAntes.isEnabled=false
        --video = native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
        video:load( array[1] )
        video:addEventListener("video", videoListener )
        video:play()
        print(array[m])
        
        elseif (m>=2 and m<=totalVideos) then
            m=m-1
            btnNext.isVisible=true
            btnNext.isEnabled=true
            btnAntes.isVisible=true
            btnAntes.isEnabled=true
            --video = native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
            video:load( array[m] )
            print(array[m])
            video:addEventListener("video", videoListener )
            video:play()
         end
end

local  function next( event )
        
        if(m>=1 and m<totalVideos) then
            --video = native.newVideo( display.contentCenterX, display.contentCenterY, 520, 680 )
            video:pause()
            m=m+1
            video:load( array[m] )
            print(array[m])
            video:addEventListener("video", videoListener )
            video:play()

            if(totalVideos==m) then
                btnNext.isVisible=false
                btnNext.isEnabled=false
            else
                btnAntes.isVisible=true
                btnAntes.isEnabled=true
            end
                        
        end

end

-- Called when the scene's view does not exist:
function scene:create( event )
    local group = self.view
        
        gettipoJuego()

        video = native.newVideo( centerX, display.contentCenterY-35, 1024, 520 )
         
        musicafondo =audio.loadStream(destino.."sonido.mp3" )
        local background = display.newImage( group,path.."fondo-1.png" )--fondo video
        background:translate( centerX, centerY )

        --local topsign = display.newImage( group,"images/top-sign.png" )
        --topsign:translate( centerX, centerY/5 )
        
        btnJ1 = display.newImage( group,"images/menu/play.png" )
        btnJ1:translate( centerX, centerY )
        btnJ1.isVisible=true
        btnJ1.isEnabled=true
        btnJ1:addEventListener("tap", btnPlay)
        
       function botonesVideo( )
            btnAntes = display.newImage(group,"videos/Utilerias/btnAntes.png")
            btnAntes:translate( centerX*1.3, centerY*1.9)
            btnAntes.isVisible=false
            btnAntes.isEnabled=false
            btnAntes: addEventListener("tap", antes)
            btnAntes:scale(.55,.55)

            btnInicio = display.newImage(group,"videos/Utilerias/btnPlays.png")
            btnInicio:translate( centerX*1.5, centerY*1.9)
            btnInicio:addEventListener("tap", inicio)
            btnInicio:scale(.55,.55)
            
            btnNext = display.newImage(group,"videos/Utilerias/btnNexts.png")
            btnNext:translate(centerX*1.7, centerY*1.9)
            btnNext: addEventListener("tap", next)
            btnNext:scale(.55,.55)

            btnPause = display.newImage(group,"videos/Utilerias/btnPauses.png")
            btnPause:translate( centerX*1.9, centerY*1.9)
            btnPause:addEventListener("tap", pause)
            btnPause:scale(.55,.55)

        end 
                ---- +++ video dentro de una pantalla +++ ----
        -- http://docs.coronalabs.com/api/library/native/newVideo.html
        
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menus.menuHistory"
        
       
        
        --utils.reproducirSonido("sounds/lacompra/Cantar el cuento/intro",0)
        composer.removeScene( "bloqueo" )
    introIsPlaying=true

end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
    -- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
    local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
    local group = self.view
   
    if video ~= nil then
        video:removeSelf()
        video = nil
    end
            
    composer.removeScene( composer.getSceneName( "current" ) )
    
    -- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end



-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
    local group = self.view
    bandera=false  
    
    -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
---------------------------------------------------------