-----------------------------------------------------------------------------------------
-- arrastra.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local base = require("utilerias.base" )
local scene = composer.newScene()
local imagenProducto = {}
local introIsPlaying


-- Variable que indica si ganas el juego
gameState.success = 0

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil

-- Variable que contabilizará los aciertos del juego
local getSuccess
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
--crea el objeto r que va a servir para hacer las grabaciones
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)
end
local function remover ()
    if gameState.tipodejuego ~= 5 then
    scene.imagenSeleccionada:removeSelf()
    end
end


---------------------******Funcion obligatoria*********----
function scene:successful()
    
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    --local pronuncia = true
    
        --[[if pronuncia then
        local r=math.random(1,2)
       local ruta
        if r==1 then
            ruta="excelente"
        else
            ruta="muybien"
            utils.reproducirSonido("sounds/principales/"..ruta,0,function ( ... )
        end]]--
        composer.showOverlay( "bloqueo" ,{ isModal = true } )
       
        print("presionado")
    utils.reproducirSonido("bip")
timer.performWithDelay(1000, function()
   r:startRecording()
   end,1)
   
    timer.performWithDelay(3000, function() 
                    r:stopRecording()
                    utils.reproducirSonido("bip")
                    timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
	 			audio.play( playbackSoundHandle, { onComplete= llamame,remover() } )
              end 
              end,1)
                end,1)
        scene.imagenSeleccionada:removeEventListener("touch")
           

            gameState.success = gameState.success+1
            

        
            
        
            if gameState.success >= getSuccess then
                
                 
                     
                    if pathSelection == "juego1/" then
                        base.actualizarVariable("nivel2j" .. gameState.tipodejuego .. "arrastra", 'true')
                    elseif pathSelection == "juego2/" then
                        base.actualizarVariable("nivel3j" .. gameState.tipodejuego .. "arrastra", 'true')
                    elseif pathSelection == "juego3/" then
                        base.actualizarVariable("nivel2j" .. gameState.tipodejuego .. "arrastra", 'false')
                        base.actualizarVariable("nivel3j" .. gameState.tipodejuego .. "arrastra", 'false')
                    end
                     
                     if gameState.tipodejuego == 5 then
                         timer.performWithDelay(6000, function()
                          utils.ponerGlobos("menus.menuJuego")
                          end,1)
                     else
                        timer.performWithDelay(6000, function()
                         --utils.reproducirSonido("sounds/principales/gana",1,function ( ... )
                         utils.ponerGlobos()
                    --end)
                    end,1)
                      end
                     
                         gameState.success = 0
            else
                composer.removeScene( "bloqueo" )
            end      
        --end)

              
            
       --[[ else
            transition.to( scene.imagenSeleccionada , {x=scene.posIniX, y=scene.posIniY})
            
            utils.reproducirSonido("sounds/principales/intentalo",1)       
        end]]--
end

 ------------------------------------------------------------------------------------------
-- FUNCION "SHUFFLE" PARA NÚMEROS ALEATORIOS NO REPETIDOS
-------------------------------------------------

local function shuffleArray(array)
        local arrayCount = #array
            for i = arrayCount, 2, -1 do
                local j = math.random(1, i)
                array[i], array[j] = array[j], array[i]
            end
    return array
end

 ------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
local function btnTap(event)
       if introIsPlaying == true then
    local audioChannel = audio.stop() 
   introIsPlaying = false
    end
       utils.reproducirSonido("boton")
        composer.gotoScene ( event.target.destination, { effect = "fade"} )
        print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------

function scene:create( event )
   
    local objColision
    local objColision2
    local objColision3
    local counter = 6
    local tipoJuego
    
    local group = self.view
    
   print(gameState.tipodejuego.."aca")
        if(gameState.tipodejuego == 2) then
            tipoJuego = 15
            imagenProducto = {1,2,3,4,5,6}
            
        elseif gameState.tipodejuego == 5 then
                tipoJuego = 71                
                imagenProducto = {1,2,3,4,5,6}                               
                objColision = {}  
        elseif gameState.tipodejuego == 4 then
            imagenProducto = {1,2,3,4,5,6}
        else
            
            counter = 5
            tipoJuego = 19                
            imagenProducto = {1,2,3,4,5}
                
        end
        
    ----------------------- CREACIÓN DE COLISIONADORES -----------------------------------
    
    if(gameState.tipodejuego ~= 5)then
        
        objColision = display.newImage( group, path..pathGame.."objColision.png" )
        objColision:translate( centerX/2.5, centerY/2 )
        objColision.w=objColision.contentWidth
        objColision.h=objColision.contentHeight
        local inst = display.newText( optionsTextMenu )
        group:insert(inst)
        inst.text ="P"
        
        objColision2 = display.newImage( group, path..pathGame.."objColision2.png" )
        objColision2:translate( centerX*1.05, centerY/2 )
        objColision2.w=objColision.contentWidth
        objColision2.h=objColision.contentHeight
        
        objColision3= display.newImage( group, path..pathGame.."objColision3.png" )
        objColision3:translate(centerX/.6 , centerY/2 )
        objColision3.w=objColision.contentWidth
        objColision3.h=objColision.contentHeight
        
    else
        local sumWidth, sumHeight = centerX/2.3, centerY - 40
        for count = 7, 12 do
            objColision[count] = display.newImage( group, path..pathGame..pathSelection..count..".png" )
            objColision[count]:translate( sumWidth, sumHeight )
                        
            sumWidth = sumWidth + 280
            
            if(count%3==0) then
                sumHeight = sumHeight + 177
                sumWidth = sumWidth - 840
            end
             
            objColision[count].w=objColision[count].contentWidth/2
            objColision[count].h=objColision[count].contentHeight
        end
    end
        
        ----------------------- CREACIÓN DE DRAGGEABLES -----------------------------------
         
        local imgWidth, sumWidth
        local imgHeight, sumHeight
        local countHeight
                
            -- Asignar valores de inicio a cada juego
        
            if gameState.tipodejuego == 2 then -- La maleta
                    imgWidth, imgHeight = display.contentWidth/2.1, centerY / 2.35
                    sumWidth, sumHeight = 121, 150
                    countHeight = 2
                    getSuccess = 6
                    
            elseif(gameState.tipodejuego == 5) then -- Secuencias   
                    imgWidth, imgHeight = 100, centerY / 3.1
                    sumWidth, sumHeight = 165, 150
                    countHeight = 7
                    getSuccess = 6
                    
            else
                
                imgWidth, imgHeight = display.contentWidth*.1, centerY*1.5 
                    sumWidth, sumHeight = 170, 170
                    countHeight = 6
                    getSuccess = 6
            end
        
        -- hacemos random un las posiciones de las piezas        
        shuffleArray(imagenProducto)
        
        --for count = 1, counter do
        for posicion, pieza in pairs (imagenProducto) do
            
            imagenProducto[posicion] = display.newImage(group, path..pathGame..pathSelection..pieza..".png")            
            imagenProducto[posicion].myName = pieza
                        
            -- CREAR IMÁGENES POR EL TIPO DE JUEGO
            --utils.renombrarImagenes(tipoJuego,imagenProducto[posicion])
            if gameState.tipodejuego == 3 then
                if pathSelection == "juego1/" then  --penny arrastra
                    utils.renombrarImagenes(46,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(47,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(48,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 4 then
                if pathSelection == "juego1/" then  --los sonidos arrastra
                    utils.renombrarImagenes(34,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(35,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(36,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 6 then
                if pathSelection == "juego1/" then  --danny arrastra
                    utils.renombrarImagenes(52,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(53,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(54,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 7 then
                if pathSelection == "juego1/" then  --cally arrastra
                    utils.renombrarImagenes(55,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(56,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(57,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 8 then
                if pathSelection == "juego1/" then  --zara arrastra
                    utils.renombrarImagenes(58,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(59,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(60,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 2 then
                if pathSelection == "juego1/" then  --the trip arrastra
                    utils.renombrarImagenes(68,imagenProducto[posicion])
                elseif pathSelection =="juego2/" then
                    utils.renombrarImagenes(69,imagenProducto[posicion])
                elseif pathSelection =="juego3/" then
                    utils.renombrarImagenes(70,imagenProducto[posicion])
                end
            elseif gameState.tipodejuego == 5 then
                  --the trip arrastra
                    utils.renombrarImagenes(71,imagenProducto[posicion])
                
            end
                    
            imagenProducto[posicion]:translate(imgWidth, imgHeight)
            imgWidth = imgWidth + sumWidth   
            
            
            
            if(posicion%countHeight == 0) then
                imgHeight = imgHeight + sumHeight
                imgWidth = imgWidth - (sumHeight*(countHeight/1.3))
    end          
                      
            imagenProducto[posicion].w = imagenProducto[posicion].contentWidth
            imagenProducto[posicion].h = imagenProducto[posicion].contentHeight
            
                if(gameState.tipodejuego ~= 5)then
                    
                    ---***************************************************************
                    utils.dragging(imagenProducto[posicion],objColision,composer.getSceneName("current"),
                        function (resultado) 
                               --esto es para quitar el bloqueo 
                            if introIsPlaying == true then
                             local audioChannel = audio.stop() 
                            introIsPlaying = false
                            end 
                            if resultado then
                              

                                if(gameState.tipodejuego == 3) then --penny and benny
                                    if pathSelection == "juego1/" then
--ver q onda aqui
                                        if(imagenProducto[posicion].id == "puma" or imagenProducto[posicion].id == "baboon" or imagenProducto[posicion].id == "bunny") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego2/" then
                                        if(imagenProducto[posicion].id == "bowl" or imagenProducto[posicion].id == "pole" or imagenProducto[posicion].id == "bull") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego3/" then
                                        if(imagenProducto[posicion].id == "boa" or imagenProducto[posicion].id == "boar" or imagenProducto[posicion].id == "pour") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end    
                                    end
                                elseif (gameState.tipodejuego == 4) then --los sonidos
                                    if pathSelection == "juego1/" then
                                        if(imagenProducto[posicion].id == "park" or imagenProducto[posicion].id == "puppy") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego2/" then
                                        if(imagenProducto[posicion].id == "hippo" or imagenProducto[posicion].id == "grasshopper") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego3/" then
                                        if(imagenProducto[posicion].id == "sheep" or imagenProducto[posicion].id == "rope") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end    
                                    end
                               
                      
                                elseif (gameState.tipodejuego == 8) then --zarah 
                                    if pathSelection == "juego1/" then
                                        if(imagenProducto[posicion].id == "salami" or imagenProducto[posicion].id == "scissors" or imagenProducto[posicion].id == "zinnias") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego2/" then
                                        if(imagenProducto[posicion].id == "sea" or imagenProducto[posicion].id == "seals" or imagenProducto[posicion].id == "sew") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego3/" then
                                        if(imagenProducto[posicion].id == "sail" or imagenProducto[posicion].id == "sunrise" or imagenProducto[posicion].id == "celery") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end    
                                    end
                                else--este es el 2
                                    utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    
                                end
                            else
                                --[[
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})]]--
                                utils.dragging(imagenProducto[posicion],objColision2,composer.getSceneName("current"),
                        function (resultado)
                            if resultado then
                                if pathSelection == "juego1/" then  
                                        if(imagenProducto[posicion].id == "tiger" or imagenProducto[posicion].id == "turtle") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego2/" then
                                        if(imagenProducto[posicion].id == "butterfly" or imagenProducto[posicion].id == "cheetah") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego3/" then
                                        if(imagenProducto[posicion].id == "basket" or imagenProducto[posicion].id == "cat") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end    
                                    end
                                else
                                    
                                    utils.dragging(imagenProducto[posicion],objColision3,composer.getSceneName("current"),
                        function (resultado)
                            if resultado then
                                if pathSelection == "juego1/" then
                                        if(imagenProducto[posicion].id == "camel" or imagenProducto[posicion].id == "cow") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego2/" then
                                        if(imagenProducto[posicion].id == "backpack " or imagenProducto[posicion].id == "raincoat") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end
                                    elseif pathSelection == "juego3/" then
                                        if(imagenProducto[posicion].id == "yak" or imagenProducto[posicion].id == "firework") then
                                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                        else
                                            utils.reproducirSonido("error")
                                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                        end    
                                    end
                                else
                                    utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                
                            end
                            
                            end)
                                
                            end
                            
                            end)
                            end
                        end)
                    
                    else 
                    utils.dragging(imagenProducto[posicion],objColision[pieza+6],composer.getSceneName("current"),
                    function (resultado) 
                         --esto es para quitar el bloqueo 
    if introIsPlaying == true then
    local audioChannel = audio.stop() 
   introIsPlaying = false
    end          
                        if resultado then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    end)
                    
                    
                        
                    
                end
        end      
                
    local background = display.newImage( group, path..pathGame.."fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()

    matchText = display.newText(group,"", centerX, (centerY/3)*4, "fonts/gothamblack" , 26 )
    matchText:setTextColor(0, 0, 0)
    matchText.x = centerX
--[[if(gameState.tipodejuego~=5 and gameState.tipodejuego~=2 and gameState.tipodejuego~=1)then
    local topsign = display.newImage( group,"images/top-sign.png" )
    topsign:translate( centerX, centerY/5 ) 
    local inst = display.newText( optionsTextMenu )
    group:insert(inst)
 
 if(gameState.tipodejuego == 3)then
     inst.text="“B” or “P”"
 elseif(gameState.tipodejuego == 4)then
     inst.text="“P”, “T” and “K”"
 elseif(gameState.tipodejuego == 6)then
     inst.text="“T” or “D”"
 elseif(gameState.tipodejuego == 7)then
     inst.text="“C” or “G”"
 elseif(gameState.tipodejuego == 8)then
     inst.text="“S” or “Z”"
     end
    end]]--
    
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    
    if gameState.tipodejuego == 5 then
        btnRegresar.destination = "menus.menuJuego"
        destinoBack = "menus.menuJuego"
    else
        btnRegresar.destination = "menus.menuDesbloquear"
        destinoBack = "menus.menuDesbloquear"
    end
    

    utils.reproducirSonido("sounds/intros diferences/"..path..pathSelection.."intro",0)
    composer.removeScene( "bloqueo" )
    introIsPlaying=true 
   


end        
 ------------------------------------------------------------------------------------------


function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
        composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene
