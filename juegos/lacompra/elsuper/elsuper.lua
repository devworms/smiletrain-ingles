local composer = require( "composer" )
local scene = composer.newScene()
local carrito = nil
local gameState=require("utilerias.gameState")
--local introIsPlaying
destinoBack = "menus.menuJuego"

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil

-- ------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"

if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end

--crea el objeto r que va a servir para hacer las grabaciones
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)

--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
    audio.dispose(event.handle)
end

---------------------******Funcion obligatoria*********----
function scene:successful()
    --local pronuncia = utils.validarVoz(scene.imagenSeleccionada.id)
    local pronuncia = true
    -------------------********inicio del record********----------------------
    print("presionado")
    utils.reproducirSonido("bip")
    timer.performWithDelay(1000, function()
        r:startRecording()
    end,1)
   
    timer.performWithDelay(3000, function() 
        r:stopRecording()
        local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
                    
        utils.reproducirSonido("bip")
        timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
                local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
            end 
        end,1)
    end,1)

    ----------**********fin de el record*****------------------------------
    if pronuncia then
        timer.performWithDelay(5000, function() 
            composer.removeScene( "bloqueo" ) 
            composer.gotoScene ("juegos.lacompra.elsuper.menuSuperMercado" , { effect = "fade"} )
        end,1)
    else
        transition.to(carrito,{x=scene.posIniX,y=scene.posIniY})  
        utils.reproducirSonido("sounds/principales/intentalo",1)
    end
   
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
    if introIsPlaying == true then
        local audioChannel = audio.stop() 
        introIsPlaying = false
    end

    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
   
    print(event.target.destination)
    return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
    
	local group = self.view

    gameState.success = 0

    local background = display.newImage( group,"images/lacompra/fondo-1.png" )
    background:translate( centerX, centerY )
    
    local topsign = display.newImage( group,"images/top-sign.png" )
    topsign:translate( centerX, centerY/5 )
    
    local carmen = display.newImage( group,"images/lacompra/elsuper/carmen.png" )
    carmen:translate( centerX*1.6, centerY*1.1 )
    
    local super = display.newImage( group,"images/lacompra/elsuper/super.png" )
    super:translate( centerX/3.6, centerY*1.12 )
    
    carrito = display.newImage( group,"images/lacompra/elsuper/car.png" )
    carrito:translate( centerX*1.25, centerY*1.3 )
    carrito.xScale = -1
    carrito.w = carrito.contentWidth
    carrito.h = carrito.contentHeight
    carrito.id = "cart"
    
    super.w=super.contentWidth
    super.h=super.contentHeight
        
    utils.dragging(carrito,super,"juegos.lacompra.elsuper.elsuper", function (resultado) 
        --esto es para quitar el bloqueo 
        if introIsPlaying == true then
            local audioChannel = audio.stop() 
            introIsPlaying = false
        end

        if resultado then
            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x,y= scene.imagenColisionada.y+ scene.imagenColisionada.h/2-35})

            utils.primerSonido("juegos.lacompra.elsuper.elsuper", scene.imagenSeleccionada.id)
            --utils.primerSonido("juegos.lacompra.elsuper.elsuper", "coca")
        else
            utils.reproducirSonido("error")
            transition.to(carrito,{x=scene.posIniX,y=scene.posIniY})  

        end
    end)
        
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "menus.menuJuego"

    local optionsTextMenu = 
    {
        --parent = group,
        text = "",
        width = 800,
        x=centerX,
        y=centerY/3.5,
        align = "center",--required for multi-line and alignment
        font = "gotham",
        fontSize = 32
    }
        
    local inst = display.newText( optionsTextMenu )
    group:insert(inst)
    inst.text = "Drag the cart to the market"
    utils.reproducirSonido("sounds/intros/shopping/intro1")
    --composer.removeScene( "bloqueo" )
    introIsPlaying=true
   
end


-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view

	-- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene