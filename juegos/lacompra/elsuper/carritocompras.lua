local composer = require( "composer" )
local utils = require("utilerias.utils" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
destinoBack = "juegos.lacompra.elsuper.menuSuperMercado"

local productosIzquierda = nil
local productosDerecha=nil

local carrito=""
local productoIzq=nil
local productoDer=nil
local productosIzq=nil
local productosDer=nil

local grupoImagenes = nil

local btnOrigen = nil
-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil 

local function crearProducto(producto, event)
    producto:translate( event.x, event.y )
    producto.w = producto.contentWidth
    producto.h = producto.contentHeight
    
    utils.dragging(producto, carrito, "juegos.lacompra.elsuper.carritocompras",
        function (resultado) 
            if resultado then
                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x,y= scene.imagenColisionada.y})

                utils.primerSonido("juegos.lacompra.elsuper.carritocompras", scene.imagenSeleccionada.id)
                --utils.primerSonido("juegos.lacompra.elsuper.carritocompras","coca")
            else
                if productoIzq ~= nil and productosIzq~=nil then
                   productoIzq:removeSelf()
                   productoIzq = nil
               elseif productoDer ~= nil and productosDer~=nil then
                   productoDer:removeSelf()
                   productoDer = nil
               end

            end
        end)
end

------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"

if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end

--crea el objeto r que va a servir para hacer las grabaciones
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)

--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
    audio.dispose(event.handle)
end

---------------------******Funcion obligatoria*********----
function scene:successful()
    -------------------********inicio del record********----------------------
    print("presionado")
    utils.reproducirSonido("bip")
    timer.performWithDelay(1000, function()
        r:startRecording()
    end,1)
   
    timer.performWithDelay(3000, function() 
        r:stopRecording()
        utils.reproducirSonido("bip")
        timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
                local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame} )
            end 
        end,1)
    end,1)

    ----------**********fin de el record*****------------------------------
    timer.performWithDelay(5500, function() 
        if productosIzq ~=nil and productoIzq~=nil  then
            productoIzq:removeEventListener("touch")
            -- productoIzq=nil
            productosIzq:removeSelf()
            productosIzq=nil
            carrito:toFront()
        elseif productosDer ~=nil and productoDer~=nil  then
            productoDer:removeEventListener("touch")
            -- productoDer=nil
            productosDer:removeSelf()
            productosDer=nil
            carrito:toFront()  
        end
        
        --- juego terminado
        if productosIzq==nil and productosDer==nil then
            -- variable >= no de coincidencias que debe haber
            utils.actualizarPreferenciaJuego(btnOrigen, 'false', 'elsuper')
            row = utils.obtenerPreferencesJuego("elsuper")
            if  row.btn1== 'false' and row.btn2== 'false' and row.btn3== 'false' and row.btn4== 'false' then
                --composer.removeScene("juegos.lacompra.elsuper.carritocompras")
                --composer.gotoScene ("juegos.lacompra.final-lacompra" , { effect = "fade"} )
                timer.performWithDelay(6000, function()
                    utils.ponerGlobos("menus.menuJuego")
                end,1)
            else
                composer.removeScene("juegos.lacompra.elsuper.carritocompras")
                composer.gotoScene ("juegos.lacompra.elsuper.menuSuperMercado" , { effect = "fade"} )
            end
        else
            composer.removeScene( "bloqueo" )
        end
    end,1)

end

-- @return
---Crea las imagenes que debe de llevar el juego a la derecha y a la izquierda
local function configurarProductos()
    if gameState.tipodeSubJuego == 1 then
        productosIzquierda = "calabazas"
        productosDerecha = "peras"
        btnOrigen="btn1"
    elseif gameState.tipodeSubJuego == 2 then
        productosIzquierda = "cacahuates"
        productosDerecha = "chocolates"
        btnOrigen="btn2"
    elseif gameState.tipodeSubJuego == 3 then
        productosIzquierda = "papas"
        productosDerecha = "tortillas"
        btnOrigen="btn3"
    elseif gameState.tipodeSubJuego == 4 then
        productosIzquierda = "cocas"
        productosDerecha = "teas"
        btnOrigen="btn4"
    end
end

--Decide que imagen debe de crearse al momento de que se toque una de las imagenes de los productos de la izquierda o la derecha
--tap es 2 si es derecha y 1 si es izquierda
local function configurarTipoProductoTap(tap)
    local variableProducto=""

    if tap == 1 then -- toque a la izquierda
        if gameState.tipodeSubJuego == 1 then -- si el tipo es 1 y se cliqueo a la izquierda
            variableProducto = "pumpkin"
        elseif gameState.tipodeSubJuego == 2 then
            variableProducto = "peanut"
        elseif gameState.tipodeSubJuego == 3 then
            variableProducto = "pizza"
        elseif gameState.tipodeSubJuego == 4 then
            variableProducto = "coffee"
        end
    else --toque a la derecha
        if gameState.tipodeSubJuego == 1 then
            variableProducto = "pear"    
        elseif gameState.tipodeSubJuego == 2 then
            variableProducto = "candy"
        elseif gameState.tipodeSubJuego == 3 then
            variableProducto = "taco"
        elseif gameState.tipodeSubJuego == 4 then
            variableProducto = "tea"
        end  
    end
    
    return variableProducto
end

local function btnTapIzq(event)
     --esto es para quitar el bloqueo 
    if introIsPlaying == true then
        local audioChannel = audio.stop() 
        introIsPlaying = false
    end

    if event.phase == "began" then
        if productoDer ~=nil and productosDer~=nil then
               productoDer:removeSelf()
               productoDer=nil
        end    

        if productoIzq == nil then
            --print "boton tap"
            local variableProducto = configurarTipoProductoTap(1)
            productoIzq = display.newImage(grupoImagenes,"images/lacompra/elsuper/"..variableProducto..".png" )
            productoIzq.id=variableProducto
            crearProducto(productoIzq,event)
        end
    end

   return true
end

local function btnTapDer(event)
     --esto es para quitar el bloqueo 
    if introIsPlaying == true then
        local audioChannel = audio.stop() 
        introIsPlaying = false
    end

    if event.phase == "began" then
        if productoIzq ~=nil and productosIzq~=nil then
            productoIzq:removeSelf()
            productoIzq=nil
        end

        if productoDer == nil then  
            local variableProducto = configurarTipoProductoTap(2)
            productoDer = display.newImage( grupoImagenes,"images/lacompra/elsuper/"..variableProducto..".png" )
            productoDer.id=variableProducto
            --print ("posicion:"..event.x..","..event.y)
            crearProducto(productoDer,event)
         end 
    end

    return true
end

local function btnTap(event)
     --esto es para quitar el bloqueo 
    if introIsPlaying == true then
        local audioChannel = audio.stop() 
        introIsPlaying = false
    end

    utils.reproducirSonido("boton")
    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    return true
end

-- Called when the scene's view does not exist:
function scene:create( event )
	local group = self.view
    grupoImagenes = group
    configurarProductos()
        
    local background = display.newImage( group,"images/lacompra/elsuper/fondo.png" )
    background:translate( centerX, centerY )
    
    if productosIzquierda~=nil then
        productosIzq = display.newImage( group,"images/lacompra/elsuper/".. productosIzquierda ..".png" )
        productosIzq:translate( centerX*.38, centerY*.85 )
        productosIzq:addEventListener("touch", btnTapIzq)
    
    end

    if productosDerecha~=nil then
        productosDer = display.newImage( group,"images/lacompra/elsuper/"..productosDerecha..".png" )
        productosDer:translate( centerX*1.65, centerY*.85 )
        productosDer:addEventListener("touch", btnTapDer)
    end
        
    carrito = display.newImage( group,"images/lacompra/elsuper/car.png" )
    carrito:translate( centerX-20, centerY*1.4 )
    
    carrito.w = carrito.contentWidth
    carrito.h = carrito.contentHeight
    -- Asignacion de eventos a los estantes derecha e izquierda
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = "juegos.lacompra.elsuper.menuSuperMercado"
    
    utils.reproducirSonido("sounds/intros/shopping/intro3", 0)
    composer.removeScene( "bloqueo" )
    introIsPlaying=true  
end

-- Called immediately after scene has moved onscreen:
function scene:show( event )
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	local group = self.view

end

-- Called when scene is about to move offscreen:
function scene:hide( event )
	local group = self.view
	
	composer.removeScene( composer.getSceneName( "current" ) )
	
	--Runtime:removeEventListener("enterFrame", update)
	-- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end

-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
	local group = self.view
    grupoImagenes:removeSelf()
    grupoImagenes=nil
        
       -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	-- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene

