local composer = require( "composer" )
local gameState=require("utilerias.gameState")
local scene = composer.newScene()
--local introIsPlaying
 
  gameState.success = 0
  
  if gameState.tipodejuego == 5 then
    destinoBack = "menus.menuJuego"
  else
    destinoBack = "menus.menuDesbloquear"
  end
  
    local pres=false
    local contadordevueltas=0
    local velocidad=5
    local objeto
    local para=0
    local paravelo=0
    local posanterior=0
    local objetosSalidos={}
    local numeroDeGiros=0
    local ruleta
    local bandera=false
    local aleatorio

    
local function btnTapJugar(event)
    --esto es para quitar el bloqueo 
    if introIsPlaying == true then
    local audioChannel = audio.stop() 
      introIsPlaying = false
    end

    utils.reproducirSonido("sounds/popi/jugar/Ruleta/ruleta")
    
    introIsPlaying=true 

composer.showOverlay( "bloqueo" ,{ isModal = true } )
  
   
   pres=true
   aleatorio=math.random(1, 8)--Escoge el objeto aleatorio que caera en la ruleta
   contadordevueltas=0
   velocidad=5
   posanterior= ruleta.rotation
   para=0
   paravelo=0
end

function gira()
    
    if pres== true then
        ruleta.rotation=ruleta.rotation +velocidad
        contadordevueltas=contadordevueltas+velocidad
       
        if aleatorio==1 then -- Pone el objeto que cayo asi como su grados de rotacion y su velocidad de parada
            objeto= "tidy" -- revisar estas palabras por que son las que estan en la imagen pero no en el word
            para=22.5
            paravelo=2
        elseif aleatorio==2 then
             objeto= "paws"
              para=67.5
              paravelo=2
        elseif aleatorio==3 then
            objeto= "fresbee"
             para=112.5
             paravelo=2
        elseif aleatorio==4 then
            objeto= "park"
             para=157.5
             paravelo=2.5
        elseif aleatorio==5 then
            objeto= "bowl"
             para=202.5
             paravelo=2.5
        elseif aleatorio==6 then
            objeto= "play"
             para=247.5
             paravelo=3
        elseif aleatorio==7 then
            objeto= "ball"
             para=292.5
             paravelo=3
        elseif aleatorio==8 then
            objeto= "bone"
             para=337.5
             paravelo=3
            
        end
        for i= 0,posanterior do -- Resta la posicion anterior del objeto que cayo para que estos concuerden
            if para<=0 then
                para=360
            end
            para=para-1
        end
        --print("Objeto que cayo "..objeto)
        --print("Valor Para "..para)
        --print("Valor Pos anterior"..posanterior)
    end

    if contadordevueltas>=360 and contadordevueltas<360*1 then--a lenta la ruleta
         velocidad= paravelo
    elseif contadordevueltas>=360*2 + para  then-- Detiene la ruleta
        pres=false
        
        for index, objetodelista in ipairs(objetosSalidos) do --pseudocode
            print("Objetos en la lista "..objetodelista)
            if objetodelista==objeto then

            bandera=true
            end
        end
 
      
       if bandera==true then
           bandera=false
           utils.reproducirSonido("error",1)
            
       else
           numeroDeGiros=numeroDeGiros+1
           utils.primerSonido("juegos.popi.ruleta",objeto)
           --utils.primerSonido("juegos.popi.ruleta","coca")
       end
       
        contadordevueltas=0
    end
end

local function btnTap(event)
    --esto es para quitar el bloqueo 
    if introIsPlaying == true then
    local audioChannel = audio.stop() 
   introIsPlaying = false
    end
    utils.reproducirSonido("boton")
   composer.gotoScene ( event.target.destination, { effect = "fade"} )
   print(event.target.destination)
   return true
end
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"
if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end
--crea el objeto r que va a servir para hacer las grabaciones
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)
--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
   
    audio.dispose(event.handle)

 --   utils.reproducirSonido("sounds/principales/gana",0)
    
    introIsPlaying=true
    objetosSalidos[numeroDeGiros]=objeto

                -- timer.performWithDelay(200, function()
                          
        --composer.removeScene( "bloqueo" )
         gameState.success = gameState.success+1
                
                -- variable >= no de coincidencias que debe haber
            if gameState.success >= 6 then
                utils.ponerGlobos(destinoBack)
                    
                
                
               gameState.success = 0
           else
            composer.removeScene( "bloqueo" )
            end
           -- end,1)
end

function scene:successful() 
    
        print("presionado successful")
utils.reproducirSonido("bip")
timer.performWithDelay(1000, function()
   r:startRecording()

   timer.performWithDelay(3000, function() 
        r:stopRecording()
        utils.reproducirSonido("bip")
        timer.performWithDelay(1000, function()
   
                    local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
                    
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
            local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
        audio.play( playbackSoundHandle, { onComplete= llamame} )
              end     
                end,1)
        
                    
            end,1)

   end,1)
   
    
    
end

function scene:create( event )
     local group = self.view
      local background = display.newImage( group,path.."ruleta/popi-ruleta-fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()
     
    ruleta=display.newImage(group,path.."ruleta/ruleta.png")
    ruleta:translate( centerX+(centerX/3), centerY-(centerY/6) )
    ruleta.rotation=0
    local flecha=display.newImage(group, "images/popi/ruleta/indicador-ruleta.png")
    flecha:translate( centerX+(centerX/3), centerY-(centerY/3) )
    flecha:toFront()
    
    local btnJugar=  display.newImage(group,"images/menu/boton-play-ruleta.png")
    btnJugar:translate( (centerX/9.5)*4, centerY+((centerY/5)*2.32) )
    btnJugar:addEventListener("tap", btnTapJugar)
    
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
    btnRegresar.destination = destinoBack    
     
    utils.reproducirSonido("sounds/popi/jugar/Ruleta/s1") 
    -- composer.removeScene( "bloqueo" )
    introIsPlaying=true
     Runtime:addEventListener("enterFrame", gira )
end



function scene:show( event )
	local group = self.view 
        
end
function scene:hide( event )
	local group = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
        
         composer.removeScene( composer.getSceneName( "current" ) )
	
end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (group)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local group = self.view
	
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene
