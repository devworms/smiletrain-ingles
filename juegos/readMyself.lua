local composer = require( "composer" )
local scene = composer.newScene()
local gameState=require("utilerias.gameState")
local lfs = require("lfs") --sistema de archivos
local  destino
local textLoad
local banderaPop = false
local codePaciente = ""
local dialogText
local btnVideos 
local totalEscenas--Numero total de imagenes
local array ={}--Array que contiene la direccion de ubicacion de imagenes
local m=0--Variable boto
local bandera= false
local btnAntes--Variable boton Antes
local btnNext--Variable boton Next
local btnJ1--Variable boton Play
local musicafondo
local myImage--Variable Imagen de las Escenas
local widget = require( "widget" )
local botonPalabra = {}
---------------------------------------------cuentosId es donde van a ir las palabras especiales, se acomodan por grupos por pagina cada {}
----------------------------------- cada {} es una pagina
---------------------------------- es el mismo caso con posX y posY
---------------------------------los numeros es tal cual el de los cuentos
---------------------------------las palabras estan ligadas a los archivos de sonidos,
local cuentosId ={}
local cuentosPosX ={}
local cuentosPosY ={}
cuentosId[1]={{}--1 LA COMPRA
,{"would","talk","carmen","did"},--2
{"father","take","store","buy","groceries"},--3
{"carmen", "took", "the", "bag", "left"},--4
{"she","very","thinking", "buy","eat"},--5
{"watermelon","i","no","know"},--6
{"delicious","things","explosive","sounds","kah","pah"},--7
{"practice"},--8
{"grocery","store","got","coca","coca"},--9
{"big", "pumpkin","some","kale","two"},--10
{"potato","chips","bag","of","peanuts"},--11
{"big", "pears","and","chocolate","candy"},--12
{"house"},--13
{"very","hungry","brought","dinner"},--14
{"carmen", "told", "brought"},--cambiar por bought--15
{"and","pie"},--16
{"chocolate"},--17
{"peanuts"},--18
{"pumpkin"}--19

}
cuentosId[2]={{},--1
{"sara","leaving","tomorrow","is","day"},--2
{"she","pack","up","everything","stray"},--3
{"pack", "shirt","toothbrush","too"},--4
{"things","select","list","just","grew"},--5
{"sara","trip","and","tomorrow","the"},--6
{"has","to","there","is","time"},--7
{"need","shirt", "and","toothbrush"},--8
{"so","things","select","just","grew"},--9
{"cozy","sweater","scarf","socks","sneakers"},--10
{"must","silk","tshirt","that","blue"},--11
{"remember","two","brushes","teeth","for"},--12
{"put","them","arranging","with","care"},--13
{"feels","sleepy","packing","items","their","place"},--14
{"sits","down","chair","rest","after","closing","tight"},--15
{"wakes","shes","ready","house","behind"},--16
{"can","find"},--17
{},--18
{"miss","smilling","face"}--19
}
cuentosId[3] ={{},--1
{"pail","pull","penny","pull","pull"},--2
{"bunny"},--3
{"pail","pull","benny","pull","pull"},--4
{"bilby"},--5
{"pail","pull","penny","pull","pull"},--6
{"baboon"},--7
{"pail","pull","benny","pull","pull"},--8
{"boar"},--9
{"pail","pull","penny","pull","pull"},--10
{"piranha"},--11
{"pail","pull","benny","pull","pull"},--12
{"bull"},--13
{"pail","pull","penny","pull","pull"},--14
{"boa"},--15
{"pail","pull","benny","pull","pull"},--16
{"puma"},--17
{"pail","pull","penny","pull","pull"},--18
{"polar bear"},--19
{"pail","pull","benny","pull","pull"},--20
{"brown bear"},--21
{"piled","bilby","boar","bull","brown bear"},--22
{"up","bunny","baboon","piranha","polar bear"}--23
}
cuentosId[4]={{},--1
{"make","can", "hear","explosion","ear"},--2
{"puff","face","release","it","pop","clear"},--3
{"sounds","ka","pa","ta"},--4
{"lets","practice","sounds"},--5
{"ka","pa","ta","pop"},--6
{"make","strong","speak","palate","pull"},--7
{"so","speak","strongly","push","pop"},--8
{"ka","pop"},--9
{"pa","pop"},--10
{"ta", "pop"},--11
{"ko","pop"},--12
{"po","pop"},--13
{"to","pop"}--14
}
cuentosId[5]={{},--1
{"pet","pepe"},--2
{"color","purple","spots"},--3
{"plays","very","happy"},--4
{"balls","frisbees"},--5
{"is","the","grass","likes","dirty"},--6
{"pepe","messy","always","has","cleaned"},--7
{"have","plan","make","tidy","follow"},--8
{"pat","his","head","with","soap"},--9
{"two", "i","bathe","his","chest"},--10
{"three", "i","scrub","his","tummy"},--11
{"four", "i","clean","his","paws"},--12
{"five","wipe","his","long","tail"},--13
{"six","i","bring","the","towel"},--14
{"now","clean","tidy", "purple","puppy"},--15
{"eat","give","plate","kibble"},--16
{"rest"},--17
{"dont","forget","favorite" ,"green", "pillow"},--18
{"covered","blanket","say"},
{"sweet","dreams","tomorrow","day","play"}
}
cuentosId[6]={{},--0
{"dainty","deer", "tanya", "tall", "danny"},--1
{"had", "told","do","hot","day"},--2
{"tanya","danny", "idea"},--3
{"want", "said", "eat", "too", "water"},--4
{"went", "wide", "window", "time", "dinner"},--5
{"hot","tuna", "wilted", "tomato", "tea"},--7
{"traded","tuna","ate","tomato","ate"},--8
{"tired", "lied", "down", "want", "down"},--9
{"dawned", "dash", "wanted", "dart", "around"},--10
{"didnt","want","to", "wait", "down"},--11
{"out","trot","town", "lowered"},--12
{"wanted","take","water","into","tin"},--13
{"water","around","made","wet", "danny"},--14
{"want", "out", "uttered", "danny", "waited"},--15
{"tilted","heard","want","out","yelled"},--16
{"named","tianna","heard","hauled","out"},--17
{"wet","tianna", "told","headed"},--18
{"hut","around","turned","tent","toddled"},--19
{"trotted","dashed","waddled","day","night"},--20
{"end", "day", "returned", "returned","worried"},--21
{"tanya","didnt","tell","and","dad"},--22
{"wet","tired","returned","hand","towel"},--23
{"reunited","told","dad","tale","mad"},--24
{"ordered","danny","not","to","it"},--25
{"dialed","tianna","to","tell","water"},--26
{"day","dad","delighted","to","reunited"}--27
}
cuentosId[7]={{},--1
 {"cal","gilly","eager", "cal", "kooky", "gilly", "make"},--2
 {"cal", "kooky", "gilly", "like", "coloring"},--3
 {"winking", "camel","groggy", "gorilla", "canoe"},--4
 {"giddy", "cougar","making","icky","cake"},--5
 {"gross","iguana","crying","clunky","car"},--6
 {"eager","color","angry","chameleon","glaring"},--7
 {"color","keen","gecko","kayak","lake"},--8
 {"grey","koala","calling","caring","eagle"},--9
 {"cal","gilly","color","growling","raccoon"},--10
 {"color", "calm","yak","cooking","kale"},--11
 {"eager","giggling","monkey","licking","cone"},--12
 {"cal", "gilly","color","kicking","kangaroo"},--13
 {"kooky","cool","crane","cane","creek"},--14
 {"color","cranky","killerwhale","making","cookie"},--15
 {"make","kicky","cow","leaking","milk"},--16
 {"eager","cal","gilly","call","look"},--17
 {"cal","gilly","call","clean","grimy"},--18
 {"eager","cal","kooky","gilly","groan"},--20
 {"eager","cal","kooky","gilly","hungry"},--21
 {"eager","call","can","make","cake"},--22
 {"cal", "kooky","gilly","call","clean"},--23
 {"winking", "camel","groggy","gorilla","canoe"},--24
 {"giddy","cougar","making","icky","cake"},--25
 {"gross","iguana","crying","green", "car"},--26
 {"gilly","clean","angry","chameleon","glaring"},--27
 {"clean", "keen","gecko","kayak","lake"},--28
 {"grey", "koala","calling","caring","eagle"},--29
 {"cal","gilly","clean","growling","raccoon"},--30
 {"eager","calm","yak","cooking","kale"},--31
 {"clean","giggling","monkey","licking","cone"},--32
 {"eager", "kooky","clean","kicking","kangaroo"},--33
 {"clean","cool","crane","cane","creek"},--34
 {"clean","cranky","killerwhale","making","cookie"},--35
 {"clean","kicky","cow","leaking","milk"},--36
 {"gleaming", "eager","kooky","gilly","hungry"},--37
 {"cake", "oclock"}--38
}
cuentosId[8]={
{},--1
{"sara","zane","zoom","sunrise"},--2
{"sees","celery","scissors","say","slice"},--3
{"salami","zanehs","scissors","slice"},--4
{"sees","salmon","sarahs","scissors","slice"},--5
{"sees","raisins","zanehs","say","slice"},--6
{"salsa","sarahs","scissors","say","slice"},--7
{"zane","sees","sole","scissors","slice"},--8
{"sara","sees","roses","senses","smell"},--9
{"zinnias","zanehs","senses","say","smell"},--10
{"sees","irises","sarahs","senses","smell"},--11
{"sara","zane","sing","zoom","sunrise"}--12
}
cuentosPosX[1]={{},--1
{312,557,464,618},--2
{250,558,210,390,635},--3
{155,307,403,658,819},--4
{168,387,715,525,653},--5
{366,456,622,482},--6
{338,514,800,334,554,682},--7
{467},--8
{325,478,747,288,394},--9
{434,506,275,386,483},--10
{504,635,277,357,410},--11
{563,634,317,397,587},--12
{696},--13
{466,558,422,760},--14
{134,290,746},--15
{568,680},--16
{534},--17
{410},--18
{402}--19

}
cuentosPosY[1]={{},--1
{434,432,464,466},--2
{429,430,466,461,464},--3
{ 30, 30, 32, 34, 32},--4
{437,440,438,470,474},--5
{438,472,437,472},--6
{437,440,445,477,470,472},--7
{442},--8
{ 18, 16, 16, 43, 50},--9
{ 10, 18, 48, 43, 45},--10
{ 14, 14, 45, 46, 46},--11
{ 40, 48, 80, 80, 80},--12
{ 11},--13
{ 48, 43, 77, 75},--14
{13,13,10},--15
{ 50, 50},--16
{ 48},--17
{ 51},--18
{ 13}--19
}
cuentosPosX[2]={{},--1
{254,411,333,525,635},--2
{182,386,483,542,592},--3
{474,317,491,702},--4
{459,635,346,411,491},--5
{256,645,723,333,565},--6
{264,336,765,354,451},--7
{323,314,411,490},--8
{288,461,635,411,670},--9
{317,414,680,334,530},--10
{288,581,653,454,634},--11
{342,610,691,347,611},--12
{291,365,301,546,637},--13
{365,520,650,341,496,595},--14
{317,392,581,733,235,334,549},--15
{259,608,709,483,605},--16
{592,667},--17
{},
{397,557,702}--18
}
cuentosPosY[2]={{},--1
{ 22, 21, 58, 54, 54},--2
{ 22, 27, 29,29, 61},--3
{ 27, 59, 54, 54},--4
{ 24, 29, 54, 53, 59},--5
{ 21, 22, 29, 58, 54},--6
{ 21, 24, 22, 54, 54},--7
{ 26, 54, 59, 54},--8
{ 22, 24, 27, 54, 58},--9
{ 29, 26, 29, 61, 61},--10
{ 24, 27, 24, 58, 56},--11
{ 29, 24, 19, 58, 56},--12
{ 29, 22, 58, 61, 62},--13
{ 21, 29, 27, 53, 56, 56},--14
{ 26, 26, 27, 27, 59, 56, 56},--15
{ 26, 26, 30, 54, 56},--16
{ 62, 54},--17
{},
{ 61, 58, 53}--18
}
cuentosPosX[3]={{},--1
{386,349,458,613,360},--2
{478},--3
{384,349,454,360},--4
{477},--5
{371,334,445,346},--6
{448},--7
{371,334,443,346},--8
{480},--9
{371,333,443,346},--10
{450},--11
{370,334,442,347},--12
{490},--13
{370,334,445,347},--14
{490},--15
{371,333,443,344},--16
{470},--17
{371,334,445,346},--18
{418},--19
{371,333,440,346},--20
{405},--21
{182, 72, 72, 72, 72 },--22
{514,282,280,282,376 }--23
}
cuentosPosY[3]={{},--1
{ 30, 64, 64, 72, 99},--2
{ 45},--3
{ 46, 80, 80,114},--4
{ 42},--5
{ 43, 78, 77,112},--6
{ 43},--7
{ 42, 75, 77,112},--8
{ 43},--9
{ 43, 77, 77,112},--10
{ 50},--11
{ 43, 78, 77,114},--12
{ 42},--13
{ 43, 77, 77,112},--14
{ 43},--15
{ 43, 77, 77,110},--16
{ 50},--17
{ 43, 77, 77,112},--18
{ 50, 50, 48},--19
{ 43, 74, 77,114},--20
{ 43, 51, 43},--21
{ 53, 83,112,147,182, 181, 182},--22
{ 19, 45, 80,120,154, 147, 154}--23
}
cuentosPosX[4]={{},--1
{542,246,320,472,790},--2
{328,502,598,739,344,581},--3
{333,539,592,723},--4
{282,378,606},--5
{302,410,520,614},--6
{174,384,779,398,597},--7
{142,363,480,698,645},--8
{336,600},--9
{328,608},--10
{354,586},--11
{334,603},--12
{323,616},--13
{350,589}--14
}
cuentosPosY[4]={{},--1
{430,466,459,464,466},--2
{432,429,432,429,461,462},--3
{434,429,434,429},--4
{429,432,434},--5
{392,397,394,394},--6
{422,427,429,461,458},--7
{422,427,427,429,453},--8
{429,430},--9
{432,429},--10
{430,430},--11
{427,429},--12
{430,430},--13
{432,430}}--14
cuentosPosX[5]={{},--1      POPPI
{374,461},--2
{304,446,456},--3
{285,635,730},--4
{437,613},--5
{338,422,493,656,464},--6
{170,366,554,688,435},--7
{179,310,451,662,755},--8
{312,384,446,547,638},--9
{467,352,379,494,562},--10
{456,344,368,482,544},--11
{467,365,386,496,558},--12
{467,358,459,523,616},--13
{480,357,379,486,558},--14
{155,411,531,616,746},--15
{379,525,726,451},--16
{555},{168,277,466,--17
622,739},--18
{190,512,566},--19
{133,258,514,403,533}--20
}
cuentosPosY[5]={{},--1 POPPI
{ 37, 64},--2
{ 35, 37, 67},--3
{ 32, 37, 29},--4
{ 29, 29},--5
{ 30, 32, 37, 30, 64},--6
{ 30, 35, 37, 30, 66},--7
{ 29, 35, 37, 32, 30},--8
{ 69, 62, 61, 66, 69},--9
{ 29, 70, 62, 62, 66},--10
{ 32, 62, 69, 64, 66},--11
{ 30, 62, 64, 64, 70},--12
{ 30, 62, 61, 64, 62},--13
{ 32, 64, 62, 64, 64},--14
{ 30, 32, 30, 37, 37},--15
{ 35, 37, 35, 62},--16
{ 67},--17
{ 32, 30, 30, 35, 32},--18
{ 30, 30, 67},--19
{ 30, 34, 30, 64, 64}--20
}
cuentosPosX[6]={{},--1
{275,397,613,138,411},--2
{366,440,454,597,666},--3
{256,440,686},--4
{259,205,478,541,819},--5
{437,643,738,182,392},--6
{293,363,701,819,595},--7
{387,781,189,285,874},--8
{304,531,603,475,670},--9
{318,640,333,523,605},--10
{264,379,470,517,704},--11
{390,507,784,507},--12
{283,469,552,666,773},--13
{424,586,376,517,590},--14
{320,416,635,309,426},--15
{312,198,486,581,798},--16
{262,392,515,776,512},--17
{256,558,341,571},--18
{442,741,134,512,666},--19
{405,613,314,525,686},--20
{214,403,603, 390, 453},--21 
{128,240,594,757,830},--22
{426,573,787,338,509},--23
{ 74,370,619,880,640},--24
{346,498,613,686,790},--25
{389,509,630,682,470},--26
{464,714,109,285,331}--27
}
cuentosPosY[6]={{},--1
{414,416,413,446,445},--2
{429,432,466,462,464},--3
{419,418,418},--4
{419,453,451,446,451},--5
{426,426,421,453,454},--6
{ 18, 21, 24, 19, 53},--7
{ 21, 19, 54, 53, 56},--8
{426,427,430,464,464},--9
{432,426,462,462,467},--10
{466,470,462,464,462},--11
{ 38, 35, 37, 67},--12
{ 35, 35, 40, 32, 34},--13
{ 38, 38, 72, 74, 61},--14
{ 37, 40, 38, 64, 70},--15
{ 35, 69, 69, 74, 72},--16
{442,440,438,438,475},--17
{446,438,474,472},--18
{435,443,472,474,470},--19
{440,440,475,474,474},--20
{394, 390, 394, 424,458},--21
{442,443,438,445,440},--22
{421,419,421,448,450},--23
{ 24, 26, 24, 26, 61},--24
{ 24, 21, 26, 19, 21},--25
{ 19, 24, 26, 22,80},--26
{418,418,454,456,454}--27
}
cuentosPosX[7]={{},--1
{350,558,274,392,542,667,392},--2
{392,541,667,395,469},--3
{ 99,256,488,634,838},--4
{208,326,467,669,752},--5
{141,253,386,605,851},--6
{222,710,304,422,635},--7
{710,235,333,533,739},--8
{227,322,429,595,722},--9
{339,618,710,365,536},--10
{710,318,418,491,653},--11
{224,237,400,558,725},--12
{339,616,710,366,510},--13
{490,216,306,542,730},--14
{709,194,350,554,731},--15
--{"color","cranky","killerwhale","making","cookie"},--16
{707,       317,    420,            509,    654},--16
{234,352,624,718,286},--17
{250, 528, 816, 318, 506},--18 
{211,326,480,603,699},--19
{242,360,509,634,427},--20
{234,720,370,520,666},--21
{251,402,526,818,480},--22
{392,546,378,523,486},--23
{384,501,640,448,530},--24
{397,510,645,539,656},--25
{666,269,432,554,438},--26
{294,437,536,362,571},--27
{398,496,603,390,517},--28
{389,664,272,411,587},--29
{269,421,517,594,466},--30
{282,           426,       592,     390,    561},--31
-- {"clean","giggling","monkey","licking","cone"},--32
{270,539,272,418,560},--32
{234,376,462,696,498},--33
{266,405,560,352,533},--34
--{"clean","cranky","killerwhale","making","cookie"},--35
{275,414,525,608,464},--35
{570,386,298,421,587},--36
{373,490}--37
}
cuentosPosY[7]={{},--1
{ 35, 35, 78, 80, 82, 85,115},--2
{ 27, 24, 27, 58, 64},--3
{ 58, 58, 58, 58, 58},--4
{ 66, 62, 62, 58, 64},--5
{ 66, 58, 62, 64, 66},--6
{ 24, 30, 62, 62, 62},--7
{ 30, 56, 64, 56, 58},--8
{ 66, 59, 61, 62, 61},--9
{ 24, 26, 29, 61, 58},--10
{ 22, 53, 53, 50, 48},--11
{ 24, 66, 62, 56, 64},--12
{ 24, 27, 30, 56, 58},--13
{ 21, 59, 59, 61, 58},--14
{ 29, 59, 61, 61, 61},--15
--{"color","cranky","killerwhale","making","cookie"},--16
{ 29,        56,        64,         53,     62},--16
{427,429,429,435,461},--17
{400, 398, 402, 430, 434},--18
{ 91, 94, 90, 91, 99},--19
{ 11, 16, 14, 18, 48},--20
{ 13, 19, 53, 51, 51},--21
{ 14, 14, 14, 16, 48},--22
{ 75, 77,112,110,146},--23
{ 75, 78, 77,107,110},--24
{ 83, 72, 78,112,112},--25
{ 42, 80, 78, 78,109},--26
{ 75, 74, 80,104,104},--27
{ 74, 72, 75,114,110},--28
{ 43, 42, 75, 77, 77},--29
{ 40, 80, 77, 78,106},--30
{ 78, 78, 75, 110,107,110},--31
-- {"clean","giggling","monkey","licking","cone"},--32
{ 38, 42, 74, 72, 74},--32
{ 72, 77, 75, 78,107},--33
{ 74, 75,75,112,110},--34
--{"clean","cranky","killerwhale","making","cookie"},--35
{ 74, 72, 78, 74,109},--35
{ 26, 53, 86, 86, 86},--36
{ 19, 19}--37
}    
cuentosPosX[8]={{},--1
{368,539,352,498},--2
{485,576,496,651,379},--3
{560,371,501,379},--4
{475,568,374,496,379},--5
{488,578,370,656,378},--6
{587,374,496,651,379},--7
{410,510,602,501,374},--8
{400,493,586,507,357},--9
{574,382,512,643,357},--10
{493,586,386,506,358},--11
{355,550,653,352,499}--12
}
cuentosPosY[8]={{},--1
{ 37, 35, 70,107},--2
{ 42, 40, 72, 75,101},--3
{ 43, 69, 77,101},--4
{ 42, 35, 70, 74,102},--5
{ 42, 42, 67, 75,104},--6
{ 43, 69, 75, 74,102},--7
{ 35, 40, 38, 74, 99},--8
{ 35, 40, 38, 74, 99},--9
{ 40, 67, 77, 74,101},--10
{ 43, 37, 69, 75,102},--11
{ 37, 35, 46, 69,107}--12
}
   

destinoBack = "menus.menuHistory"

local  function dameArray( totalVideos, destino )--Crea Array de ubicaciones de imagenes
    for i=1, totalEscenas do
        array[i]=destino.."Esc"..i..".png"--Coloca el nombre de la imagen, cada imagen esta como Esc1..Esc2 etc
        print(array[i])
    end
    return array
end 
local function onMouseEvent( event )

    if (event.isPrimaryButtonDown)then
 
    print(math.round(event.x)..","..math.round(event.y))
 
    end
 
end


local function mostrarImagen( url )--Coloca Imagen
    myImage = display.newImage( url )--Carga la Imagen de la Url
    print(display.contentWidth)
    print(display.contentHeight)
    myImage:scale(1, .9)--Escala la Imagen
    myImage.x = display.contentWidth/2--coloca al centro del dispositivo x
    myImage.y = myImage.contentHeight/2--coloca al centro del dispositivo x
   
end

local function eliminaBotones()
    --------------------------------- cuando se usa # te da la longitud de el arreglo ;)
    
    for i=1, #botonPalabra do
            if botonPalabra ~= nil then
                 botonPalabra[i]:removeSelf()
                botonPalabra[i] = nil
        end
    end
end
local function btnTap(event)--Boton Regresar

	local audioChannel = audio.stop()
            
    if (musicafondochanel ~= nil) then
        audio.stop(musicafondochanel)  
        audio.dispose(musicafondochanel)
        musicafondochanel=nil
    end
    
    utils.tipoImagen()
    utils.reproducirSonido("boton")

    composer.gotoScene ( event.target.destination, { effect = "fade"} )
    print(event.target.destination)
    
    return true
end 
local function handleButtonEvent( event )
    --------------------------se tiene que detener el audio para que no se reproduzca mas de un sonido a la vez 
    
    --local sonidoPalabra = audio.loadSound("sounds/sounds/objetos/"..event.target.id..".mp3", { duration=3000 } )
        if ( event.phase == "ended" ) then
        local sonidoPalabra = audio.loadSound("sounds/sounds/objetos/"..event.target.id..".mp3", { duration=3000 } )
        audio.play(sonidoPalabra)
        print( "Button was pressed and released ".. event.target.id)

        end
   
end
---------------------------------------------------------- ahora creo un arreglo de botones por pagina
---------------------------------------------------------- indice es el numero de cuento, pagina es la variable m que esta pro ahi
---------------------------------------------------------- m esta ligada al cambio de pagina
local  function crearBotones(indice,pagina)
    local tamaSonido
    print( "pagina "..pagina )
    for i=1, #cuentosId[indice][pagina] do
        if botonPalabra ~= nil then
            if cuentosPosX[indice][pagina][i] ~= nil and cuentosPosY[indice][pagina][i] ~=nil then
                tamaSonido=string.len(cuentosId[indice][pagina][i] )
                if (tamaSonido<=1)then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 20, 30 )
                elseif(tamaSonido>1 and tamaSonido<=2)then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 30, 30 ) 
                elseif(tamaSonido>2 and tamaSonido<=4)then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 70, 30 ) 
                elseif(tamaSonido>4 and tamaSonido<=8) then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 125, 30 ) 
                elseif(tamaSonido>8 and tamaSonido<=12) then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 190, 30 ) 
                 elseif(tamaSonido>12 and tamaSonido<20) then
                    botonPalabra[i] = display.newRect ( cuentosPosX[indice][pagina][i], cuentosPosY[indice][pagina][i], 250, 30 ) 
                
                end
                botonPalabra[i]:addEventListener("touch", handleButtonEvent)
                botonPalabra[i].id = cuentosId[indice][pagina][i]
                botonPalabra[i].anchorX= 0
                botonPalabra[i].anchorY= 0
                botonPalabra[i]:setFillColor(1,1,1,0.01)
                --botonPalabra[i].isVisible = false
                --botonPalabra[i].isEnable = true
            end
        
        
        end
    end
end
--------------------------------------con esta funcion limpiamos los botones y hacemos que no se queden por ahi 

local function btnPlay(event) --Boton Play
    if btnJ1 ~=nil then
    btnJ1:removeSelf()--Elimina el boton play
    end
    m=1--Contador para las Escenas
    audio.stop()
    musicafondochanel= audio.play( musicafondo,{ channel=0, loops=-1} )
    audio.setVolume( 0.4 )
    dameArray(totalVideos, destino)
    mostrarImagen(array[m])--LLama funcion mostrar imagenes
    botonesEscenas()--Muestra los botones de Antes y Despues
    crearBotones(gameState.tipodejuego,m)
                
end
--[[local function onMouseEvent( event )
    -- Print the mouse cursor's current position to the log.
    local message = "Mouse Position = (" .. tostring(event.x) .. "," .. tostring(event.y) .. ")"
    --print( message )
end]]

local function gettipoJuego()
    if  gameState.tipodejuego == 1 then -- la compra
        destino="cuentosMyself/laCompra/" 
        totalEscenas=19
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif  gameState.tipodejuego == 2 then -- el viaje
        destino="cuentosMyself/elViaje/" 
        totalEscenas=19-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif  gameState.tipodejuego == 3 then -- penny
        destino="cuentosMyself/penny/"
        totalEscenas=23-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif  gameState.tipodejuego == 4 then -- los sonidos
        destino="cuentosMyself/losSonidos/"
        totalEscenas=14-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif  gameState.tipodejuego == 5 then -- popi
        destino="cuentosMyself/popi/"
        totalEscenas=20-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif gameState.tipodejuego == 6 then  -- danny
        destino="cuentosMyself/danny/"
        totalEscenas=27-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif gameState.tipodejuego == 7 then  -- cally
        destino="cuentosMyself/cally/"
        totalEscenas=37-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    elseif gameState.tipodejuego == 8 then  -- zara
        destino="cuentosMyself/zara/"
        totalEscenas=12-- total de Escenas/imagenes en la carpeta
        print("ruta:",destino)
        print("Total de Escenas: ",totalEscenas)
    end    
end 
local  function antes( event )--Boton antes

print("antes m= "..m)

        if ( event.phase == "ended" ) then
             display.remove(myImage)--Remueve la Imagen Actual
             eliminaBotones()-------limpia los botones
            if(m==2)then
            	m=m-1
                btnAntes.isVisible=false--Oculta el boton antes
                btnAntes.isEnabled=false
                mostrarImagen(array[1])--Funcion muestra la Nueva imagen
                crearBotones(gameState.tipodejuego,1)
                print(array[m])
            elseif (m>2 and m<=totalEscenas) then
                m=m-1
                btnNext.isVisible=true--Mostramos el boton next
                btnNext.isEnabled=true
                btnAntes.isVisible=true--Oculta el boton antes
                btnAntes.isEnabled=true
                mostrarImagen(array[m])--Funcion muestra la Nueva imagen
                crearBotones(gameState.tipodejuego,m)
                print(array[m])
                
             end
     end
end


local  function next( event )
	print("next m= "..m)
        if ( event.phase == "began" ) then
            print("hola")
        elseif ( event.phase == "ended" ) then
            display.remove(myImage)--Eliminamos la imagen Actual
            eliminaBotones()-------limpia los botones
            if(m>=1 and m<totalEscenas) then
                m=m+1
                mostrarImagen(array[m])--Funcion muestra la Nueva imagen
                crearBotones(gameState.tipodejuego,m)
                print(array[m])
                if(totalEscenas==m) then
                        btnNext.isVisible=false--Oculta el boton despues
                        btnNext.isEnabled=false
                else
                    btnAntes.isVisible=true--Mostramos el boton Antes
                    btnAntes.isEnabled=true
                end
            end

            
            end
       
        
end

-- Called when the scene's view does not exist:
function scene:create( event )
    local group = self.view
        
    gettipoJuego()
    musicafondo =audio.loadStream(destino.."sonido.mp3" )
    
    function botonesEscenas( )--Mostrar boton Next y Antes
        btnAntes = display.newImage(group,"videos/Utilerias/btnAntes.png")
        btnAntes:translate( centerX*1.3, centerY*1.9)
        btnAntes.isVisible=false
        btnAntes.isEnabled=false
        btnAntes: addEventListener("touch", antes)
        btnAntes:scale(.55,.55)
        
        btnNext = display.newImage(group,"videos/Utilerias/btnNexts.png")
        btnNext:translate(centerX*1.7, centerY*1.9)
        btnNext: addEventListener("touch", next)
        btnNext:scale(.55,.55)
        btnNext.isVisible=true
        btnNext.isEnabled=true

        Runtime:addEventListener( "mouse", onMouseEvent )
    end

    -- musicafondo =audio.loadStream(destino.."sonido.mp3" )
        local background = display.newImage( group,path.."fondo-1.png" )--fondo video
        background:translate( centerX, centerY )
        btnJ1 = display.newImage( group,"images/menu/play.png" )
        btnJ1:translate( centerX, centerY )
        btnJ1.isVisible=true
        btnJ1.isEnabled=true
        btnJ1:addEventListener("tap", btnPlay)
         
       
        local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
        btnRegresar:translate( centerX/4, centerY*1.9 )
        btnRegresar:addEventListener("tap", btnTap)
        btnRegresar.destination = "menus.menuHistory"
        composer.removeScene( "bloqueo" )
        introIsPlaying=true
end

function scene:show( event )
    -- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
    local group = self.view

end
 

 
-- Called when scene is about to move offscreen:
function scene:hide( event )
    local group = self.view
   
    if myImage ~= nil then
        display.remove(myImage)--Remueve la imagen
        eliminaBotones()-------limpia los botones
    end
            
    composer.removeScene( composer.getSceneName( "current" ) )
    
    -- INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks
end



-- Called prior to the removal of scene's "view" (display group)
function scene:destroy( event )
    local group = self.view
    bandera=false  
    
    -- INSERT code here (e.g. remove listeners, widgets, save state, etc.)
    -- Remove listeners attached to the Runtime, timers, transitions, audio tracks

end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
---------------------------------------------------------