-----------------------------------------------------------------------------------------
-- diferencias.lua
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local gameState = require("utilerias.gameState")
local base = require("utilerias.base" )
local scene = composer.newScene()
local imagenProducto = {}
local introIsPlaying = true 
local ar = true
local B = true
local isTap = false
local lugarIzq
local lugarDer

-- Variable que indica si ganas el juego
gameState.success = 0

destinoBack = "menus.menuDesbloquear"

-- Variables obligatorias para juegos con dragging
scene.imagenSeleccionada = nil
scene.imagenColisionada = nil
scene.posIniX = nil
scene.posIniY = nil
local topsign
------VARIABLES DE GRABACION--------------
local r
local isAndroid = false
local dataFileName = "audiopapu"

if "simulator" == system.getInfo("environment") then
    dataFileName = dataFileName .. ".aif"
else
	local platformName = system.getInfo( "platformName" )
    
    if "iPhone OS" == platformName then
        dataFileName = dataFileName .. ".aif"
    elseif "Android" == platformName then
        dataFileName = dataFileName .. ".wav"
        isAndroid = true
    else
    	print("Unknown OS " .. platformName )
    end
end

--crea el objeto r que va a servir para hacer las grabaciones
local filePath = system.pathForFile( dataFileName, system.DocumentsDirectory )
r = media.newRecording(filePath)

--funcion para limpiar la memoria yliberar el audio
local function llamame(event)
    print("khe")
    audio.dispose(event.handle)
end

local function remover ()
    scene.imagenSeleccionada:removeSelf()
end

---------------------******Funcion obligatoria*********----
function scene:successful()
    composer.showOverlay( "bloqueo" ,{ isModal = true } )
           
    -------------------********inicio del record********----------------------
 
    print("presionado")
    utils.reproducirSonido("bip")
    timer.performWithDelay(1000, function()
        r:startRecording()
    end,1)
   
    timer.performWithDelay(4000, function() 
        r:stopRecording()
        utils.reproducirSonido("bip")
        timer.performWithDelay(1000, function()
            -- Play back the recording
            local file = io.open( filePath, "r" ) 
            if file then
                io.close( file )
                local playbackSoundHandle = audio.loadStream( dataFileName, system.DocumentsDirectory )
				audio.play( playbackSoundHandle, { onComplete= llamame,remover()} )
            end 
        end,1)
    end,1)
    ----------**********fin de el record*****------------------------------
             
    composer.showOverlay( "bloqueo" ,{ isModal = true } )
    timer.performWithDelay(8000, function()
    --scene.imagenSeleccionada:removeEventListener("touch")
        B = true
        gameState.success = gameState.success+1
        
        if gameState.success >= 6 then
            timer.performWithDelay(1250, function()	
                utils.ponerGlobos()   
            end,1)
                
            gameState.success = 0
        else 
            composer.removeScene( "bloqueo" )
        end        
    end,1)
end

----------------------------------------------------------------------------------------
local function btnTap(event)
    --esto es para quitar el bloqueo 
    if introIsPlaying == true then
        local audioChannel = audio.stop() 
        introIsPlaying = false
    end
        
    utils.reproducirSonido("boton")
    composer.gotoScene ( "menus.menuDesbloquear", { effect = "fade"} )
    print(event.target.destination)
    return true
end 
----------------------------------------------------------------------------------------

 ------------------------------------------------------------------------------------------
-- FUNCION "SHUFFLE" PARA NÚMEROS ALEATORIOS NO REPETIDOS
-------------------------------------------------

local function shuffleArray(array)
    local arrayCount = #array
    for i = arrayCount, 2, -1 do
        local j = math.random(1, i)
        array[i], array[j] = array[j], array[i]
    end
    return array
end

-------------------------------------------------
function scene:create( event )
    
    print(gameState.tipodejuego)
    
    local group = self.view

    local background = display.newImage( group,path..pathGame.."fondo.png" )
    background:translate( centerX, centerY )
    background:toBack()

    local objColision
    local objColision2
    local objColision3
    
    --*****---- circulos
    if gameState.tipodejuego == 1 or gameState.tipodejuego == 2 then
        objColision = display.newImage( group, path..pathGame.."objColision.png" )
        objColision:translate( centerX/2.5, centerY/2 )
        objColision.w=objColision.contentWidth
        objColision.h=objColision.contentHeight
        
        objColision2 = display.newImage( group, path..pathGame.."objColision2.png" )
        objColision2:translate( centerX*1.05, centerY/2 )
        objColision2.w=objColision.contentWidth
        objColision2.h=objColision.contentHeight
        
        objColision3= display.newImage( group, path..pathGame.."objColision3.png" )
        objColision3:translate(centerX/.6 , centerY/2 )
        objColision3.w=objColision.contentWidth
        objColision3.h=objColision.contentHeight
    else 
        objColision = display.newImage( group, path..pathGame.."objColision.png" )
        objColision:translate( centerX*.5, centerY/2 )
        objColision.w=objColision.contentWidth
        objColision.h=objColision.contentHeight
      
        objColision2 = display.newImage( group, path..pathGame.."objColision2.png" )
        objColision2:translate( centerX*1.5, centerY/2 )
        objColision2.w=objColision.contentWidth
        objColision2.h=objColision.contentHeight
    end

    -- Creamos las posiciones iniciales para cada juego
    local imgWidth, sumWidth
    local imgHeight, sumHeight
    local countHeight
                
    -- Asignar valores de inicio a cada juego
    imagenProducto = {1,2,3,4,5,6}  
    imgWidth, imgHeight = display.contentWidth*.1, centerY*1.5 
    sumWidth, sumHeight = 170, 170
    countHeight = 6
                            
    shuffleArray(imagenProducto)
    
    
    --ASIGNA LA VARIABLE TIPO DE JUEGO Y LOS SETS
             
    for posicion, pieza in pairs (imagenProducto) do
            
        imagenProducto[posicion] = display.newImage(group, path..pathGame..pathSelection..pieza..".png")            
        imagenProducto[posicion].myName = pieza
                    
        -- CREAR IMÁGENES POR EL TIPO DE JUEGO
        --utils.renombrarImagenes(tipoJuego,imagenProducto[posicion])
       if gameState.tipodejuego == 1 then 
            if pathSelection == "juego1/" then  --la compra diferencias
                utils.renombrarImagenes(25,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(26,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(27,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 2 then
            if pathSelection == "juego1/" then  --el viaje diferencias
                utils.renombrarImagenes(28,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(29,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(30,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 3 then
            if pathSelection == "juego1/" then  --penny diferencias
                utils.renombrarImagenes(31,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(32,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(33,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 4 then
            if pathSelection == "juego1/" then  --los sonidos
                utils.renombrarImagenes(34,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(35,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(36,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 6 then
            if pathSelection == "juego1/" then  --danny diferencias
                utils.renombrarImagenes(37,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(38,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(39,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 7 then
            if pathSelection == "juego1/" then  --cally diferencias
                utils.renombrarImagenes(40,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(41,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(42,imagenProducto[posicion])
            end
        elseif gameState.tipodejuego == 8 then
            if pathSelection == "juego1/" then  --zara diferencias
                utils.renombrarImagenes(43,imagenProducto[posicion])
            elseif pathSelection =="juego2/" then
                utils.renombrarImagenes(44,imagenProducto[posicion])
            elseif pathSelection =="juego3/" then
                utils.renombrarImagenes(45,imagenProducto[posicion])
            end 
        end
                    
        imagenProducto[posicion]:translate(imgWidth, imgHeight)
        imgWidth = imgWidth + sumWidth   
            
        if(posicion%countHeight == 0) then
            imgHeight = imgHeight + sumHeight
            imgWidth = imgWidth - (sumHeight*(countHeight/1.3))
        end          
                          
        imagenProducto[posicion].w = imagenProducto[posicion].contentWidth
        imagenProducto[posicion].h = imagenProducto[posicion].contentHeight
            
        --if(gameState.tipodejuego ~= 5)then
                    
        ---***************************************************************
        utils.dragging(imagenProducto[posicion],objColision,composer.getSceneName("current"), function (resultado) 
            --esto es para quitar el bloqueo 
            if introIsPlaying == true then
                local audioChannel = audio.stop() 
                introIsPlaying = false
            end 
            
            if resultado then                  
                -----------------primer objeto--------------
                print "primer objeto"
                if(gameState.tipodejuego == 3) then --penny and benny
                    if pathSelection == "juego1/" then
                    --ver q onda aqui
                        if(imagenProducto[posicion].id == "puma" or imagenProducto[posicion].id == "pail" or imagenProducto[posicion].id == "pea") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "shampoo" or imagenProducto[posicion].id == "hippo" or imagenProducto[posicion].id == "apple") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "rope" or imagenProducto[posicion].id == "soap" or imagenProducto[posicion].id == "lollipop") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                elseif (gameState.tipodejuego == 7) then --call gally
                    if pathSelection == "juego1/" then
                        if(imagenProducto[posicion].id == "camel" or imagenProducto[posicion].id == "car" or imagenProducto[posicion].id == "cake") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "monkey" or imagenProducto[posicion].id == "raccoon" or imagenProducto[posicion].id == "bicycle") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "yak" or imagenProducto[posicion].id == "cake" or imagenProducto[posicion].id == "milk") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                elseif (gameState.tipodejuego == 8) then --zarah 
                    if pathSelection == "juego1/" then
                        if(imagenProducto[posicion].id == "scissors" or imagenProducto[posicion].id == "swans" or imagenProducto[posicion].id == "salami") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "baseball" or imagenProducto[posicion].id == "popsicle" or imagenProducto[posicion].id == "dinosaur") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "horse" or imagenProducto[posicion].id == "bus" ) then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                elseif (gameState.tipodejuego == 6) then --danny 
                    if pathSelection == "juego1/" then
                        if(imagenProducto[posicion].id == "tea" or imagenProducto[posicion].id == "toad" or imagenProducto[posicion].id == "teapot") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "button" or imagenProducto[posicion].id == "computer" or imagenProducto[posicion].id == "kitten") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "bat" or imagenProducto[posicion].id == "heart" or imagenProducto[posicion].id == "swimsuit") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                elseif (gameState.tipodejuego == 2) then --the trip 
                    if pathSelection == "juego1/" then
                        if(imagenProducto[posicion].id == "sweater" or imagenProducto[posicion].id == "sandals") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "glasses"  or imagenProducto[posicion].id == "whistle") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "socks"  or imagenProducto[posicion].id == "bus" or imagenProducto[posicion].id == "necklace") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                elseif (gameState.tipodejuego == 1) then --shooping
                    if pathSelection == "juego1/" then
                        if(imagenProducto[posicion].id == "chips"  or imagenProducto[posicion].id == "pear") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego2/" then
                        if(imagenProducto[posicion].id == "apple"  or imagenProducto[posicion].id == "pumpkin") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    elseif pathSelection == "juego3/" then
                        if(imagenProducto[posicion].id == "grape" or imagenProducto[posicion].id == "soup") then
                            utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                            transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end    
                    end
                else--este es el 2 ver q onda aqui
                    utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                    transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})   
                end    
            else ----------resultado es false
                
                if utils.colision(imagenProducto[posicion],objColision2) then 

                    scene.imagenColisionada = objColision2

                    if(gameState.tipodejuego == 3) then --penny and benny
                        if pathSelection == "juego1/" then
                        --ver q onda aqui
                            if(imagenProducto[posicion].id == "brown bear" or imagenProducto[posicion].id == "bat" or imagenProducto[posicion].id == "baboon") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "robin" or imagenProducto[posicion].id == "cowboy" or imagenProducto[posicion].id == "strawberry") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "lightbulb" or imagenProducto[posicion].id == "crab" or imagenProducto[posicion].id == "tub") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    elseif (gameState.tipodejuego == 7) then --los sonidos
                        if pathSelection == "juego1/" then
                            if(imagenProducto[posicion].id == "gecko" or imagenProducto[posicion].id == "girl" or imagenProducto[posicion].id == "gorila") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "eagle" or imagenProducto[posicion].id == "kangaroo" or imagenProducto[posicion].id == "tiger") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "mug" or imagenProducto[posicion].id == "pig" or imagenProducto[posicion].id == "dog") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    elseif (gameState.tipodejuego == 8) then --zarah 
                        if pathSelection == "juego1/" then
                            if(imagenProducto[posicion].id == "zinnias" or imagenProducto[posicion].id == "zane" or imagenProducto[posicion].id == "zebra") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "roses" or imagenProducto[posicion].id == "raisins" or imagenProducto[posicion].id == "daisy") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "paws" or imagenProducto[posicion].id == "gloves" or imagenProducto[posicion].id == "nose") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    elseif (gameState.tipodejuego == 6) then --danny 
                        if pathSelection == "juego1/" then
                            if(imagenProducto[posicion].id == "dog" or imagenProducto[posicion].id == "deer" or imagenProducto[posicion].id == "daisy") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "window" or imagenProducto[posicion].id == "ladybug" or imagenProducto[posicion].id == "ladder") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "toad" or imagenProducto[posicion].id == "bread" or imagenProducto[posicion].id == "red") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    elseif (gameState.tipodejuego == 2) then --the trip 
                        if pathSelection == "juego1/" then
                            if(imagenProducto[posicion].id == "taxi" or imagenProducto[posicion].id == "towel" or imagenProducto[posicion].id == "toy") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "hotel"  or imagenProducto[posicion].id == "button") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "belt"  or imagenProducto[posicion].id == "swimsuit" ) then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    elseif (gameState.tipodejuego == 1) then --shopping
                        if pathSelection == "juego1/" then
                            if(imagenProducto[posicion].id == "tea"  or imagenProducto[posicion].id == "tomato") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego2/" then
                            if(imagenProducto[posicion].id == "water"  or imagenProducto[posicion].id == "pasta") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end
                        elseif pathSelection == "juego3/" then
                            if(imagenProducto[posicion].id == "carrot"  or imagenProducto[posicion].id == "pot") then
                                utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                            else
                                utils.reproducirSonido("error")
                                transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                            end    
                        end
                    
                    else--este es el 2 ver q onda aqui
                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})   
                    end
                else

                    if gameState.tipodejuego == 1 or gameState.tipodejuego == 2 then
                        if utils.colision(imagenProducto[posicion],objColision3) then 

                            scene.imagenColisionada = objColision3

                            -----------------âqui va el 3
                            if gameState.tipodejuego == 1 then --shopping
                                if pathSelection == "juego1/" then
                                    if(imagenProducto[posicion].id == "carrot" or imagenProducto[posicion].id == "cake") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end
                                elseif pathSelection == "juego2/" then
                                    if(imagenProducto[posicion].id == "bacon " or imagenProducto[posicion].id == "pickle") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end
                                elseif pathSelection == "juego3/" then
                                    if(imagenProducto[posicion].id == "milk" or imagenProducto[posicion].id == "fork") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end    
                                end
                            elseif gameState.tipodejuego == 2 then
                                if pathSelection == "juego1/" then
                                    if(imagenProducto[posicion].id == "ship" or imagenProducto[posicion].id == "shampoo") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end
                                elseif pathSelection == "juego2/" then
                                    if(imagenProducto[posicion].id == "t-shirt" or imagenProducto[posicion].id == "brushes") then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end
                                elseif pathSelection == "juego3/" then
                                    if(imagenProducto[posicion].id == "toothbrush" ) then
                                        utils.primerSonido(composer.getSceneName("current"), scene.imagenSeleccionada.id)                            
                                        transition.to(scene.imagenSeleccionada, {x=scene.imagenColisionada.x, y=scene.imagenSeleccionada.y})
                                    else
                                        utils.reproducirSonido("error")
                                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                                    end    
                                end
                            end
                        else
                            utils.reproducirSonido("error")
                            transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                        end
                    else
                        utils.reproducirSonido("error")
                        transition.to(scene.imagenSeleccionada, {x=scene.posIniX, y=scene.posIniY})
                    end
                end
            end
        end)
    end   
                     
            
            
    local btnRegresar = display.newImage( group,"images/btn-regresar.png" )
    btnRegresar:translate( centerX/4, centerY*1.9 )
    btnRegresar:addEventListener("tap", btnTap)
            
    print(path..pathSelection)      
    utils.reproducirSonido("sounds/intros diferences/"..path..pathSelection.."introD",0)
    composer.removeScene( "bloqueo" )
    introIsPlaying=true 
end        
 ------------------------------------------------------------------------------------------

function scene:show( event )
    local group = self.view
end

function scene:hide( event )
    local group = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
        
    composer.removeScene( composer.getSceneName( "current" ) )    
end

function scene:destroy( event )
    -- Called prior to the removal of scene's "view" (group)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local group = self.view 
end

-------------- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------

return scene